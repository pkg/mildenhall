/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef __MILDENHALL_STATUSBAR_H__
#define __MILDENHALL_STATUSBAR_H__
#include "org.apertis.Mildenhall.Statusbar.h"
#endif //__MILDENHALL_STATUSBAR_H__
