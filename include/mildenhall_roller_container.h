/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* roller-container.h */
#ifndef _ROLLER_CONTAINER_H
#define _ROLLER_CONTAINER_H

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

/***********************************************************************************
*       @Filename       :       roller-container.h
*       @Module         :       RollerContainer
*       @Project        :       --
*----------------------------------------------------------------------------------
*       @Date           :       18/07/2012
*----------------------------------------------------------------------------------
*       @Description    :       The basic idea of RollerContainer To customize the
*                               mx-roller as per mildenhall requirements.They are,
*                               1)Background color for roller
*                               2)Arrow at the focus item
*                               3)Blurr Effects
*                               4)Any addtional overlay upon the roller if any.
*
************************************************************************************/

/***********************************************************************************
        @Includes
************************************************************************************/

#include "liblightwood-container.h"
#include "liblightwood-roller.h"
#include <glib-object.h>
#include <thornbury/thornbury.h>

/***********************************************************************************
        @Macro Definitions
************************************************************************************/
G_BEGIN_DECLS

GType mildenhall_roller_container_get_type (void) G_GNUC_CONST;
#define MILDENHALL_TYPE_ROLLER_CONTAINER mildenhall_roller_container_get_type ()

#define MILDENHALL_ROLLER_CONTAINER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_ROLLER_CONTAINER, MildenhallRollerContainer))

#define MILDENHALL_ROLLER_CONTAINER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_ROLLER_CONTAINER, MildenhallRollerContainerClass))

#define MILDENHALL_IS_ROLLER_CONTAINER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_ROLLER_CONTAINER))

#define MILDENHALL_IS_ROLLER_CONTAINER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_ROLLER_CONTAINER))

#define MILDENHALL_ROLLER_CONTAINER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_ROLLER_CONTAINER, MildenhallRollerContainerClass))

typedef struct _MildenhallRollerContainer MildenhallRollerContainer;
typedef struct _MildenhallRollerContainerClass MildenhallRollerContainerClass;

typedef enum
{
  MILDENHALL_FIXED_ROLLER_CONTAINER,
  MILDENHALL_VARIABLE_ROLLER_CONTAINER,
} MildenhallRollerContainerType;

struct _MildenhallRollerContainer
{
  LightwoodContainer parent;
};

struct _MildenhallRollerContainerClass
{
  LightwoodContainerClass parent_class;

  void (*locking_finished)              (MildenhallRollerContainer *self);
  void (*item_activated)                (MildenhallRollerContainer *self,
                                         guint row);
  void (*item_expanded)                 (MildenhallRollerContainer *self,
                                         gboolean expanded);
  void (*scroll_started)                (MildenhallRollerContainer *self);
  void (*scrolling)                     (MildenhallRollerContainer *self);
  void (*roller_scroll_finished)        (MildenhallRollerContainer *self,
                                         guint first_visible_item,
                                         guint total_visible_item);
  void (*footer_clicked)                (MildenhallRollerContainer *self);
};


/***********************************************************************************
        @Prototypes of global functions
************************************************************************************/

gboolean mildenhall_roller_container_get_show_arrow     (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_show_arrow         (MildenhallRollerContainer *self,
                                                         gboolean enable);

gint mildenhall_roller_container_get_focused_row        (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_focused_row        (MildenhallRollerContainer *self,
                                                         gint focused_row,
                                                         gboolean animate);

gfloat mildenhall_roller_container_get_arrow_position   (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_arrow_position     (MildenhallRollerContainer *self,
                                                         gfloat pos);

guint mildenhall_roller_container_get_drag_threshold    (MildenhallRollerContainer *self);

gboolean mildenhall_roller_container_get_roll_over      (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_roll_over          (MildenhallRollerContainer *self,
                                                         gboolean enable);

GType mildenhall_roller_container_get_item_type         (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_item_type          (MildenhallRollerContainer *self,
                                                         GType item_type);

MildenhallRollerContainerType mildenhall_roller_container_get_roller_type
                                                        (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_roller_type        (MildenhallRollerContainer *self,
                                                         MildenhallRollerContainerType roller_type);

gboolean mildenhall_roller_container_get_motion_blur    (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_motion_blur        (MildenhallRollerContainer *self,
                                                         gboolean motion_blur);

gboolean mildenhall_roller_container_get_cylinder_distort
                                                        (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_cylinder_distort   (MildenhallRollerContainer *self,
                                                         gboolean cylinder_dstort);

gboolean mildenhall_roller_container_get_show_animation (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_show_animation     (MildenhallRollerContainer *self,
                                                         gboolean animate);

ThornburyModel *mildenhall_roller_container_get_model   (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_model              (MildenhallRollerContainer *self,
                                                         ThornburyModel *model);

gboolean mildenhall_roller_container_get_show_flow_layout
                                                        (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_show_flow_layout   (MildenhallRollerContainer *self,
                                                         gboolean show);

gfloat mildenhall_roller_container_get_width            (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_width              (MildenhallRollerContainer *self,
                                                         gfloat width);

gfloat mildenhall_roller_container_get_height           (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_height             (MildenhallRollerContainer *self,
                                                         gfloat height);

gfloat mildenhall_roller_container_get_child_width      (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_child_width        (MildenhallRollerContainer *self,
                                                         gfloat width);

gfloat mildenhall_roller_container_get_child_height     (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_child_height       (MildenhallRollerContainer *self,
                                                         gfloat height);

gboolean mildenhall_roller_container_get_focus_to_center
                                                        (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_focus_to_center    (MildenhallRollerContainer *self,
                                                         gboolean enable);

gboolean mildenhall_roller_container_get_display_background
                                                        (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_display_background (MildenhallRollerContainer *self,
                                                         gboolean display);

void mildenhall_roller_container_set_min_visible_actors (MildenhallRollerContainer *self,
                                                         gint min_visible_actors);

void mildenhall_roller_container_refresh                (MildenhallRollerContainer *self);

void mildenhall_roller_container_resize                 (MildenhallRollerContainer *self,
                                                         gfloat roller_width,
                                                         gfloat item_width,
                                                         gfloat item_height,
                                                         gint focused_row,
                                                         ClutterTimeline *timeline,
                                                         gboolean animate);

void mildenhall_roller_container_add_attribute          (MildenhallRollerContainer *self,
                                                         const gchar *property,
                                                         gint column);

ClutterActor *mildenhall_roller_container_get_actor_for_row 
                                                        (MildenhallRollerContainer *self,
                                                         guint row);

const gchar *mildenhall_roller_container_get_language   (MildenhallRollerContainer *self);
void mildenhall_roller_container_set_language           (MildenhallRollerContainer *self,
                                                         const gchar *language);


void mildenall_roller_container_set_cylinder_distort (MildenhallRollerContainer *self,
						      gboolean cylinder_distort);

void v_ROLLERCONT_animate_footer(MildenhallRollerContainer *container,
				 guint inAnimatefooter);

void v_ROLLERCONT_set_footer_model(MildenhallRollerContainer *container,
				   ThornburyModel *footerModel);

ClutterActor *create_scroll_view (void);

G_END_DECLS


#endif /* _ROLLER_CONTAINER_H */
