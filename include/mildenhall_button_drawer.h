/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-button-drawer.h
 *
 * Created on: Sept 06, 2012
 *
 * mildenhall-button-drawer.h */


#ifndef _MILDENHALL_BUTTON_DRAWER_H
#define _MILDENHALL_BUTTON_DRAWER_H

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include "liblightwood-button.h"
#include <thornbury/thornbury.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_BUTTON_DRAWER mildenhall_button_drawer_get_type()

#define MILDENHALL_BUTTON_DRAWER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_BUTTON_DRAWER, MildenhallButtonDrawer))

#define MILDENHALL_BUTTON_DRAWER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_BUTTON_DRAWER, MildenhallButtonDrawerClass))

#define MILDENHALL_IS_BUTTON_DRAWER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_BUTTON_DRAWER))

#define MILDENHALL_IS_BUTTON_DRAWER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_BUTTON_DRAWER))

#define MILDENHALL_BUTTON_DRAWER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_BUTTON_DRAWER, MildenhallButtonDrawerClass))

//typedef enum _LightwoodTooltipDirection LightwoodTooltipDirection;
typedef struct _MildenhallButtonDrawer MildenhallButtonDrawer;
typedef struct _MildenhallButtonDrawerClass MildenhallButtonDrawerClass;
typedef struct _MildenhallButtonDrawerPrivate MildenhallButtonDrawerPrivate;

/**
 * MildenhallButtonDrawer:
 *
 * The #MildenhallButtonDrawer struct contains only private data.
 *
 */
struct _MildenhallButtonDrawer
{
	/*< private >*/
	LightwoodButton parent;

	MildenhallButtonDrawerPrivate *priv;
};

/**
 * MildenhallButtonDrawerClass
 * @state_changed: class handler for the #MildenhallButtonDrawer::state_changed signal
 * @swipe_end:	   class handler for the #MildenhallButtonDrawer::swipe_end signal	
 *
 * The #MildenhallButtonDrawerClass struct contains only private data.
 */
struct _MildenhallButtonDrawerClass
{
	LightwoodButtonClass parent_class;

	//void (* tooltip_shown) (MildenhallButtonDrawer *pButton);
        //void (* tooltip_hidden) (MildenhallButtonDrawer *pButton);
        void (* state_changed) (GObject *pButtonDrawer, gint inNewState);
	void (*swipe_end)    ( GObject *pButtonDrawer, LightwoodSwipeDirection inDirection);
};

# if 0
/* tooltip direction on button */
enum _LightwoodTooltipDirection
{
        MILDENHALL_BUTTON_DRAWER_LIGHTWOOD_TOOLTIP_LEFT,
	MILDENHALL_BUTTON_DRAWER_TOOLTIP_RIGHT,
	MILDENHALL_BUTTON_DRAWER_TOOLTIP_UP,
	MILDENHALL_BUTTON_DRAWER_TOOLTIP_DOWN
};
# endif

/* background states for the drawer button */
typedef enum
{
  MILDENHALL_BUTTON_DRAWER_BG_OPEN,
  MILDENHALL_BUTTON_DRAWER_BG_CLOSE,
  MILDENHALL_BUTTON_DRAWER_BG_NORMAL
} enBackgroundState;

GType mildenhall_button_drawer_get_type (void) G_GNUC_CONST;

/* funtion to create the mildenhall drawer button object */
ClutterActor *mildenhall_button_drawer_new (void);

/* functions to set properties */
void mildenhall_button_drawer_set_multi_state(MildenhallButtonDrawer *pButtonDrawer, gboolean bState);
void mildenhall_button_drawer_set_tooltip_width(MildenhallButtonDrawer *pButtonDrawer, gfloat flWidth);
void mildenhall_button_drawer_set_current_state(MildenhallButtonDrawer *pButtonDrawer, gint64 inState);
void mildenhall_button_drawer_set_model(MildenhallButtonDrawer *pButtonDrawer, ThornburyModel *pModel);
void mildenhall_button_drawer_set_active(MildenhallButtonDrawer *pButtonDrawer, gboolean bState);
void mildenhall_button_drawer_set_bg_state(MildenhallButtonDrawer *pButtonDrawer, enBackgroundState state);
void mildenhall_button_drawer_set_views_drawer(MildenhallButtonDrawer *pButtonDrawer, gboolean bState);
void mildenhall_button_drawer_set_tooltip_show(MildenhallButtonDrawer *pButtonDrawer, gboolean bState);
void mildenhall_button_drawer_set_tooltip_duration(MildenhallButtonDrawer *pButtonDrawer, gint inDuration);
void mildenhall_button_drawer_set_tooltip_direction(MildenhallButtonDrawer *pButtonDrawer, LightwoodTooltipDirection direction);
void mildenhall_button_drawer_set_tooltip_enabled(MildenhallButtonDrawer *pButtonDrawer, gboolean bEnabled);
void mildenhall_button_drawer_set_language(MildenhallButtonDrawer *pButtonDrawer, gchar *pLanguage);
/* functions to get properties */
gfloat mildenhall_button_drawer_get_width(MildenhallButtonDrawer *pButtonDrawer);
gfloat mildenhall_button_drawer_get_height(MildenhallButtonDrawer *pButtonDrawer);
gboolean mildenhall_button_drawer_get_multi_state(MildenhallButtonDrawer *pButtonDrawer);
gfloat mildenhall_button_drawer_get_tooltip_width(MildenhallButtonDrawer *pButtonDrawer);
gint mildenhall_button_drawer_get_current_state(MildenhallButtonDrawer *pButtonDrawer);
ThornburyModel *mildenhall_button_drawer_get_model(MildenhallButtonDrawer *pButtonDrawer);
gboolean mildenhall_button_drawer_get_active(MildenhallButtonDrawer *pButtonDrawer);
enBackgroundState mildenhall_button_drawer_get_bg_state(MildenhallButtonDrawer *pButtonDrawer);
gboolean mildenhall_button_drawer_get_views_drawer(MildenhallButtonDrawer *pButtonDrawer);
gboolean mildenhall_button_drawer_get_tooltip_show(MildenhallButtonDrawer *pButtonDrawer);
gboolean mildenhall_button_drawer_get_tooltip_enabled (MildenhallButtonDrawer *pButtonDrawer);
gint mildenhall_button_drawer_get_tooltip_duration (MildenhallButtonDrawer *pButtonDrawer);
LightwoodTooltipDirection mildenhall_button_drawer_get_tooltip_direction (MildenhallButtonDrawer *pButtonDrawer);



G_END_DECLS

#endif /* _MILDENHALL_BUTTON_DRAWER_H */
