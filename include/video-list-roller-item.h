/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* video-list-roller-item.h */

#ifndef __VIDEO_LIST_ROLLER_ITEM_H__
#define __VIDEO_LIST_ROLLER_ITEM_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include<clutter/clutter.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_VIDEO_LIST_ROLLER_ITEM mildenhall_video_list_roller_item_get_type ()
G_DECLARE_FINAL_TYPE (MildenhallVideoListRollerItem, mildenhall_video_list_roller_item, MILDENHALL, VIDEO_LIST_ROLLER_ITEM, ClutterActor)

MildenhallVideoListRollerItem *mildenhall_video_list_roller_item_new (void);

G_END_DECLS

#endif /* __VIDEO_LIST_ROLLER_ITEM_H__ */
