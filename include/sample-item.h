/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * Sample item for MildenhallRoller
 *
 *
 */

#ifndef __SAMPLE_ITEM_H__
#define __SAMPLE_ITEM_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <clutter/clutter.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_SAMPLE_ITEM  (mildenhall_sample_item_get_type ())
G_DECLARE_FINAL_TYPE (MildenhallSampleItem, mildenhall_sample_item, MILDENHALL, SAMPLE_ITEM, ClutterActor);

enum
{
  ARROW_WHITE,
  ARROW_GREY,
  ARROW_HIDDEN,

  ARROW_LAST
};

struct _MildenhallSampleItemClass
{
  ClutterActorClass        parent_class;

  // An effect can be attached to only one actor at the same time
};

ClutterActor*  mildenhall_sample_item_new              (void);

#ifndef MILDENHALL_DISABLE_DEPRECATED

#define TYPE_SAMPLE_ITEM      MILDENHALL_TYPE_SAMPLE_ITEM
#define SAMPLE_ITEM           MILDENHALL_SAMPLE_ITEM
#define SAMPLE_ITEM_CLASS     MILDENHALL_SAMPLE_ITEM_CLASS
#define IS_SAMPLE_ITEM        MILDENHALL_IS_SAMPLE_ITEM
#define IS_SAMPLE_ITEM_CLASS  MILDENHALL_IS_SAMPLE_ITEM_CLASS
#define SAMPLE_ITEM_GET_CLASS MILDENHALL_SAMPLE_ITEM_GET_CLASS

typedef MildenhallSampleItem      SampleItem;
typedef MildenhallSampleItemClass SampleItemClass;

G_DEPRECATED_FOR(mildenhall_sample_item_get_type)
GType         sample_item_get_type (void) G_GNUC_CONST;
G_DEPRECATED_FOR(mildenhall_sample_item_new)
ClutterActor *sample_item_new (void);

#endif /* MILDENHALL_DISABLE_DEPRECATED */

G_END_DECLS

#endif /* __SAMPLE_ITEM_H__ */

