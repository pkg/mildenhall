/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_views_drawer.h
 *
 * Created on: Oct 31, 2012
 *
 * mildenhall_views_drawer.h */


#ifndef _MILDENHALL_VIEWS_DRAWER_H
#define _MILDENHALL_VIEWS_DRAWER_H

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include "mildenhall_drawer_base.h"

G_BEGIN_DECLS

#define MILDENHALL_TYPE_VIEWS_DRAWER mildenhall_views_drawer_get_type()

#define MILDENHALL_VIEWS_DRAWER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_VIEWS_DRAWER, MildenhallViewsDrawer))

#define MILDENHALL_VIEWS_DRAWER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_VIEWS_DRAWER, MildenhallViewsDrawerClass))

#define MILDENHALL_IS_VIEWS_DRAWER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_VIEWS_DRAWER))

#define MILDENHALL_IS_VIEWS_DRAWER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_VIEWS_DRAWER))

#define MILDENHALL_VIEWS_DRAWER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_VIEWS_DRAWER, MildenhallViewsDrawerClass))

typedef struct _MildenhallViewsDrawer MildenhallViewsDrawer;
typedef struct _MildenhallViewsDrawerClass MildenhallViewsDrawerClass;
typedef struct _MildenhallViewsDrawerPrivate MildenhallViewsDrawerPrivate;

/**
 * MildenhallViewsDrawer:
 *
 * The #MildenhallviewsDrawer struct contains only private data.
 *
 */
struct _MildenhallViewsDrawer
{
	MildenhallDrawerBase parent;

	MildenhallViewsDrawerPrivate *priv;
};

/**
 * MildenhallViewsDrawerClass:
 *
 * The #MildenhallViewsDrawerClass struct contains only private data.
 *
 */
struct _MildenhallViewsDrawerClass
{
	MildenhallDrawerBaseClass parent_class;
};

GType mildenhall_views_drawer_get_type (void) G_GNUC_CONST;

ClutterActor* mildenhall_views_drawer_new (void);

G_END_DECLS

#endif /* _MILDENHALL_VIEWS_DRAWER_H */



