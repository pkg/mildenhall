/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* video-thumb-roller.h */

#ifndef __MILDENHALL_VIDEO_THUMBNAIL_ROLLER_H__
#define __MILDENHALL_VIDEO_THUMBNAIL_ROLLER_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include <libgrassmoor-av-player.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_VIDEO_THUMBNAIL_ROLLER mildenhall_video_thumbnail_roller_get_type ()
G_DECLARE_FINAL_TYPE (MildenhallVideoThumbnailRoller, mildenhall_video_thumbnail_roller, MILDENHALL, VIDEO_THUMBNAIL_ROLLER, ClutterActor)

MildenhallVideoThumbnailRoller *mildenhall_video_thumbnail_roller_new (void);

G_END_DECLS

#endif /* __MILDENHALL_VIDEO_THUMBNAIL_ROLLER_H__ */
