/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_text_box_entry.h
 *
 *
 *  mildenhall_text_box_entry.h   */

#ifndef __MILDENHALL_TEXT_BOX_ENTRY_H__
#define __MILDENHALL_TEXT_BOX_ENTRY_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include <thornbury/thornbury.h>
#include "liblightwood-textbox.h"

G_BEGIN_DECLS

#define MILDENHALL_TYPE_TEXT_BOX_ENTRY mildenhall_text_box_entry_get_type()

#define MILDENHALL_TEXT_BOX_ENTRY(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_TEXT_BOX_ENTRY, MildenhallTextBoxEntry))

#define MILDENHALL_TEXT_BOX_ENTRY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_TEXT_BOX_ENTRY, MildenhallTextBoxEntryClass))

#define MILDENHALL_IS_TEXT_BOX_ENTRY(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_TEXT_BOX_ENTRY))

#define MILDENHALL_IS_TEXT_BOX_ENTRY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_TEXT_BOX_ENTRY))

#define MILDENHALL_TEXT_BOX_ENTRY_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_TEXT_BOX_ENTRY, MildenhallTextBoxEntryClass))

typedef struct _MildenhallTextBoxEntry MildenhallTextBoxEntry;
typedef struct _MildenhallTextBoxEntryClass MildenhallTextBoxEntryClass;
typedef struct _MildenhallTextBoxEntryPrivate MildenhallTextBoxEntryPrivate;


/**
 * MildenhallTextBoxEntry:
 *
 * The #MildenhallTextBoxEntry struct contains only private data.
 *
 */
struct _MildenhallTextBoxEntry
{
  LightwoodTextBox parent;

  MildenhallTextBoxEntryPrivate *priv;
};


/**
 * MildenhallTextBoxEntryClass:
 *
 * The #MildenhallTextBoxEntryClass struct contains only private data.
 *
 */
struct _MildenhallTextBoxEntryClass
{
  LightwoodTextBoxClass parent_class;
  /* signal */
  void (* scroller_status) (GObject *pObject, gboolean scrollerState);
};

GType mildenhall_text_box_entry_get_type (void) G_GNUC_CONST;

/*function to create new mildenhall textbox */
ClutterActor *mildenhall_text_box_entry_new (void);
ThornburyModel *p_mildenhall_text_box_get_model (MildenhallTextBoxEntry *pTextBoxEntry);
void mildenhall_text_box_get_displayed_lines (MildenhallTextBoxEntry *pTextBoxEntry);

G_END_DECLS

#endif /* __MILDENHALL_TEXT_BOX_ENTRY_H__ */
