/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* map-widget.h */

#ifndef __MAP_WIDGET_H__
#define __MAP_WIDGET_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include "liblightwood-mapbase.h"
#include <clutter/clutter.h>
#include <champlain/champlain.h>

G_BEGIN_DECLS

#define MAP_TYPE_WIDGET map_widget_get_type()

#define MAP_WIDGET(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MAP_TYPE_WIDGET, MapWidget))

#define MAP_WIDGET_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MAP_TYPE_WIDGET, MapWidgetClass))

#define MAP_IS_WIDGET(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MAP_TYPE_WIDGET))

#define MAP_IS_WIDGET_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MAP_TYPE_WIDGET))

#define MAP_WIDGET_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MAP_TYPE_WIDGET, MapWidgetClass))

typedef struct _MapWidget MapWidget;
typedef struct _MapWidgetClass MapWidgetClass;
typedef struct _MapWidgetPrivate MapWidgetPrivate;

struct _MapWidget
{
  LightwoodMapBase parent;

  MapWidgetPrivate *priv;
};

struct _MapWidgetClass
{
  LightwoodMapBaseClass parent_class;
};

typedef enum
{
  MAP_WIDGET_VPTYPE_CURRENT,
  MAP_WIDGET_VPTYPE_DESTINATION,
  MAP_WIDGET_VPTYPE_POI,
  MAP_WIDGET_VPTYPE_CUSTOM,
  MAP_WIDGET_VPTYPE_ROUTE
} VpType;

GType map_widget_get_type (void) G_GNUC_CONST;

MapWidget *p_map_widget_new (void);
MapWidget* map_widget_thumbnail_map_init();
void map_widget_get_thumbnail_map(gint width,gint height,gfloat latitude, gfloat longitude);


G_END_DECLS

#endif /* __MAP_WIDGET_H__ */
