/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-cabinet-roller.h */

#ifndef _MILDENHALL_CABINET_ROLLER_H
#define _MILDENHALL_CABINET_ROLLER_H

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include "mildenhall_roller_container.h"
#include "mildenhall_cabinet_item.h"

G_BEGIN_DECLS

#define MILDENHALL_TYPE_CABINET_ROLLER mildenhall_cabinet_roller_get_type()

#define MILDENHALL_CABINET_ROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_CABINET_ROLLER, MildenhallCabinetRoller))

#define MILDENHALL_CABINET_ROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_CABINET_ROLLER, MildenhallCabinetRollerClass))

#define MILDENHALL_IS_CABINET_ROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_CABINET_ROLLER))

#define MILDENHALL_IS_CABINET_ROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_CABINET_ROLLER))

#define MILDENHALL_CABINET_ROLLER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_CABINET_ROLLER, MildenhallCabinetRollerClass))

#define MILDENHALL_CABINET_PICTURE_OVERLAY_PATH        "0"
#define MILDENHALL_CABINET_BOOKMARK_OVERLAY_PATH       "1"
#define MILDENHALL_CABINET_FOLDER_OVERLAY_PATH         "2"
#define MILDENHALL_CABINET_MUSIC_OVERLAY_PATH          "3"
#define MILDENHALL_CABINET_VIDEO_OVERLAY_PATH          "4"
#define MILDENHALL_CABINET_DOC_OVERLAY_PATH            "5"
#define MILDENHALL_CABINET_DELICOUS_OVERLAY_PATH       "6"

typedef struct _MildenhallCabinetRoller MildenhallCabinetRoller;
typedef struct _MildenhallCabinetRollerClass MildenhallCabinetRollerClass;
typedef struct _MildenhallCabinetRollerPrivate MildenhallCabinetRollerPrivate;


struct _MildenhallCabinetRoller
{
  ClutterActor parent;

  MildenhallCabinetRollerPrivate *priv;
};

struct _MildenhallCabinetRollerClass
{
	ClutterActorClass parent_class;
	void (* locking_finished) (MildenhallCabinetRoller *cabinet, MildenhallRollerContainer *roller);
	void (* prev_item_activated) (MildenhallCabinetRoller *roller, guint row,ThornburyModel *model);
	void (* same_prev_item_activated) (MildenhallCabinetRoller *roller, guint row,ThornburyModel *model);
	void (* active_item_activated) (MildenhallCabinetRoller *roller, guint row,ThornburyModel *model);
	void (* wipe_started) (MildenhallCabinetRoller *cabinet, MildenhallRollerContainer *roller);
	void (* wipe_completed) (MildenhallCabinetRoller *cabinet, MildenhallRollerContainer *roller);
};

GType mildenhall_cabinet_roller_get_type (void) G_GNUC_CONST;

MildenhallCabinetRoller *mildenhall_cabinet_roller_new (void);
void
v_CABINETROLL_show_prev_roller (MildenhallCabinetRoller *cabinet, gboolean bShow);
void
v_CABINETROLL_show_root_roller (MildenhallCabinetRoller *cabinet, gboolean bShow);
void
v_CABINETROLL_create_roller (MildenhallCabinetRoller *cabinet,
                             ThornburyModel *pModel);
void
v_CABINETROLL_set_item_type (MildenhallCabinetRoller *cabinet,
                             GType itemType);

G_END_DECLS

#endif /* _MILDENHALL_CABINET_ROLLER_H */
