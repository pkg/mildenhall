/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*  mildenhall_speller_multi_line_entry.h
 *
 *
 *  mildenhall_speller_multi_line_entry.h  */

#ifndef __MILDENHALL_SPELLER_MULTI_LINE_ENTRY_H__
#define __MILDENHALL_SPELLER_MULTI_LINE_ENTRY_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include "mildenhall_toggle_button.h"

G_BEGIN_DECLS

#define MILDENHALL_TYPE_SPELLER_MULTI_LINE_ENTRY mildenhall_speller_multi_line_entry_get_type()

#define MILDENHALL_SPELLER_MULTI_LINE_ENTRY(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_SPELLER_MULTI_LINE_ENTRY, MildenhallSpellerMultiLineEntry))

#define MILDENHALL_SPELLER_MULTI_LINE_ENTRY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_SPELLER_MULTI_LINE_ENTRY, MildenhallSpellerMultiLineEntryClass))

#define MILDENHALL_IS_SPELLER_MULTI_LINE_ENTRY(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_SPELLER_MULTI_LINE_ENTRY))

#define MILDENHALL_IS_SPELLER_MULTI_LINE_ENTRY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_SPELLER_MULTI_LINE_ENTRY))

#define MILDENHALL_SPELLER_MULTI_LINE_ENTRY_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_SPELLER_MULTI_LINE_ENTRY, MildenhallSpellerMultiLineEntryClass))

typedef struct _MildenhallSpellerMultiLineEntry MildenhallSpellerMultiLineEntry;
typedef struct _MildenhallSpellerMultiLineEntryClass MildenhallSpellerMultiLineEntryClass;
typedef struct _MildenhallSpellerMultiLineEntryPrivate MildenhallSpellerMultiLineEntryPrivate;

/**
 * MildenhallSpellerMultiLineEntry:
 *
 * The #MildenhallSpellerMultiLineEntry struct contains only private data.
 *
 */
struct _MildenhallSpellerMultiLineEntry
{
  ClutterActor parent;
  MildenhallSpellerMultiLineEntryPrivate *priv;
};

/**
 * MildenhallSpellerMultiLineEntryClass:
 *
 * The #MildenhallSpellerMultiLineEntryClass struct contains only private data.
 *
 */
struct _MildenhallSpellerMultiLineEntryClass
{
  ClutterActorClass parent_class;
};

GType mildenhall_speller_multi_line_entry_get_type (void) G_GNUC_CONST;

/*function to create new multi-line entry */
ClutterActor *mildenhall_speller_multi_line_entry_new (void);
void v_multi_line_speller_set_text(MildenhallSpellerMultiLineEntry *pSelf, GVariant *pArgList);
void v_multi_line_speller_set_model(MildenhallSpellerMultiLineEntry *pSelf, ThornburyModel *pModel);
void v_multi_line_speller_set_clear_text(MildenhallSpellerMultiLineEntry *pSelf, gboolean clearText);
ThornburyModel *p_multi_line_create_text_box_model (void);
ThornburyModel *p_multi_line_create_toggle_model (void);
void v_multi_line_speller_column_changed_cb (gint col, gpointer pSelf);

G_END_DECLS

#endif /* __MILDENHALL_SPELLER_MULTI_LINE_ENTRY_H__ */
