/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_button_speller.h
 *
 *
 * mildenhall_button_speller.h */

#ifndef __MILDENHALL_BUTTON_SPELLER_H__
#define __MILDENHALL_BUTTON_SPELLER_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include <thornbury/thornbury.h>
#include "liblightwood-button.h"

G_BEGIN_DECLS

#define MILDENHALL_TYPE_BUTTON_SPELLER mildenhall_button_speller_get_type()

#define MILDENHALL_BUTTON_SPELLER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_BUTTON_SPELLER, MildenhallButtonSpeller))

#define MILDENHALL_BUTTON_SPELLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_BUTTON_SPELLER, MildenhallButtonSpellerClass))

#define MILDENHALL_IS_BUTTON_SPELLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_BUTTON_SPELLER))

#define MILDENHALL_IS_BUTTON_SPELLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_BUTTON_SPELLER))

#define MILDENHALL_BUTTON_SPELLER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_BUTTON_SPELLER, MildenhallButtonSpellerClass))

typedef struct _MildenhallButtonSpeller MildenhallButtonSpeller;
typedef struct _MildenhallButtonSpellerClass MildenhallButtonSpellerClass;
typedef struct _MildenhallButtonSpellerPrivate MildenhallButtonSpellerPrivate;

/**
 * MildenhallButtonSpeller:
 *
 * The #MildenhallButtonSpeller struct contains only private data.
 *
 */
struct _MildenhallButtonSpeller
{
  LightwoodButton parent;
  MildenhallButtonSpellerPrivate *priv;
};

/**
 * MildenhallButtonSpellerClass:
 *
 * The #MildenhallButtonSpellerClass struct contains only private data.
 *
 */
struct _MildenhallButtonSpellerClass
{
  LightwoodButtonClass parent_class;
};

GType mildenhall_button_speller_get_type (void) G_GNUC_CONST;

/* functions to create new speller button  */
ClutterActor *mildenhall_button_speller_new (void);
/* functions to set properties */
void v_button_speller_set_text(ClutterActor *pActor, const gchar *pText);
void v_button_speller_set_x(MildenhallButtonSpeller *pButtonSpeller, gfloat flX);
void v_button_speller_set_y(MildenhallButtonSpeller *pButtonSpeller, gfloat flY);
void v_button_speller_set_width(MildenhallButtonSpeller *pButtonSpeller, gfloat flWidth);
void v_button_speller_set_height(MildenhallButtonSpeller *pButtonSpeller, gfloat flHeight);
void v_button_speller_set_type(MildenhallButtonSpeller *pButtonSpeller, gchar *pType);
void v_button_speller_set_name(MildenhallButtonSpeller *pButtonSpeller, gchar *pName);
void v_button_speller_set_normal(MildenhallButtonSpeller *pButtonSpeller, gchar *pNormal);
void v_button_speller_set_pressed(MildenhallButtonSpeller *pButtonSpeller, gchar *pPressed);
void v_button_speller_set_model (MildenhallButtonSpeller *pButtonSpeller, ThornburyModel *pModel);
void v_button_speller_set_font (MildenhallButtonSpeller *pButtonSpeller, gchar *pFontType);

G_END_DECLS

#endif /* __MILDENHALL_BUTTON_SPELLER_H__ */
