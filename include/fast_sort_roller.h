/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* sort-roller.h */

#ifndef __MILDENHALL_SORT_ROLLER_H__
#define __MILDENHALL_SORT_ROLLER_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

GType mildenhall_sort_roller_get_type (void) G_GNUC_CONST;
#define MILDENHALL_TYPE_SORT_ROLLER mildenhall_sort_roller_get_type()

#define MILDENHALL_SORT_ROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_SORT_ROLLER, MildenhallSortRoller))

#define MILDENHALL_SORT_ROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_SORT_ROLLER, MildenhallSortRollerClass))

#define SORT_IS_ROLLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_SORT_ROLLER))

#define SORT_IS_ROLLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_SORT_ROLLER))

#define MILDENHALL_SORT_ROLLER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_SORT_ROLLER, MildenhallSortRollerClass))

typedef struct _MildenhallSortRoller MildenhallSortRoller;
typedef struct _MildenhallSortRollerClass MildenhallSortRollerClass;

struct _MildenhallSortRoller
{
  ClutterGroup parent;
};

struct _MildenhallSortRollerClass
{
  ClutterGroupClass parent_class;
};

MildenhallSortRoller *mildenhall_sort_roller_new (void);

#define ROLLER_FONT(size) "DejaVuSansCondensed " #size "px"
G_END_DECLS

#endif /* __MILDENHALL_SORT_ROLLER_H__ */
