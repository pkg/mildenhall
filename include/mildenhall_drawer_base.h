/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_drawer_base.h
 *
 * Created on: Sept 06, 2012
 *
 * mildenhall_drawer_base.h */


#ifndef _MILDENHALL_DRAWER_BASE_H
#define _MILDENHALL_DRAWER_BASE_H

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include "mildenhall_button_drawer.h"

G_BEGIN_DECLS

#define MILDENHALL_DRAWER_TYPE_BASE mildenhall_drawer_base_get_type()

#define MILDENHALL_DRAWER_BASE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_DRAWER_TYPE_BASE, MildenhallDrawerBase))

#define MILDENHALL_DRAWER_BASE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_DRAWER_TYPE_BASE, MildenhallDrawerBaseClass))

#define MILDENHALL_DRAWER_IS_BASE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_DRAWER_TYPE_BASE))

#define MILDENHALL_DRAWER_IS_BASE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_DRAWER_TYPE_BASE))

#define MILDENHALL_DRAWER_BASE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_DRAWER_TYPE_BASE, MildenhallDrawerBaseClass))

typedef struct _MildenhallDrawerBase MildenhallDrawerBase;
typedef struct _MildenhallDrawerBaseClass MildenhallDrawerBaseClass;
typedef struct _MildenhallDrawerBasePrivate MildenhallDrawerBasePrivate;

/**
 * MildenhallDrawerBase:
 *
 * The #MildenhallDrawerBase struct contains only private data.
 *
 */
struct _MildenhallDrawerBase
{
	ClutterActor parent;

	MildenhallDrawerBasePrivate *priv;
};

/**
 * MildenhallDrawerBaseClass:
 * @drawer_open: class handler for the #MildenhallDrawerBase::drawer_open signal
 * @drawer_close: class handler for the #MildenhallDrawerBase::drawer_close signal
 * @drawer_tooltip_shown: class handler for the #MildenhallDrawerBase::drawer_tooltip_shown signal
 * @drawer_tooltip_hidden: class handler for the #MildenhallDrawerBase::drawer_tooltip_hidden signal
 * @drawer_button_released: class handler for the #MildenhallDrawerBase::drawer_button_released signal
 *
 * The #MildenhallDrawerBaseClass struct contains only private data.
 */
struct _MildenhallDrawerBaseClass
{
	ClutterActorClass parent_class;
	void (*drawer_open)    ( GObject *pDrawer);
	void (*drawer_close)   ( GObject *pDrawer);
	void (*drawer_tooltip_shown)  ( GObject *pDrawer);
	void (*drawer_tooltip_hidden) ( GObject *pDrawer);
	void (*drawer_button_released) ( GObject *pDrawer, gchar* pButtonName);
};

/**
 * enMildenhallDrawerType:
 * @MILDENHALL_CONTEXT_DRAWER: Context drawer widget.
 * @MILDENHALL_VIEWS_DRAWER: Viwes drawer widget.
 * @MILDENHALL_NAVI_DRAWER: Navi drawer widget.
 *
 **/
typedef enum 
{
  MILDENHALL_CONTEXT_DRAWER,
  MILDENHALL_VIEWS_DRAWER,
  MILDENHALL_NAVI_DRAWER
} enMildenhallDrawerType;

/**
 * MildenhallDrawerBaseModelColumns:
 * These are enumerations which denotes merely a numbers. Here the type given in front of the enum is the type of model columns.
 *
 * @MILDENHALL_DRAWER_COLUMN_NAME: G_TYPE_STRING ,Name of the column.
 * @MILDENHALL_DRAWER_COLUMN_ICON: G_TYPE_STRING ,icon path.
 * @MILDENHALL_DRAWER_COLUMN_TOOLTIP_TEXT: G_TYPE_STRING ,Text for toolTip text.
 * @MILDENHALL_DRAWER_COLUMN_ACTIVE: G_TYPE_BOOLEAN ,TRUE for active button,FALSE for inactive button.
 * @MILDENHALL_DRAWER_COLUMN_LAST: Total number of columns.
 *
 * @DRAWER_COLUMN_LAST denotes total number of columns present in the Model.
 *              ---------------------------------------------------------------------------------------------------------
 *              | MILDENHALL_DRAWER_COLUMN_NAME   		|MILDENHALL_DRAWER_COLUMN_ICON      	|...........    |MILDENHALL_DRAWER_COLUMN_ACTIVE	|
 *              | (G_TYPE_STRING)(col 0)       		|(G_TYPE_STRING)(col 1)         |...........    	|(G_TYPE_BOOLEAN)(col n)|
 *              |--------------------------------------	|-------------------------------|---------------|-----------------------|
 *ROWS->        |-------------------------------	|-------------------------------|---------------|-----------------------|
 *              |-------------------------------	|-------------------------------|---------------|-----------------------|
 *              |-------------------------------	|-------------------------------|---------------|-----------------------|
 *              |-------------------------------	|-------------------------------|---------------|-----------------------|

 * Model has to be created using these enumerations.
 * Example:
 * ThornburyModel * model = thornbury_list_model_new(DRAWER_COLUMN_LAST,G_TYPE_STRING,NULL
 *						G_TYPE_STRING,NULL,
 *						G_TYPE_STRING,NULL,
 *						G_TYPE_BOOLEAN,NULL,
 *						NULL);
 *
 *	thornbury_model_append (model,
 *                        DRAWER_COLUMN_NAME,"SomeName",
 *                       DRAWER_COLUMN_ICON,"/icon/path",
 *                       DRAWER_COLUMN_TOOLTIP_TEXT,"SomeText",
 *                    	 DRAWER_COLUMN_ACTIVE,TRUE,
 *            		-1);
 **/

typedef enum
{
  MILDENHALL_DRAWER_COLUMN_NAME,
  MILDENHALL_DRAWER_COLUMN_ICON,
  MILDENHALL_DRAWER_COLUMN_TOOLTIP_TEXT,
  MILDENHALL_DRAWER_COLUMN_ACTIVE,
  MILDENHALL_DRAWER_COLUMN_LAST,
} MildenhallDrawerBaseModelColumns;

GType mildenhall_drawer_base_get_type (void) G_GNUC_CONST;

ClutterActor *mildenhall_drawer_base_new (void);
gfloat mildenhall_drawer_base_get_x (MildenhallDrawerBase *pDrawerBase);
gfloat mildenhall_drawer_base_get_y (MildenhallDrawerBase *pDrawerBase);
gfloat mildenhall_drawer_base_get_width (MildenhallDrawerBase *pDrawerBase);
gfloat mildenhall_drawer_base_get_height (MildenhallDrawerBase *pDrawerBase);
gboolean mildenhall_drawer_base_get_close (MildenhallDrawerBase *pDrawerBase);
enMildenhallDrawerType mildenhall_drawer_base_get_drawer_type (MildenhallDrawerBase *pDrawerBase);
gfloat mildenhall_drawer_base_get_tooltip_width (MildenhallDrawerBase *pDrawerBase);
ThornburyModel* mildenhall_drawer_base_get_model (MildenhallDrawerBase *pDrawerBase);
void mildenhall_drawer_base_set_model (MildenhallDrawerBase *pDrawerBase, ThornburyModel *pModel);
void mildenhall_drawer_base_set_drawer_type (MildenhallDrawerBase *pDrawerBase, enMildenhallDrawerType enType);
ClutterActor* mildenhall_drawer_base_get_default_button (MildenhallDrawerBase *pDrawerBase);
GList* mildenhall_drawer_base_get_button_list (MildenhallDrawerBase *pDrawerBase);

G_END_DECLS

#endif /* _MILDENHALL_DRAWER_BASE_H */
