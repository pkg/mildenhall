/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_loading_bar
 * @title: MildenhallLoadingBar
 * @short_description: #MildenhallLoadingBar is used to give the user an
 * indication of the progress of an operation.
 * @see_also: #ThornburyItemFactory, #ClutterActor
 *
 * #MildenhallLoadingBar uses the concept of steps which ranges from 0.0 to 1.0
 * and it will display the percentage of steps that have been completed.
 * The applications can set the status for steps through the property
 * "loading-bar-pos".
 *
 * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 *
 * GObject *object = NULL;
 * MildenhallLoadingBar *bar = NULL;
 * ThornburyItemFactory *item_factory = NULL;
 *
 * item_factory = thornbury_item_factory_generate_widget_with_props (
 * 	MILDENHALL_TYPE_LOADING_BAR,
 * 	"/usr/share/mildenhall/mildenhall_loading_bar_prop.json");
 *
 * // Obtain the Object created by Item Factory
 * g_object_get (item_factory, "object", &object, NULL);
 * bar = MILDENHALL_LOADING_BAR (object);
 *
 * clutter_actor_add_child (stage, CLUTTER_ACTOR (bar));
 *
 * ]|
 *
 * Since: 0.3.0
 *
 */
#include "mildenhall_loading_bar.h"

/* Set the environment variable in terminal to enable traces: export LOADING_BAR_DEBUG=loading-bar */
enum _enLoadingBarDebugFlag
{
        LOADING_BAR_DEBUG = 1 << 0,

};

guint  loading_bar_debug_flags = 0;

static const GDebugKey loading_bar_debug_keys[] =
{
        { "loading-bar",   LOADING_BAR_DEBUG }
};

#define LOADING_BAR_HAS_DEBUG               ((loading_bar_debug_flags ) & 1)
#define LOADING_BAR_PRINT( a ...) \
        if (G_LIKELY (LOADING_BAR_HAS_DEBUG )) \
        {                               \
                g_print(a);           \
        }




static void v_get_loading_bar_show_status (GObject *pObject, GValue *pValue);
static void v_get_loading_bar_height (GObject *pObject, GValue *pValue);
static void v_get_loading_bar_width (GObject *pObject, GValue *pValue);
static void v_get_loading_bar_pos (GObject *pObject, GValue *pValue);

static void v_set_loading_bar_show_status (GObject *pObject, const GValue *pValue);
static void v_set_loading_bar_height (GObject *pObject, const GValue *pValue);
static void v_set_loading_bar_width (GObject *pObject, const GValue *pValue);
static void v_set_loading_bar_pos(GObject *pObject, const GValue *pValue);
enum LoadingBarProperty
{
	PROP_MILDENHALL_LOADING_BAR_FIRST,
	PROP_MILDENHALL_LOADING_BAR_WIDTH,
	PROP_MILDENHALL_LOADING_BAR_HEIGHT,
	PROP_MILDENHALL_LOADING_BAR_SHOW,
	PROP_MILDENHALL_LOADING_BAR_POS,
	PROP_MILDENHALL_LOADING_BAR_LAST
};


struct _MildenhallLoadingBarPrivate
{
	gfloat fLoadingBarWidth;
	gfloat fLoadingBarHeight;
	gfloat fLoadingBarPos;
	gboolean bLoadingBarShow;
	ClutterActor *pLoadingBar;
	ClutterActor *pLoadingBarLine;

};

G_DEFINE_TYPE_WITH_PRIVATE (MildenhallLoadingBar, mildenhall_loading_bar, CLUTTER_TYPE_ACTOR)


static void
mildenhall_loading_bar_get_property (GObject    *pObject,
                              guint       uinPropertyId,
                              GValue     *pValue,
                              GParamSpec *pPspec)
{
  LOADING_BAR_PRINT("\n loading_bar_get_property \n");
  switch (uinPropertyId)
    {
case	PROP_MILDENHALL_LOADING_BAR_WIDTH:
		v_get_loading_bar_width(pObject,pValue);
	break;
case	PROP_MILDENHALL_LOADING_BAR_HEIGHT:
		v_get_loading_bar_height(pObject,pValue);
	break;
case	PROP_MILDENHALL_LOADING_BAR_SHOW:
		v_get_loading_bar_show_status(pObject,pValue);
	break;
case	PROP_MILDENHALL_LOADING_BAR_POS:
		v_get_loading_bar_pos(pObject,pValue);
	break;
	default:
      		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyId, pPspec);
	break;
    }
}

static void
mildenhall_loading_bar_set_property (GObject      *pObject,
                              guint         uinPropertyId,
                              const GValue *pValue,
                              GParamSpec   *pPspec)
{
  LOADING_BAR_PRINT("\n loading_bar_set_property \n");
  switch (uinPropertyId)
    {
case	PROP_MILDENHALL_LOADING_BAR_WIDTH:
		v_set_loading_bar_width(pObject,pValue);
	break;
case	PROP_MILDENHALL_LOADING_BAR_HEIGHT:
		v_set_loading_bar_height(pObject,pValue);
	break;
case	PROP_MILDENHALL_LOADING_BAR_SHOW:
		v_set_loading_bar_show_status(pObject,pValue);
	break;
case	PROP_MILDENHALL_LOADING_BAR_POS:
		v_set_loading_bar_pos(pObject,pValue);
	break;	
        default:
    		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyId, pPspec);
	break;
    }
}
/*********************************************************************************************
 * Function:    mildenhall_loading_bar_dispose
 * Description: Dispose the object
 * Parameters:  The object's reference
 * Return:      void
 ********************************************************************************************/

static void
mildenhall_loading_bar_dispose (GObject *pObject)
{
  G_OBJECT_CLASS (mildenhall_loading_bar_parent_class)->dispose (pObject);
}
/*********************************************************************************************
 * Function:    mildenhall_loading_bar_finalize
 * Description: Call a finalize on the object
 * Parameters:  The object's reference
 * Return:      void
 ********************************************************************************************/

static void
mildenhall_loading_bar_finalize (GObject *pObject)
{
  G_OBJECT_CLASS (mildenhall_loading_bar_parent_class)->finalize (pObject);
}

/*********************************************************************************************
 * Function:    v_set_loading_bar_pos
 * Description: Set the loading bar position property
 * Parameters:  The object reference, and property value for the
 *              the property
 * Return:      void
 ********************************************************************************************/

static void v_set_loading_bar_pos(GObject *pObject, const GValue *pValue)
{
	MildenhallLoadingBarPrivate *priv = mildenhall_loading_bar_get_instance_private (MILDENHALL_LOADING_BAR (pObject));
        gfloat fLoadingBarPos = g_value_get_float (pValue);
  LOADING_BAR_PRINT("\n loading_bar_pos_set_property \n");
        /* set the width of progress bar */
	LOADING_BAR_PRINT("\nFloadingBarPos = %f\n",fLoadingBarPos);
	mx_progress_bar_set_progress(MX_PROGRESS_BAR(priv->pLoadingBar), fLoadingBarPos);
        if(priv->fLoadingBarPos == fLoadingBarPos)
                return;
        priv->fLoadingBarPos = fLoadingBarPos;
	
}


/*********************************************************************************************
 * Function:    v_set_loading_bar_width
 * Description: Set the loading bar width property
 * Parameters:  The object reference, and property value for the
 *              the property
 * Return:      void
 ********************************************************************************************/
static void v_set_loading_bar_width (GObject *pObject, const GValue *pValue)
{
        MildenhallLoadingBarPrivate *priv = mildenhall_loading_bar_get_instance_private (MILDENHALL_LOADING_BAR (pObject));
        gfloat fLoadingBarWidth = g_value_get_float (pValue);
        /* set the width of progress bar */
  LOADING_BAR_PRINT("\n Width = %f\n",fLoadingBarWidth);
	clutter_actor_set_width (priv->pLoadingBar,fLoadingBarWidth);

#ifdef WITH_EXTRA_LINE
	clutter_actor_set_width (priv->pLoadingBarLine,fLoadingBarWidth);
#endif
        if(priv->fLoadingBarWidth == fLoadingBarWidth)
                return;
        priv->fLoadingBarWidth = fLoadingBarWidth;
}

/*********************************************************************************************
 * Function:    v_set_loading_bar_height
 * Description: Set the loading bar height property
 * Parameters:  The object reference, and property value for the
 *              the property
 * Return:      void
 ********************************************************************************************/
static void v_set_loading_bar_height (GObject *pObject, const GValue *pValue)
{
        MildenhallLoadingBarPrivate *priv = mildenhall_loading_bar_get_instance_private (MILDENHALL_LOADING_BAR (pObject));
        gfloat fLoadingBarHeight = g_value_get_float (pValue);
  LOADING_BAR_PRINT("\n loading_bar_height_set_property \n");
        /* set the width of progress bar */
  	LOADING_BAR_PRINT("\n Height = %f\n",fLoadingBarHeight);
	clutter_actor_set_height (priv->pLoadingBar,fLoadingBarHeight);
//	clutter_actor_set_height (priv->pLoadingBarLine,fLoadingBarHeight);
        if(priv->fLoadingBarHeight == fLoadingBarHeight)
                return;
        priv->fLoadingBarHeight = fLoadingBarHeight;
}

/*********************************************************************************************
 * Function:    v_set_loading_bar_show_status
 * Description: Set the loading bar show status property
 * Parameters:  The object reference, and property value for the
 *              the property
 * Return:      void
 ********************************************************************************************/
static void v_set_loading_bar_show_status (GObject *pObject, const GValue *pValue)
{
        MildenhallLoadingBarPrivate *priv = mildenhall_loading_bar_get_instance_private (MILDENHALL_LOADING_BAR (pObject));
	/* set the width of progress bar */
        gboolean bLoadingBarShow = g_value_get_boolean (pValue);
  LOADING_BAR_PRINT("\n loading_bar_show_set_property \n");
	if(bLoadingBarShow)
	{
	LOADING_BAR_PRINT("\nIF bLoadingBarShow = %d",bLoadingBarShow);
		clutter_actor_show(CLUTTER_ACTOR(priv->pLoadingBar));
#ifdef WITH_EXTRA_LINE
		clutter_actor_show(CLUTTER_ACTOR(priv->pLoadingBarLine));
#endif
	}
	else
	{
	LOADING_BAR_PRINT("\nElse bLoadingBarShow = %d",bLoadingBarShow);
		clutter_actor_hide(CLUTTER_ACTOR(priv->pLoadingBar));
#ifdef WITH_EXTRA_LINE
		clutter_actor_hide(CLUTTER_ACTOR(priv->pLoadingBarLine));
#endif
	}
        if(priv->bLoadingBarShow == bLoadingBarShow)
                return;
        priv->bLoadingBarShow = bLoadingBarShow;
}
/*********************************************************************************************
 * Function:    v_get_loading_bar_width
 * Description: Get the loading bar width property
 * Parameters:  The object reference, and return location
 *              where the property value is to be updated
 * Return:      void
 ********************************************************************************************/
static void v_get_loading_bar_width (GObject *pObject, GValue *pValue)
{
        MildenhallLoadingBarPrivate *priv = mildenhall_loading_bar_get_instance_private (MILDENHALL_LOADING_BAR (pObject));
  LOADING_BAR_PRINT("\n loading_bar_width_get_property \n");
        g_value_set_float (pValue, priv->fLoadingBarWidth);
}

/*********************************************************************************************
 * Function:    v_get_loading_bar_height
 * Description: Get the loading bar height property
 * Parameters:  The object reference, and return location
 *              where the property value is to be updated
 * Return:      void
 ********************************************************************************************/
static void v_get_loading_bar_height (GObject *pObject, GValue *pValue)
{
        MildenhallLoadingBarPrivate *priv = mildenhall_loading_bar_get_instance_private (MILDENHALL_LOADING_BAR (pObject));
  LOADING_BAR_PRINT("\n loading_bar_height_get_property \n");
        g_value_set_float (pValue, priv->fLoadingBarHeight);
}

/*********************************************************************************************
 * Function:    v_get_loading_bar_pos
 * Description: Get the loading bar position property
 * Parameters:  The object reference, and return location
 *              where the property value is to be updated
 * Return:      void
 ********************************************************************************************/

static void v_get_loading_bar_pos (GObject *pObject, GValue *pValue)
{
        MildenhallLoadingBarPrivate *priv = mildenhall_loading_bar_get_instance_private (MILDENHALL_LOADING_BAR (pObject));
  LOADING_BAR_PRINT("\n loading_bar_pos_get_property \n");
        g_value_set_float (pValue, priv->fLoadingBarPos);



}
/*********************************************************************************************
 * Function:    v_get_loading_bar_show_status
 * Description: Get the loading bar show status property
 * Parameters:  The object reference, and return location
 *              where the property value is to be updated
 * Return:      void
 ********************************************************************************************/
static void v_get_loading_bar_show_status (GObject *pObject, GValue *pValue)
{
        MildenhallLoadingBarPrivate *priv = mildenhall_loading_bar_get_instance_private (MILDENHALL_LOADING_BAR (pObject));
  LOADING_BAR_PRINT("\n loading_bar_show_get_property \n");
        g_value_set_boolean(pValue, priv->bLoadingBarShow);
}

/*********************************************************************************************
 * Function:    mildenhall_loading_bar_class_init
 * Description: Class initialisation function for the object type.
 *                              Called automatically on the first call to g_object_new
 * Parameters:  The object's class reference
 * Return:      void
 ********************************************************************************************/

static void
mildenhall_loading_bar_class_init (MildenhallLoadingBarClass *pKlass)
{
  GObjectClass *pObjectClass = G_OBJECT_CLASS (pKlass);
  GParamSpec *pPspec = NULL;
  const char *pEnvString;
  pEnvString = g_getenv ("LOADING_BAR_DEBUG");

  if (pEnvString != NULL)
  {
  	loading_bar_debug_flags = g_parse_debug_string (pEnvString, loading_bar_debug_keys, G_N_ELEMENTS (loading_bar_debug_keys));
        LOADING_BAR_PRINT ("LOADING_BAR_PRINT: env_string %s %ld %d \n", pEnvString, G_LIKELY (LOADING_BAR_HAS_DEBUG), loading_bar_debug_flags);
  }

  pObjectClass->get_property = mildenhall_loading_bar_get_property;
  pObjectClass->set_property = mildenhall_loading_bar_set_property;
  pObjectClass->dispose = mildenhall_loading_bar_dispose;
  pObjectClass->finalize = mildenhall_loading_bar_finalize;
  LOADING_BAR_PRINT("\n Class Init \n");
  pPspec = g_param_spec_float("loading-bar-height", "LOADING-BAR-HEIGHT", "The loading bar height", 0.0,1000.0,0.0, G_PARAM_READWRITE);
  g_object_class_install_property(pObjectClass, PROP_MILDENHALL_LOADING_BAR_HEIGHT, pPspec);

  pPspec = g_param_spec_float("loading-bar-width", "LOADING-BAR-WIDTH", "The loading bar width", 0.0,1000.0,0.0, G_PARAM_READWRITE);
  g_object_class_install_property(pObjectClass, PROP_MILDENHALL_LOADING_BAR_WIDTH, pPspec);

  pPspec = g_param_spec_float("loading-bar-pos", "LOADING-BAR-POSITION", "The loading bar position", 0.0,1000.0,0.0, G_PARAM_READWRITE);
  g_object_class_install_property(pObjectClass, PROP_MILDENHALL_LOADING_BAR_POS, pPspec);

  pPspec = g_param_spec_boolean("loading-bar-show", "LOADING-BAR-SHOW", "To show and hide the loading bar",FALSE, G_PARAM_READWRITE);
  g_object_class_install_property(pObjectClass, PROP_MILDENHALL_LOADING_BAR_SHOW, pPspec);

}
/*********************************************************************************************
 * Function:    mildenhall_loading_bar_init
 * Description: Instance initialisation function for the object type.
 *                              Called automatically on every call to g_object_new
 * Parameters:  The object's reference
 * Return:      void
 ********************************************************************************************/
static void
mildenhall_loading_bar_init (MildenhallLoadingBar *pSelf)
{
  pSelf->priv = mildenhall_loading_bar_get_instance_private (pSelf);
  v_create_loading_bar(pSelf);
  LOADING_BAR_PRINT("\n Init \n");
}
/*********************************************************************************************
 * Function:    create_loading_bar
 * Description: Used to create the default loading bar.
 * Parameters:  The object's reference
 * Return:      void
 ********************************************************************************************/

void v_create_loading_bar(MildenhallLoadingBar *pLoadingBar)
{
	MildenhallLoadingBarPrivate *priv = pLoadingBar->priv;
  LOADING_BAR_PRINT ("\n Create_loading bar function \n");
	priv->pLoadingBar = mx_progress_bar_new();
	clutter_actor_set_size (priv->pLoadingBar, 45,10);
	clutter_actor_set_name (priv->pLoadingBar, "loadingbar");
	mx_progress_bar_set_progress(MX_PROGRESS_BAR(priv->pLoadingBar), 0.0);
	clutter_actor_add_child(CLUTTER_ACTOR(pLoadingBar),priv->pLoadingBar);
#ifdef WITH_EXTRA_LINE
	ClutterColor pLineColor = {0xFF, 0xFF, 0xFF, 0xFF};
	priv->pLoadingBarLine = clutter_rectangle_new_with_color (&pLineColor);
	clutter_actor_set_size (priv->pLoadingBarLine, 45, 1);
	clutter_actor_set_name (priv->pLoadingBarLine, "loadingbarline");
	clutter_actor_add_child(CLUTTER_ACTOR(pLoadingBar),priv->pLoadingBarLine);
#endif
	clutter_actor_show(CLUTTER_ACTOR(pLoadingBar));
  LOADING_BAR_PRINT("\n Create_loading bar function  end\n");
}

/**
 * mildenhall_loading_bar_new:
 *
 * Creates a new #MildenhallLoadingBar
 *
 * Returns: (transfer full): a newly created #MildenhallLoadingBar object.
 */
MildenhallLoadingBar *
mildenhall_loading_bar_new (void)
{
  return g_object_new (MILDENHALL_TYPE_LOADING_BAR, NULL);
}

/* *********************************************************************
                        End of file
 *********************************************************************** */

