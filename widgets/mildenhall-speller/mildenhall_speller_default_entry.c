/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_speller_default_entry
 * @title: MildenhallSpellerDefaultEntry
 * @short_description: The default entry type used by the Speller.
 * @see_also: #ClutterActor, #ThornburyItemFactory, #MildenhallSpeller
 *
 * #MildenhallSpellerDefaultEntry is the default entry type shown on the speller
 *  which supports auto-complete and history features.
 *
 * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * Since: 0.3.0
 */

#include "mildenhall_speller.h"
#include "mildenhall_speller_default_entry.h"
#include "mildenhall_text_box_entry.h"
#include "mildenhall_speller_urlhistory.h"

#define MILDENHALL_DEFAULT_SPELLER_PRINT(...) //g_print(__VA_ARGS__)
#define MILDENHALL_BLANK_RECT_WIDTH 648
#define MILDENHALL_BLANK_RECT_HEIGHT 64

G_DEFINE_TYPE (MildenhallSpellerDefaultEntry, mildenhall_speller_default_entry, CLUTTER_TYPE_ACTOR)

#define SPELLER_DEFAULT_ENTRY_PRIVATE(o) \
		(G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_SPELLER_DEFAULT_ENTRY, MildenhallSpellerDefaultEntryPrivate))

enum
{
	SIG_ENTRY_FIRST_SIGNAL,
	SIG_ENTRY_ACTION,
	SIG_GO_ACTION,
	SIG_SLIDE_DOWN,
	SIG_ENTRY_LAST_SIGNAL
};

guint32 entry_action_signals[SIG_ENTRY_LAST_SIGNAL] = {0,};
static void v_model_data_for_default_entry( MildenhallSpellerDefaultEntry *pSelf );
void mildenhall_speller_get_next_suggestion(MildenhallSpellerDefaultEntry *pSelf);
static void fill_go_status(MildenhallSpellerDefaultEntry *pSelf);

/* private structure mem of defaultentry speller */
struct _MildenhallSpellerDefaultEntryPrivate
{
	ThornburyModel *model;
	ThornburyModel *pRightModel;
	ThornburyModel *pLeftModel;
	ThornburyModel *pTextBoxModel;
	ThornburyModel *pSuggTextBoxModel;

	ClutterActor *blankRect;
	GVariant *argList;
	ClutterActor *textBoxEntry;
	ClutterActor *autoSuggestEntry;
	ClutterActor *leftButton;
	ClutterActor *rightButton;
	ClutterActor *defaultSpellerGroup;
	ClutterActor *leftLine;
	ClutterActor *rightLine;
	gboolean clearText;
	gboolean entry_editable;
	gboolean enable_history;
	gchar *toggleLeftState;
	gchar *toggleRightState;
	gchar *toggleLeftId;
	gchar *toggleRightId;
	gchar *entryId;
	gchar* history_text;
	gfloat fltWidth;
	gfloat fltWidth1;
	gfloat touchbegin_ypos;
	gfloat touchend_ypos;
	MildenhallSpellerUrlHistory *m_urlHistory;
	historyiter m_history_match_iter;
	gint inColumnId;
	gboolean enable_textsel;
};

/* toggle button model columns */
enum
{
	COLUMN_TEXT,                // set text to toggle button
	COLUMN_ACTIVE_ICON_PATH,    // set active image to toggle button
	COLUMN_INACTIVE_ICON_PATH,  // set inactive image to toggle button
	COLUMN_STATE_ID,            // set stateId to toggle button
	COLUMN_LAST
};

enum _MildenhallDefaultSpellerProperties
{
	MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_FIRST,
	MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_TEXT,
	MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_CLEAR_TEXT,
	MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_MODEL,
	MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_ENTRY_EDITABLE,
	MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_ENABLE_HISTORY,
	MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_MARK_TEXT,
	MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_LAST
};

static gboolean mildenhall_speller_urlentry_update_autosuggest_cb (MildenhallSpellerDefaultEntry *pSelf);

/****************************************************
 * Function : create_text_box_model
 * Description: create model
 * Parameters:
 * Return value: ThornburyModel
 *******************************************************/
ThornburyModel *
create_text_box_model(void)
{
	ThornburyModel *pModel = NULL;
	pModel = (ThornburyModel *)thornbury_list_model_new (1 ,G_TYPE_STRING, NULL ,-1);
	return pModel;
}

/****************************************************
 * Function : create_toggle_model
 * Description: create model
 * Parameters:
 * Return value: ThornburyModel
 *******************************************************/
ThornburyModel *
create_toggle_model(void)
{
	ThornburyModel *model = NULL;
	model = (ThornburyModel *)thornbury_list_model_new (COLUMN_LAST,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_POINTER, NULL,
			-1);
	return model;
}

/********************************************************
 * Function : v_entry_pressed_cb
 * Description: button callback
 * Parameters: GObject * ,pUserData
 * Return value: void
 ********************************************************/
static void v_entry_pressed_cb(GObject *pObject,gpointer pUserData)
{
	MildenhallSpellerDefaultEntry *pSelf = MILDENHALL_SPELLER_DEFAULT_ENTRY(pUserData);
	MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE(pSelf);
	g_signal_emit(pSelf, entry_action_signals[SIG_ENTRY_ACTION], 0,priv->entryId,priv->entryId );
}

/********************************************************
 * Function : v_left_toggle_button_cb
 * Description: toogle button callback
 * Parameters: GObject * ,newState
 * Return value: void
 ********************************************************/
static void v_left_toggle_button_cb(GObject *pObject, gpointer newState,gpointer pUserData)
{
	MildenhallSpellerDefaultEntry *pSelf = MILDENHALL_SPELLER_DEFAULT_ENTRY(pUserData);
	MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE(pSelf);
	priv->toggleLeftState = g_strdup(newState);
	g_signal_emit(pSelf, entry_action_signals[SIG_ENTRY_ACTION], 0, priv->toggleLeftId,(gchar*)newState);
}

/********************************************************
 * Function : v_right_toggle_button_cb
 * Description: toogle button callback
 * Parameters: GObject * ,newState
 * Return value: void
 ********************************************************/
static void v_right_toggle_button_cb(GObject *pObject, gpointer newState,gpointer pUserData)
{
	MildenhallSpellerDefaultEntry *pSelf = MILDENHALL_SPELLER_DEFAULT_ENTRY(pUserData);
	MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE(pSelf);
	priv->toggleRightState = g_strdup(newState);
	g_signal_emit(pSelf, entry_action_signals[SIG_ENTRY_ACTION], 0, priv->toggleRightId,(gchar*)newState);
	if(TRUE == priv->enable_history)
	{
		mildenhall_speller_get_next_suggestion(pSelf);
	}
}


/********************************************************
 * Function : v_default_speller_row_added_cb
 * Description: callback on model row added
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallSpellerDefaultEntry *
 * Return value: void
 ********************************************************/
static void v_default_speller_row_added_cb (ThornburyModel *pModel,ThornburyModelIter *pIter, MildenhallSpellerDefaultEntry *pSelf)
{
        MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE (pSelf);
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT: %s\n", __FUNCTION__);

	if( 1 == thornbury_model_get_n_rows(pModel) )
	{
		v_model_data_for_default_entry(pSelf);
		clutter_actor_show(priv->leftLine);
		clutter_actor_show(priv->rightLine);
		clutter_actor_show(priv->textBoxEntry);
		if(NULL != priv->autoSuggestEntry)
		{
			clutter_actor_show(priv->autoSuggestEntry);
		}
	}
	else
	{
		g_warning("\n speller cannot have more than one row \n ");
	}
}

/********************************************************
 * Function : v_default_speller_row_changed_cb
 * Description: callback on model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallSpellerDefaultEntry *
 * Return value: void
 ********************************************************/
static void  v_default_speller_row_changed_cb(ThornburyModel *pModel,ThornburyModelIter *pIter,MildenhallSpellerDefaultEntry *pSelf)
{
        MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE (pSelf);
        GValue value = {0, };
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT: %s\n", __FUNCTION__);
	priv->model = pModel;

	if(1 == thornbury_model_get_n_rows(pModel) && priv->inColumnId != -1)
	{
		thornbury_model_iter_get_value(pIter, priv->inColumnId, &value);
		thornbury_model_insert_value(priv->pTextBoxModel, 0, 0, &value);
		if(TRUE == priv->enable_history)
		{
			gchar *EntryText = (gchar *)g_value_get_string ( &value );
			if(NULL != EntryText)
			{
				if(0 == g_ascii_strncasecmp("url",priv->toggleLeftState,strlen(priv->toggleLeftState)))
				{
					priv->m_history_match_iter = mildenhall_speller_url_history_match(priv->m_urlHistory,HISTORY_URL,EntryText);
				}
				else
				{
					priv->m_history_match_iter = mildenhall_speller_url_history_match(priv->m_urlHistory,HISTORY_SEARCH,EntryText);
				}
				g_timeout_add_full(G_PRIORITY_HIGH,500,(GSourceFunc)mildenhall_speller_urlentry_update_autosuggest_cb,pSelf,NULL);
			}
		}
		g_value_unset (&value);
	}
	else
	{
		g_warning(" \n no row to change \n");
	}
}

/********************************************************
 * Function : v_speller_remove_model_data
 * Description: function to remove the model data
 * Parameters: ThornburyModel *
 * Return value: void
 ********************************************************/
void v_speller_remove_model_data(ThornburyModel *pModel)
{
        gint inCnt = 0;
        gint inTotalRows = 0;
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT: %s\n", __FUNCTION__);
	inTotalRows = thornbury_model_get_n_rows (pModel);

	if(NULL != pModel)
	{
		for(inCnt=0;inCnt<inTotalRows;inCnt++)
		{
			thornbury_model_remove(pModel,0);
		}
		thornbury_model_remove(pModel,0);
	}
}

/********************************************************
 * Function : v_default_speller_row_removed_cb
 * Description: callback on model row remove
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallSpellerDefaultEntry *
 * Return value: void
 ********************************************************/
static void v_default_speller_row_removed_cb (ThornburyModel *pModel,ThornburyModelIter *pIter, MildenhallSpellerDefaultEntry *pSelf)
{
        MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE (pSelf);
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT: %s\n", __FUNCTION__);

	v_speller_remove_model_data(priv->pLeftModel);
	v_speller_remove_model_data(priv->pRightModel);

	if(NULL != priv->pTextBoxModel)
	{
		thornbury_model_remove(priv->pTextBoxModel,0);
	}
	clutter_actor_hide(priv->leftLine);
	clutter_actor_hide(priv->rightLine);
	clutter_actor_hide(priv->textBoxEntry);

	if(NULL != priv->autoSuggestEntry)
	{
		clutter_actor_hide(priv->autoSuggestEntry);
	}
}

/********************************************************
 * Function    : update_left_button
 * Description : update left button model
 * Parameters  : MildenhallSpellerDefaultEntry,GVariant,bIsText
 * Return value: void
 ********************************************************/
static void update_left_button(MildenhallSpellerDefaultEntry *pSelf,GVariant *gvLeftIcon,gboolean bIsText)
{
	MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE( pSelf );
	if(gvLeftIcon)
	{
		GVariantIter inCount;
                gchar *pValue = NULL;
                gchar *pKey = NULL;
                int inCnt = 0;
		g_variant_iter_init ( &inCount, gvLeftIcon);

		while( g_variant_iter_next ( &inCount, "{ss}", &pKey, &pValue) )
		{
			MILDENHALL_DEFAULT_SPELLER_PRINT("pKey = %s pValue= %s \n",pKey,pValue);
			if( TRUE == bIsText  )
			{
				g_object_set (priv->leftButton,
				              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY,
				              NULL);
				thornbury_model_append (priv->pLeftModel, COLUMN_TEXT, g_strdup(pValue),
						COLUMN_ACTIVE_ICON_PATH, NULL,
						COLUMN_INACTIVE_ICON_PATH, NULL,
						COLUMN_STATE_ID, g_strdup(pKey),-1);
			}
			else
			{
				g_object_set (priv->leftButton,
				              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_ONLY,
				              NULL);
				thornbury_model_append (priv->pLeftModel, COLUMN_TEXT, NULL,
						COLUMN_ACTIVE_ICON_PATH, g_strdup(pValue),
						COLUMN_INACTIVE_ICON_PATH, g_strdup(pValue),
						COLUMN_STATE_ID, g_strdup(pKey),-1);
			}
			if(0 == inCnt)
			{
				priv->toggleLeftState =g_strdup(pKey);
				inCnt++;
			}
		}
		g_object_set(priv->leftButton, "state-model", priv->pLeftModel, NULL);

	}
}

/********************************************************
 * Function    : update_right_button
 * Description : update right button model
 * Parameters  : MildenhallSpellerDefaultEntry,GVariant,bIsText
 * Return value: void
 ********************************************************/
static void update_right_button(MildenhallSpellerDefaultEntry *pSelf,GVariant *gvRightIcon,gboolean bIsText1)
{
	MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE( pSelf );
	if(gvRightIcon)
	{
		GVariantIter inCount;
                gchar *pValue = NULL;
                gchar *pKey = NULL;
                int inCnt = 0;
		g_variant_iter_init (&inCount, gvRightIcon);

		while( g_variant_iter_next ( &inCount, "{ss}", &pKey, &pValue) )
		{
			MILDENHALL_DEFAULT_SPELLER_PRINT("pKey = %s pValue= %s \n",pKey,pValue);
			if( TRUE == bIsText1  )
			{
				g_object_set (priv->rightButton,
				              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY,
				              NULL);
				thornbury_model_append (priv->pRightModel, COLUMN_TEXT, g_strdup(pValue),
						COLUMN_ACTIVE_ICON_PATH, NULL,
						COLUMN_INACTIVE_ICON_PATH, NULL,
						COLUMN_STATE_ID, g_strdup(pKey),-1);
			}
			else
			{
				g_object_set (priv->rightButton,
				              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_ONLY,
				              NULL);
				thornbury_model_append (priv->pRightModel, COLUMN_TEXT, NULL,
						COLUMN_ACTIVE_ICON_PATH, g_strdup(pValue),
						COLUMN_INACTIVE_ICON_PATH, g_strdup(pValue),
						COLUMN_STATE_ID, g_strdup(pKey),-1);
			}
			if(0 == inCnt)
			{
				priv->toggleRightState =g_strdup(pKey);
				inCnt++;
			}
		}
		g_object_set(priv->rightButton, "state-model", priv->pRightModel, NULL);
	}
}

/****************************************************
 * Function : v_model_data_for_default_entry
 * Description: update the speller entry with model info
 * Parameters: MildenhallSpellerDefaultEntry*
 * Return value: void
 *******************************************************/
static void v_model_data_for_default_entry( MildenhallSpellerDefaultEntry *pSelf )
{
        MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE (pSelf);
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT: %s\n", __FUNCTION__);

	if (G_IS_OBJECT(priv->model) && thornbury_model_get_n_rows ( priv->model) > 0 )
	{
		ThornburyModelIter *pIter = thornbury_model_get_iter_at_row( priv->model, 0 );
		if(NULL != pIter)
		{
			GValue value = {0, };
                        gboolean bIsText;
                        gpointer gPtr;
                        GVariant *gvLeftIcon = NULL;
                        gboolean bIsText1;
                        gfloat fltWidth1;
                        gpointer gPtr1;
                        GVariant *gvRightIcon = NULL;
			thornbury_model_iter_get_value(pIter, 0, &value);
			bIsText = g_value_get_boolean (&value);
			MILDENHALL_DEFAULT_SPELLER_PRINT(" bIsText = %d \n",bIsText);
			g_value_unset (&value);

			thornbury_model_iter_get_value(pIter, 1, &value);
			priv->fltWidth = g_value_get_float(&value);
			MILDENHALL_DEFAULT_SPELLER_PRINT(" fltWidth = %f \n",priv->fltWidth);
			g_value_unset (&value);

			thornbury_model_iter_get_value(pIter, 2, &value);
			priv->toggleLeftId = g_value_dup_string(&value);
			g_value_unset (&value);

			g_object_set(priv->leftButton ,"width",priv->fltWidth,NULL);

			thornbury_model_iter_get_value(pIter, 3, &value);
			gPtr = g_value_get_pointer (&value);
			gvLeftIcon = (GVariant*) gPtr;

			update_left_button(pSelf,gvLeftIcon,bIsText);
			g_value_unset (&value);

			clutter_actor_set_position( priv->leftButton, 0,0);
			clutter_actor_set_position( priv->leftLine,priv->fltWidth+1,0);

			thornbury_model_iter_get_value(pIter, 4, &value);
			bIsText1 = g_value_get_boolean (&value);
			MILDENHALL_DEFAULT_SPELLER_PRINT(" bIsText1 = %d \n",bIsText1);
			g_value_unset (&value);

			thornbury_model_iter_get_value(pIter, 5, &value);
			fltWidth1 = g_value_get_float (&value);
			priv->fltWidth1 = fltWidth1;
			MILDENHALL_DEFAULT_SPELLER_PRINT(" fltWidth1 = %f \n",fltWidth1);
			g_value_unset (&value);

			g_object_set(priv->rightButton ,"width",fltWidth1,NULL);
			clutter_actor_set_x( priv->textBoxEntry,4+priv->fltWidth );
			g_object_set(priv->textBoxEntry,"width", MILDENHALL_BLANK_RECT_WIDTH -(priv->fltWidth+fltWidth1+4),NULL);

			if(NULL != priv->autoSuggestEntry)
			{
				g_object_set(priv->autoSuggestEntry,"width", MILDENHALL_BLANK_RECT_WIDTH -(priv->fltWidth+fltWidth1+4),NULL);
				clutter_actor_set_x( priv->autoSuggestEntry,5+priv->fltWidth );
				g_object_set(priv->autoSuggestEntry,"width", MILDENHALL_BLANK_RECT_WIDTH -(priv->fltWidth+fltWidth1+4),NULL);
			}

			thornbury_model_iter_get_value(pIter, 6, &value);
			priv->toggleRightId = g_value_dup_string(&value);
			g_value_unset (&value);

			thornbury_model_iter_get_value(pIter, 7, &value);
			gPtr1 = g_value_get_pointer (&value);
			gvRightIcon = (GVariant*) gPtr1;

			update_right_button(pSelf,gvRightIcon,bIsText1);
			g_value_unset (&value);

			clutter_actor_set_position( priv->rightButton,MILDENHALL_BLANK_RECT_WIDTH-fltWidth1,0);
			clutter_actor_set_position( priv->rightLine,MILDENHALL_BLANK_RECT_WIDTH-fltWidth1-1,0);

			thornbury_model_iter_get_value(pIter, 8, &value);
			thornbury_model_insert_value(priv->pTextBoxModel, 0, 0, &value);
			g_object_set(priv->textBoxEntry,"model",priv->pTextBoxModel,NULL);
			g_value_unset (&value);

			thornbury_model_iter_get_value(pIter, 9, &value);
			priv->entryId = g_value_dup_string(&value);
			g_value_unset (&value);
		}
	}
}


/****************************************************
 * Function : v_default_speller_update_view
 * Description: update the speller entry
 * Parameters: MildenhallSpellerDefaultEntry*
 * Return value: None
 *******************************************************/
static void v_default_speller_update_view( MildenhallSpellerDefaultEntry *pSelf )
{
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT: %s\n", __FUNCTION__);
	v_model_data_for_default_entry(pSelf);
}

/********************************************************
 * Function    : mildenhall_speller_get_next_suggestion
 * Description : function to get next auto suggest element
 * Parameters  : MildenhallSpellerDefaultEntry,GVariant,bIsText
 * Return value: void
 ********************************************************/
void mildenhall_speller_get_next_suggestion(MildenhallSpellerDefaultEntry *pSelf)
{
        gchar* history_match = NULL;
        ThornburyModelIter *pIter = NULL;
	MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE(pSelf);
	clutter_actor_show(priv->autoSuggestEntry);
	lightwood_text_box_clear_text(priv->autoSuggestEntry);
	history_match = mildenhall_speller_url_history_next (priv->m_urlHistory, priv->m_history_match_iter);
	pIter = thornbury_model_get_iter_at_row (priv->pSuggTextBoxModel, 0);
	if(NULL != pIter)
	{
		GValue gValue = G_VALUE_INIT;
		g_value_init (&gValue, G_TYPE_STRING);
		g_value_set_static_string (&gValue, history_match);
		priv->history_text = history_match;
		thornbury_model_iter_set_value(pIter  ,0 ,&gValue );
		g_value_unset (&gValue);
	}
}

/********************************************************
 * Function    : mildenhall_speller_urlentry_update_autosuggest_cb
 * Description : callback function to get update with suggest element
 * Parameters  : MildenhallSpellerDefaultEntry
 * Return value: gboolean
 ********************************************************/
static gboolean mildenhall_speller_urlentry_update_autosuggest_cb (MildenhallSpellerDefaultEntry *pSelf)
{
	MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE(pSelf);

	clutter_actor_show(priv->autoSuggestEntry);
	lightwood_text_box_clear_text(priv->autoSuggestEntry);

	if(priv->m_history_match_iter != NULL)
	{
                ThornburyModelIter *pIter = NULL;
		priv->history_text = g_strdup(priv->m_history_match_iter->m_cur->data);
		priv->history_text = mildenhall_speller_url_history_next(priv->m_urlHistory,priv->m_history_match_iter);

		pIter = thornbury_model_get_iter_at_row (priv->pSuggTextBoxModel, 0);
		if(NULL != pIter)
		{
			GValue gValue = G_VALUE_INIT;
			g_value_init (&gValue, G_TYPE_STRING);
			g_value_set_static_string (&gValue, priv->history_text);
			thornbury_model_iter_set_value(pIter  ,0 ,&gValue );
			g_value_unset (&gValue);
		}
	}
	return FALSE;
}

/********************************************************
 * Function    : v_speller_auto_suggest_update
 * Description : callback function to get autosuggest element
 * Parameters  : MildenhallSpellerDefaultEntry
 * Return value: void
 ********************************************************/
static void v_speller_auto_suggest_update(MildenhallSpellerDefaultEntry *pSelf)
{
	MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE(pSelf);
	if(TRUE == priv->enable_history)
	{
                gchar* EntryText = NULL;
		if(priv->m_history_match_iter)
		{
			g_free(priv->m_history_match_iter);
			priv->m_history_match_iter  = NULL;
		}
		g_object_get(priv->textBoxEntry, "text", &EntryText, NULL);

		if(0 == g_ascii_strncasecmp("url",priv->toggleLeftState,strlen(priv->toggleLeftState)))
		{
			priv->m_history_match_iter = mildenhall_speller_url_history_match(priv->m_urlHistory,HISTORY_URL,g_strdup(EntryText));
		}
		else
		{
			priv->m_history_match_iter = mildenhall_speller_url_history_match(priv->m_urlHistory,HISTORY_SEARCH,g_strdup(EntryText));
		}
		g_timeout_add_full(G_PRIORITY_HIGH,500,(GSourceFunc)mildenhall_speller_urlentry_update_autosuggest_cb,pSelf,NULL);
	}
}

/********************************************************
 * Function    : v_key_pressed_cb
 * Description : callback function at key press
 * Parameters  : GObject,event,userdata
 * Return value: void
 ********************************************************/
static void v_key_pressed_cb(GObject *pObject,gint event,gpointer pUserData)
{
	MildenhallSpellerDefaultEntry *pSelf = MILDENHALL_SPELLER_DEFAULT_ENTRY(pUserData);
	MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE(pSelf);

	switch (event)
	{
	case CLUTTER_KEY_Escape:
	case CLUTTER_KEY_Shift_L:
	case CLUTTER_KEY_Shift_R:
	case CLUTTER_KEY_Left:
	case CLUTTER_KEY_KP_Left:
	case CLUTTER_KEY_Right:
	case CLUTTER_KEY_KP_Right:
	case CLUTTER_KEY_End:
	case CLUTTER_KEY_KP_End:
	case CLUTTER_KEY_Begin:
	case CLUTTER_KEY_Home:
	case CLUTTER_KEY_KP_Home:
	case CLUTTER_KEY_Up:
	case CLUTTER_KEY_Down:
	case CLUTTER_KEY_Delete:
	case CLUTTER_KEY_KP_Delete:
		break;

	case CLUTTER_KEY_Return:
	{
		fill_go_status(pSelf);
		g_signal_emit(pSelf, entry_action_signals[SIG_GO_ACTION], 0,NULL);
		break;
	}

	case CLUTTER_KEY_BackSpace:
	{
		if(TRUE == priv->enable_history)
		{
                        gchar* EntryText = NULL;
			if(priv->m_history_match_iter)
			{
				g_free(priv->m_history_match_iter);
				priv->m_history_match_iter  = NULL;
			}
			g_object_get(priv->textBoxEntry, "text", &EntryText, NULL);

			if( 0 < strlen(EntryText))
			{
				if(0 == g_ascii_strncasecmp("url",priv->toggleLeftState,strlen(priv->toggleLeftState)))
				{
					priv->m_history_match_iter = mildenhall_speller_url_history_match(priv->m_urlHistory,HISTORY_URL,g_strdup(EntryText));
				}
				else
				{
					priv->m_history_match_iter = mildenhall_speller_url_history_match(priv->m_urlHistory,HISTORY_SEARCH,g_strdup(EntryText));
				}
				g_timeout_add_full(G_PRIORITY_HIGH,500,(GSourceFunc)mildenhall_speller_urlentry_update_autosuggest_cb,pSelf,NULL);
			}
			else
			{
				lightwood_text_box_clear_text(priv->autoSuggestEntry);
			}
		}
		break;
	}
	default:
	{
		v_speller_auto_suggest_update(pSelf);
		break;
	}
	}
}

/********************************************************
 * Function    : auto_suggest_editable
 * Description : callback function for auto suggest editable
 * Parameters  : GObject,event,userdata
 * Return value: void
 ********************************************************/
static void auto_suggest_editable(MildenhallSpellerDefaultEntry *pSelf)
{
        MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE (pSelf);
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_SPELLER_DEFAULT_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}

	if(TRUE == priv->enable_history)
	{
                gchar* EntryText = NULL;
		if(priv->m_history_match_iter)
		{
			g_free(priv->m_history_match_iter);
			priv->m_history_match_iter  = NULL;
		}
		g_object_get(priv->textBoxEntry, "text", &EntryText, NULL);

		if( 0 < strlen(EntryText))
		{
			if(0 == g_ascii_strncasecmp("url",priv->toggleLeftState,strlen(priv->toggleLeftState)))
			{
				priv->m_history_match_iter = mildenhall_speller_url_history_match(priv->m_urlHistory,HISTORY_URL,g_strdup(EntryText));
			}
			else
			{
				priv->m_history_match_iter = mildenhall_speller_url_history_match(priv->m_urlHistory,HISTORY_SEARCH,g_strdup(EntryText));
			}
			g_timeout_add_full(G_PRIORITY_HIGH,500,(GSourceFunc)mildenhall_speller_urlentry_update_autosuggest_cb,pSelf,NULL);
		}
		else
		{
			lightwood_text_box_clear_text(priv->autoSuggestEntry);
		}
	}
}

/********************************************************
 * Function    : insert_value_to_model
 * Description :  function to insert value to model
 * Parameters  : MildenhallSpellerDefaultEntry,TextValue
 * Return value: void
 ********************************************************/
static void insert_value_to_model(MildenhallSpellerDefaultEntry *pSelf,gchar *TextValue)
{
        MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE (pSelf);
        GValue value = {0};
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_SPELLER_DEFAULT_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}

	g_value_init(&value, G_TYPE_STRING);
	g_value_set_static_string (&value,TextValue);
	thornbury_model_insert_value(priv->pTextBoxModel, 0, 0, &value);
	g_value_unset (&value);

}

/********************************************************
 * Function    : fill_go_status
 * Description :  function to fill go status
 * Parameters  : MildenhallSpellerDefaultEntry
 * Return value: void
 ********************************************************/
static void fill_go_status(MildenhallSpellerDefaultEntry *pSelf)
{
	MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE(pSelf);

	GVariantBuilder *pVariant = g_variant_builder_new (G_VARIANT_TYPE ("a{ss}"));
	gchar *pEnteredText = NULL;
	g_object_get(priv->textBoxEntry, "text", &pEnteredText, NULL);
	g_variant_builder_add (pVariant, "{ss}",priv->entryId, g_strdup(pEnteredText));
	g_variant_builder_add (pVariant, "{ss}",priv->toggleLeftId, priv->toggleLeftState);
	g_variant_builder_add (pVariant, "{ss}",priv->toggleRightId, priv->toggleRightState);
	priv->argList = g_variant_builder_end (pVariant);
	g_variant_builder_unref(pVariant);

	if(TRUE == priv->enable_history)
	{
		if(NULL != priv->autoSuggestEntry)
		{
			lightwood_text_box_clear_text(priv->autoSuggestEntry);
			if(0 == g_ascii_strncasecmp("url",priv->toggleLeftState,strlen(priv->toggleLeftState)))
			{
				mildenhall_speller_url_history_add_item(priv->m_urlHistory,HISTORY_URL,g_strdup(pEnteredText));
			}
			else
			{
				mildenhall_speller_url_history_add_item(priv->m_urlHistory,HISTORY_SEARCH,g_strdup(pEnteredText));
			}
		}
	}
}

/********************************************************
 * Function    : update_autocomplete_model
 * Description :  function to update autocomplete model
 * Parameters  : MildenhallSpellerDefaultEntry
 * Return value: void
 ********************************************************/
static void update_autocomplete_model(MildenhallSpellerDefaultEntry *pSelf)
{
        MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE (pSelf);
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_SPELLER_DEFAULT_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}

	if(TRUE == priv->enable_history)
	{
		gchar* EntryText = NULL;
		g_object_get(priv->textBoxEntry, "text", &EntryText, NULL);
		if( strlen(EntryText) >= 1 )
		{
			if(NULL != priv->history_text)
			{
				lightwood_text_box_clear_text(priv->autoSuggestEntry);
				lightwood_text_box_clear_text(priv->textBoxEntry);
				insert_value_to_model(pSelf,g_strdup(priv->history_text));
			}
		}
	}
}


/**
 * v_speller_set_text:
 * @pSelf: object reference
 * @pText: text to set on textEntryBox
 *
 * set the text on TextEntryBox
 *
 */
void v_speller_set_text(MildenhallSpellerDefaultEntry *pSelf,gchar *pText)
{
	if(!g_strcmp0(pText,"SPACE"))
	{
		insert_value_to_model(pSelf,g_strdup(" "));
	}
	else if(!g_strcmp0(pText,"SYMBOLS") || !g_strcmp0(pText,"SHIFT"))
	{
		//TBD
	}
	else if(!g_strcmp0(pText,"RETURN") || !g_strcmp0(pText,"go") )
	{
		fill_go_status(pSelf);
	}
	else if(!g_strcmp0(pText,"dot"))
	{
		insert_value_to_model(pSelf,g_strdup("."));
	}
	else if(!g_strcmp0(pText,"CANCEL"))
	{
		lightwood_text_box_clear_text(pSelf->priv->textBoxEntry);
	}
	else if(!g_strcmp0(pText,"BKSP") || !g_strcmp0(pText,"←") )
	{
		insert_value_to_model(pSelf,g_strdup(pText));
		auto_suggest_editable(pSelf);
	}
	else if(!g_strcmp0(pText,"AUTOCOMPLETE"))
	{
		update_autocomplete_model(pSelf);
	}
	else
	{
		insert_value_to_model(pSelf,g_strdup(pText));
		auto_suggest_editable(pSelf);
	}
}

/**
 * v_default_speller_set_text:
 * @pSelf: object reference
 * @pArgList: arg list of type #GVariant
 *
 * sets the default text on TextEntryBox
 *
 */
void v_default_speller_set_text(MildenhallSpellerDefaultEntry *pSelf, GVariant *pArgList)
{
        GVariantIter iter;
        gchar *pText = NULL;
        gchar *key = NULL;

	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_SPELLER_DEFAULT_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}

	if(NULL == pArgList)
		return;

	g_variant_iter_init (&iter, pArgList);
	while (g_variant_iter_next (&iter, "{ss}", &key, &pText))
	{
		v_speller_set_text(pSelf,pText);
		if(pText!=NULL)
		{
			g_free (pText);
			pText = NULL;
		}
		if(key!=NULL)
		{
			g_free (key);
			key = NULL;
		}
	}
}

/**
 * v_default_speller_set_clear_text:
 * @pSelf: object reference
 * @clearText: set to true/false to clear the text
 *
 * set to clear the text on entry
 *
 */
void v_default_speller_set_clear_text(MildenhallSpellerDefaultEntry *pSelf, gboolean clearText)
{
        MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE (pSelf);
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT :%s clearText = %d \n", __FUNCTION__,clearText);
	if(!MILDENHALL_IS_SPELLER_DEFAULT_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}

	priv->clearText = clearText;
	if( TRUE == priv->clearText)
	{
		lightwood_text_box_clear_text(pSelf->priv->textBoxEntry);
		priv->clearText = FALSE;
	}
}

/**
 * v_default_speller_set_entry_editable:
 * @pSelf: object reference
 * @editable: set to true/false for entry to edit
 *
 * set to enable/disable to edit the entry
 *
 */
void v_default_speller_set_entry_editable(MildenhallSpellerDefaultEntry *pSelf, gboolean editable)
{
        MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE (pSelf);
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT :%s editable = %d \n", __FUNCTION__,editable);
	if(!MILDENHALL_IS_SPELLER_DEFAULT_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}
	priv->entry_editable = editable;
	g_object_set( priv->textBoxEntry ,"text-editable", priv->entry_editable, NULL );

	if(TRUE == priv->entry_editable)
	{
		g_object_set(priv->textBoxEntry, "focus-cursor",TRUE, NULL);
	}
	else
	{
		g_object_set(priv->textBoxEntry, "focus-cursor",FALSE, NULL);
	}
}

/**
 * v_default_speller_set_enable_history:
 * @pSelf: object reference
 * @historySupport: set to true/false for history support
 *
 * set to enable/disable for history support
 *
 */
void v_default_speller_set_enable_history(MildenhallSpellerDefaultEntry *pSelf, gboolean historySupport)
{
        MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE (pSelf);
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT :%s historySupport = %d \n", __FUNCTION__,historySupport);
	if(!MILDENHALL_IS_SPELLER_DEFAULT_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}
	priv->enable_history = historySupport;

	if(TRUE == priv->enable_history)
	{
		ThornburyItemFactory *itemFactory = thornbury_item_factory_generate_widget_with_props(MILDENHALL_TYPE_TEXT_BOX_ENTRY, PKGDATADIR"/mildenhall_speller_text_box_prop.json" );
		GObject *pTextObject = NULL;
                MildenhallTextBoxEntry *pTextBox = NULL;
                priv->m_urlHistory = mildenhall_speller_url_history_new ();
		g_object_get(itemFactory, "object", &pTextObject, NULL);
		pTextBox = MILDENHALL_TEXT_BOX_ENTRY (pTextObject);
		priv->autoSuggestEntry = CLUTTER_ACTOR(pTextBox);
		clutter_actor_add_child(priv->defaultSpellerGroup, priv->autoSuggestEntry);
		g_object_set( priv->autoSuggestEntry,"focus-cursor",FALSE, "reactive",FALSE,"text-editable",FALSE,"text-color","#535353",NULL);
		clutter_actor_set_child_above_sibling(priv->defaultSpellerGroup,priv->textBoxEntry, NULL);
		priv->pSuggTextBoxModel = create_text_box_model();
		thornbury_model_append(priv->pSuggTextBoxModel , 0 , " " , -1);
		clutter_actor_set_x( priv->autoSuggestEntry,5+priv->fltWidth );
		g_object_set(priv->autoSuggestEntry,"width", MILDENHALL_BLANK_RECT_WIDTH -(priv->fltWidth+priv->fltWidth1+4),NULL);
		g_object_set(priv->autoSuggestEntry,"model",priv->pSuggTextBoxModel,NULL);
	}
}

/**
 * v_default_speller_set_mark_text:
 * @pSelf: object reference
 * @enable_textsel: set to true/false for test selection
 *
 * set to enable/disable for test selection
 *
 */
void v_default_speller_set_mark_text(MildenhallSpellerDefaultEntry *pSelf, gboolean enable_textsel)
{
        MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE (pSelf);
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT :%s enable_textsel = %d \n", __FUNCTION__,enable_textsel);
	if(!MILDENHALL_IS_SPELLER_DEFAULT_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}
	priv->enable_textsel = enable_textsel;

	g_object_set( priv->textBoxEntry ,"select-text", enable_textsel, NULL );
}


void v_default_speller_column_changed_cb(gint col,gpointer userdata)
{
        MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE (userdata);
	if(!MILDENHALL_IS_SPELLER_DEFAULT_ENTRY(userdata))
	{
		g_warning("invalid instance\n");
		return;
	}
	priv->inColumnId = col;
}

/**
 * v_default_speller_set_model:
 * @pSelf: meta info header object reference
 * @pModel: model for the speller
 *
 * Model format:
 *	"left-icon-istext"  - gboolean
 *	"left-icon-width"   - gfloat
 *	"left-icon-info"    - GVariant*
 *	"right-icon-istext" - gboolean
 *	"right-icon-width"  - gfloat
 *	"right-icon-info"   - GVariant*
 * 	"default-text"     - gchar*
 **/

void v_default_speller_set_model(MildenhallSpellerDefaultEntry *pSelf, ThornburyModel *pModel)
{
        MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE (pSelf);
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_SPELLER_DEFAULT_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}

	if(NULL != priv->model)
	{
		g_signal_handlers_disconnect_by_func (priv->model, G_CALLBACK (v_default_speller_row_added_cb),   pSelf);
		g_signal_handlers_disconnect_by_func (priv->model, G_CALLBACK (v_default_speller_row_changed_cb), pSelf);
		g_signal_handlers_disconnect_by_func (priv->model, G_CALLBACK (v_default_speller_row_removed_cb), pSelf);
		//g_object_unref (priv->model);
		thornbury_list_model_destroy(priv->model);
		priv->model = NULL;
	}
	/* update the new model with signals */
	if ( NULL != pModel)
	{
		g_return_if_fail (G_IS_OBJECT (pModel));
		priv->model = g_object_ref(pModel);
		g_signal_connect (priv->model, "row-added",   G_CALLBACK (v_default_speller_row_added_cb),   pSelf );
		g_signal_connect (priv->model, "row-changed", G_CALLBACK (v_default_speller_row_changed_cb), pSelf );
		g_signal_connect (priv->model, "row-removed", G_CALLBACK (v_default_speller_row_removed_cb), pSelf );
		thornbury_model_register_column_changed_cb(priv->model,v_default_speller_column_changed_cb,pSelf);
	}
	v_default_speller_update_view(pSelf);
	g_object_notify (G_OBJECT(pSelf), "model");
}

/********************************************************
 * Function : v_mildenhall_speller_default_entry_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_default_entry_get_property (GObject *pObject, guint uinPropertyId,
		GValue *pValue, GParamSpec *pPspec)
{
        MildenhallSpellerDefaultEntry *pSelf = MILDENHALL_SPELLER_DEFAULT_ENTRY (pObject);
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT: %s\n", __FUNCTION__);
	switch (uinPropertyId)
	{
	case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_TEXT:
	{
		g_value_set_variant (pValue, pSelf->priv->argList);
		break;
	}
	case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_CLEAR_TEXT:
	{
		g_value_set_boolean (pValue, pSelf->priv->clearText);
		break;
	}
	case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_ENTRY_EDITABLE:
	{
		g_value_set_boolean (pValue, pSelf->priv->entry_editable);
		break;
	}
	case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_ENABLE_HISTORY:
	{
		g_value_set_boolean (pValue, pSelf->priv->enable_history);
		break;
	}
	case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_MODEL:
	{
		g_value_set_object (pValue, pSelf->priv->model);
		break;
	}
	case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_MARK_TEXT:
	{
		g_value_set_boolean(pValue, pSelf->priv->enable_textsel);
		break;
	}
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyId, pPspec);
	}
}

/********************************************************
 * Function : v_mildenhall_speller_default_entry_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_default_entry_set_property (GObject *pObject, guint uinPropertyId,
		const GValue *pValue, GParamSpec *pPspec)
{
        MildenhallSpellerDefaultEntry *pSelf = MILDENHALL_SPELLER_DEFAULT_ENTRY (pObject);
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT: %s\n", __FUNCTION__);
	switch (uinPropertyId)
	{
	case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_TEXT:
	{
		v_default_speller_set_text (pSelf, g_value_get_variant( pValue ));
		break;
	}
	case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_CLEAR_TEXT:
	{
		v_default_speller_set_clear_text (pSelf, g_value_get_boolean( pValue ));
		break;
	}
	case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_ENTRY_EDITABLE:
	{
		v_default_speller_set_entry_editable (pSelf, g_value_get_boolean( pValue ));
		break;
	}
	case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_ENABLE_HISTORY:
	{
		v_default_speller_set_enable_history (pSelf, g_value_get_boolean( pValue ));
		break;
	}
	case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_MARK_TEXT:
	{
		v_default_speller_set_mark_text (pSelf, g_value_get_boolean( pValue ));
		break;
	}
	case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_MODEL:
	{
		v_default_speller_set_model (pSelf, g_value_get_object ( pValue ));
		break;
	}
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyId, pPspec);
	}
}

/********************************************************
 * Function : v_mildenhall_speller_default_entry_dispose
 * Description: Dispose the spller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_default_entry_dispose (GObject *pObject)
{
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT: %s\n", __FUNCTION__);
	G_OBJECT_CLASS (mildenhall_speller_default_entry_parent_class)->dispose (pObject);
}

/********************************************************
 * Function : v_mildenhall_speller_default_entry_finalize
 * Description: Finalize the speller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_default_entry_finalize (GObject *pObject)
{
	MILDENHALL_DEFAULT_SPELLER_PRINT("MILDENHALL_DEFAULT_SPELLER_PRINT: %s\n", __FUNCTION__);
	G_OBJECT_CLASS (mildenhall_speller_default_entry_parent_class)->finalize (pObject);
}

gboolean cap_event_cb(ClutterActor *actor, ClutterEvent *event, gpointer data)
{
    return FALSE;
}

static void v_touch_begin_callback(ClutterActor *actor,gfloat x,gfloat y,gpointer data)
{
	MildenhallSpellerDefaultEntry *pSelf = MILDENHALL_SPELLER_DEFAULT_ENTRY(data);

	pSelf->priv->touchbegin_ypos = y;

	MILDENHALL_DEFAULT_SPELLER_PRINT("\nIn MILDENHALL speller x=%f and y= %f\n",x,pSelf->priv->touchbegin_ypos);

}

static void v_touch_end_callback(ClutterActor *actor,gfloat x,gfloat y,gpointer data)
{
	MildenhallSpellerDefaultEntry *pSelf = MILDENHALL_SPELLER_DEFAULT_ENTRY(data);

	pSelf->priv->touchend_ypos = y;

	MILDENHALL_DEFAULT_SPELLER_PRINT("\nIn MILDENHALL speller x=%f and y= %f\n",x,pSelf->priv->touchend_ypos);


	if((pSelf->priv->touchend_ypos - pSelf->priv->touchbegin_ypos) > 70)
	{
		g_signal_emit(pSelf, entry_action_signals[SIG_SLIDE_DOWN], 0,NULL );
	}
}

/********************************************************
 * Function : v_mildenhall_speller_default_entry_constructed
 * Description: the constructor function is called by g_object_new()
                to complete the object initialization after all the
                construction properties are set.
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_default_entry_constructed (GObject *pObject)
{
	MildenhallSpellerDefaultEntry *pSelf = MILDENHALL_SPELLER_DEFAULT_ENTRY(pObject);
	MildenhallSpellerDefaultEntryPrivate *priv = SPELLER_DEFAULT_ENTRY_PRIVATE(pSelf);

	ClutterColor lineColor = {0xFF,0xFF,0xFF,0x33};
	ClutterColor backGroundColor = {0x00,0x00,0x00,0xCC};
        ThornburyItemFactory *itemFactory = NULL;
        GObject *pTextObject = NULL;
        MildenhallTextBoxEntry *pTextBox = NULL;
        ThornburyItemFactory *ToggleLeftitemFactory = NULL;
        GObject *pToggleLeftObject = NULL;
        ThornburyItemFactory *ToggleRightitemFactory = NULL;
        GObject *pToggleRightObject = NULL;
        MildenhallToggleButton *leftToggle = NULL;
        MildenhallToggleButton *rightToggle = NULL;
        MILDENHALL_DEFAULT_SPELLER_PRINT (" \n %s %d \n", __FUNCTION__, __LINE__);

	//create a group for speller
	priv->defaultSpellerGroup = clutter_actor_new();
	clutter_actor_set_position( priv->defaultSpellerGroup ,0 ,0);
	clutter_actor_add_child(CLUTTER_ACTOR(pSelf), priv->defaultSpellerGroup);

	// create a blank rectangle to add entry items
	//priv->blankRect = clutter_rectangle_new_with_color(&backGroundColor);
	priv->blankRect = clutter_actor_new();
	clutter_actor_set_background_color ( priv->blankRect,&backGroundColor);
	clutter_actor_set_size(priv->blankRect,MILDENHALL_BLANK_RECT_WIDTH,MILDENHALL_BLANK_RECT_HEIGHT);
	clutter_actor_add_child(priv->defaultSpellerGroup, priv->blankRect);
	clutter_actor_set_position( priv->blankRect,0,0);
	clutter_actor_set_reactive( priv->blankRect,TRUE);

	itemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_TEXT_BOX_ENTRY, PKGDATADIR"/mildenhall_speller_text_box_prop.json");
	g_object_get(itemFactory, "object", &pTextObject, NULL);
	pTextBox = MILDENHALL_TEXT_BOX_ENTRY (pTextObject);
	priv->textBoxEntry = CLUTTER_ACTOR(pTextBox);
	clutter_actor_add_child(priv->defaultSpellerGroup, priv->textBoxEntry);
	g_object_set(priv->textBoxEntry, "focus-cursor",TRUE, NULL);
	g_signal_connect(priv->textBoxEntry,"entry-press",  G_CALLBACK(v_entry_pressed_cb),pObject);
	g_signal_connect(priv->textBoxEntry,"keypress-event",  G_CALLBACK(v_key_pressed_cb),pObject);
	g_signal_connect (priv->textBoxEntry, "captured-event", G_CALLBACK(cap_event_cb),pObject);
	g_signal_connect (priv->textBoxEntry, "touch-begin",G_CALLBACK(v_touch_begin_callback),pObject);
	g_signal_connect (priv->textBoxEntry, "touch-end",G_CALLBACK(v_touch_end_callback),pObject);
	//g_signal_connect_after(priv->textBoxEntry,"key-release-event",  G_CALLBACK(v_key_pressed_cb),pObject);

	ToggleLeftitemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_TOGGLE_BUTTON, PKGDATADIR"/mildenhall_speller_left_toggle_prop.json");
	g_object_get(ToggleLeftitemFactory, "object", &pToggleLeftObject, NULL);
	leftToggle = MILDENHALL_TOGGLE_BUTTON (pToggleLeftObject);
	priv->leftButton = CLUTTER_ACTOR(leftToggle);
	clutter_actor_add_child(priv->defaultSpellerGroup,priv->leftButton);

	ToggleRightitemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_TOGGLE_BUTTON, PKGDATADIR"/mildenhall_speller_right_toggle_prop.json");
	g_object_get(ToggleRightitemFactory, "object", &pToggleRightObject, NULL);
	rightToggle = MILDENHALL_TOGGLE_BUTTON (pToggleRightObject);
	priv->rightButton = CLUTTER_ACTOR(rightToggle);
	clutter_actor_add_child(priv->defaultSpellerGroup,priv->rightButton);

	g_signal_connect(priv->leftButton, "button-toggled",  G_CALLBACK(v_left_toggle_button_cb), pObject);
	g_signal_connect(priv->rightButton,"button-toggled",  G_CALLBACK(v_right_toggle_button_cb),pObject);

	//priv->leftLine= clutter_rectangle_new_with_color(&lineColor);
	priv->leftLine = clutter_actor_new();
	clutter_actor_set_background_color ( priv->leftLine,&lineColor);
	clutter_actor_add_child(priv->defaultSpellerGroup, priv->leftLine);
	clutter_actor_set_size(priv->leftLine,1,63);

	// priv->rightLine= clutter_rectangle_new_with_color(&lineColor);
	priv->rightLine = clutter_actor_new();
	clutter_actor_set_background_color ( priv->rightLine,&lineColor);
	clutter_actor_add_child(priv->defaultSpellerGroup, priv->rightLine);
	clutter_actor_set_size(priv->rightLine,1,63);

	priv->pTextBoxModel = create_text_box_model();
	priv->pRightModel   = create_toggle_model();
	priv->pLeftModel    = create_toggle_model();
}

/********************************************************
 * Function : mildenhall_speller_default_entry_class_init
 * Description: Class initialisation function for the pObject type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_speller_default_entry_class_init (MildenhallSpellerDefaultEntryClass *pKlass)
{
        GObjectClass *pObject_class = G_OBJECT_CLASS (pKlass);
        GParamSpec *pPspec = NULL;
	MILDENHALL_DEFAULT_SPELLER_PRINT(" \n %s %d \n",__FUNCTION__ ,__LINE__);
	g_type_class_add_private (pKlass, sizeof (MildenhallSpellerDefaultEntryPrivate));

	pObject_class->get_property = v_mildenhall_speller_default_entry_get_property;
	pObject_class->set_property = v_mildenhall_speller_default_entry_set_property;
	pObject_class->dispose      = v_mildenhall_speller_default_entry_dispose;
	pObject_class->finalize     = v_mildenhall_speller_default_entry_finalize;
	pObject_class->constructed  = v_mildenhall_speller_default_entry_constructed;

    /**
     * MildenhallSpellerDefaultEntry:text:
     *
     * this to set text
     */
	pPspec = g_param_spec_variant("text",
			"text",
			"text",
			G_VARIANT_TYPE("a{ss}"),NULL,
			(G_PARAM_READWRITE));
	g_object_class_install_property ( pObject_class, MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_TEXT, pPspec );

    /**
     * MildenhallSpellerDefaultEntry:clear-text:
     *
     * this to set to clear the text from entry
     */
	pPspec = g_param_spec_boolean ( "clear-text",
			"clear-text",
			"clear-text",
			TRUE,
			G_PARAM_READWRITE);
	g_object_class_install_property ( pObject_class, MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_CLEAR_TEXT, pPspec );

    /**
     * MildenhallSpellerDefaultEntry:entry-editable:
     *
     * this to set to enable/disable the editable
     */
	pPspec = g_param_spec_boolean ( "entry-editable",
			"entry-editable",
			"entry-editable",
			TRUE,
			G_PARAM_READWRITE);
	g_object_class_install_property ( pObject_class, MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_ENTRY_EDITABLE, pPspec );

    /**
     * MildenhallSpellerDefaultEntry:enable-history:
     *
     * this to set to enable/disable the history
     */
	pPspec = g_param_spec_boolean ( "enable-history",
			"enable-history",
			"enable-history",
			FALSE,
			G_PARAM_READWRITE);
	g_object_class_install_property ( pObject_class, MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_ENABLE_HISTORY, pPspec );

    /**
     * MildenhallSpellerDefaultEntry:select-text:
     *
     * this to set to enable/disable the text selection
     */
	pPspec = g_param_spec_boolean ( "mark-text",
				"mark-text",
				"mark-text",
				FALSE,
				G_PARAM_READWRITE);
	g_object_class_install_property ( pObject_class, MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_MARK_TEXT, pPspec );



    /**
     * MildenhallSpellerDefaultEntry:model:
     *
     * this to set to model to entry
     */
	pPspec = g_param_spec_object ("model",
			"model",
			"model",
			G_TYPE_OBJECT,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObject_class, MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_MODEL, pPspec);


    /**
     * MildenhallSpellerDefaultEntry::entry-action:
     *
     * ::entry-action is emitted when entry is pressed
     *
     */
	entry_action_signals[SIG_ENTRY_ACTION] = g_signal_new("entry-action",
			G_TYPE_FROM_CLASS (pObject_class),
			G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (MildenhallSpellerDefaultEntryClass, state_change),
			NULL, NULL,
			g_cclosure_marshal_generic,
			G_TYPE_NONE, 2,G_TYPE_STRING, G_TYPE_STRING);

    /**
     * MildenhallSpellerDefaultEntry::go-action:
     *
     * ::go-action is emitted when go button is pressed
     *
     */
	entry_action_signals[SIG_GO_ACTION] = g_signal_new("go-action",
			G_TYPE_FROM_CLASS (pObject_class),
			G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,0,
			NULL, NULL,
			g_cclosure_marshal_VOID__VOID,
			G_TYPE_NONE, 0);

	    /**
	     * MildenhallSpellerDefaultEntry::slide-down:
	     *
	     * ::slide down is emitted when touch threshold reaches
	     *
	     */
	entry_action_signals[SIG_SLIDE_DOWN] = g_signal_new("slide-down",
				G_TYPE_FROM_CLASS (pObject_class),
				G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,0,
				NULL, NULL,
				g_cclosure_marshal_VOID__VOID,
				G_TYPE_NONE, 0);

}

/********************************************************
 * Function : mildenhall_speller_default_entry_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_speller_default_entry_init (MildenhallSpellerDefaultEntry *pSelf)
{
	pSelf->priv = SPELLER_DEFAULT_ENTRY_PRIVATE (pSelf);
	pSelf->priv->blankRect    = NULL;
	pSelf->priv->textBoxEntry = NULL;
	pSelf->priv->autoSuggestEntry = NULL;
	pSelf->priv->leftButton   = NULL;
	pSelf->priv->rightButton  = NULL;
	pSelf->priv->leftLine     = NULL;
	pSelf->priv->rightLine    = NULL;
	pSelf->priv->defaultSpellerGroup = NULL;
	pSelf->priv->m_urlHistory = NULL;
	pSelf->priv->pSuggTextBoxModel = NULL;
	pSelf->priv->inColumnId = -1;
	pSelf->priv->enable_textsel = FALSE;
}

/**
 * mildenhall_speller_default_entry_new:
 *
 * Creates a speller entry  object
 *
 * Returns: (transfer full): speller entry object
 *
 */
ClutterActor *mildenhall_speller_default_entry_new (void)
{
	MILDENHALL_DEFAULT_SPELLER_PRINT(" \n %s %d \n",__FUNCTION__ ,__LINE__);
	return g_object_new (MILDENHALL_TYPE_SPELLER_DEFAULT_ENTRY, NULL);
}
