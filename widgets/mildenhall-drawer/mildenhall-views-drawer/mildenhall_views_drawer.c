/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_views_drawer
 * @title: MildenhallViewsDrawer
 * @short_description: provides a swipe-based side panel to provide
 *  a central location for navigation.
 * @see_also: #ClutterActor, #ThornburyModel
 *
 * #MildenhallViewsDrawer extends MildenhallDrawerBase and can be positioned
 * at any of the four edges of the content item.
 * This widget is same as MildenhallNaviDrawer but only differs in the way
 * it is positioned at the stage.
 *
 * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
 * How to create #MildenhallContextDrawer
 * |[<!-- language="C" -->
 *
 * ClutterActor *views_drawer = mildenhall_views_drawer_new ();
 * ThornburyModel *model =  (ThornburyModel*) thornbury_list_model_new (COLUMN_LAST,
 *                                                                     G_TYPE_STRING, NULL,
 *                                                                     G_TYPE_STRING, NULL,
 *                                                                     G_TYPE_STRING, NULL,
 *                                                                     G_TYPE_BOOLEAN, NULL,
 *                                                                     -1);
 * thornbury_model_append (model,
 *                        COLUMN_NAME, "MUSIC",
 *                        COLUMN_ICON, "/var/lib/MILDENHALL_extensions/themes/blau/test-drawer-base/icon_music_AC.png",
 *                        COLUMN_TOOLTIP_TEXT, "MUSIC",
 *                        COLUMN_REACTIVE, TRUE, -1);
 * thornbury_model_append (model,
 *                        COLUMN_NAME, "ARTISTS",
 *                        COLUMN_ICON, "/var/lib/MILDENHALL_extensions/themes/blau/test-drawer-base/icon_music_artists_AC.png",
 *                        COLUMN_TOOLTIP_TEXT, "ARTIST",
 *                        COLUMN_REACTIVE, TRUE, -1);
 * thornbury_model_append (model,
 *                        COLUMN_NAME, "INTERNET",
 *                        COLUMN_ICON, "/var/lib/MILDENHALL_extensions/themes/blau/test-drawer-base/icon_internet_AC.png",
 *                        COLUMN_TOOLTIP_TEXT, "INTERNET",
 *                        COLUMN_REACTIVE, TRUE, -1);
 * thornbury_model_append (model,
 *                        COLUMN_NAME, "ALBUMS",
 *                        COLUMN_ICON, "/var/lib/MILDENHALL_extensions/themes/blau/test-drawer-base/icon_music_AC.png",
 *                        COLUMN_TOOLTIP_TEXT, "ALBUMS",
 *                        COLUMN_REACTIVE, TRUE, -1);
 * g_object_set (views_drawer, "model", model, NULL);
 *
 * g_signal_connect (views_drawer, "drawer-open", G_CALLBACK (drawer_opened), NULL);
 * g_signal_connect (views_drawer, "drawer-close", G_CALLBACK (drawer_closed), NULL);
 * g_signal_connect (views_drawer, "drawer-button-released", G_CALLBACK (drawer_button_released), NULL);
 * g_signal_connect (views_drawer, "drawer-tooltip-hidden", G_CALLBACK (tooltip_hidden), NULL);
 * g_signal_connect (views_drawer, "drawer-tooltip-shown", G_CALLBACK (tooltip_shown), NULL);
 *
 * clutter_actor_add_child (stage, views_drawer);
 *
 * ]|
 *
 * Signal callback implementations for MildenhallViewsDrawer::drawer-open, MildenhallViewsDrawer::drawer-close,
 * MildenhallViewsDrawer::drawer-button-released, MildenhallViewsDrawer::drawer-tooltip-hidden and
 * MildenhallViewsDrawer::drawer-tooltip-shown
 *
 * |[<!-- language="C" -->
 * static void
 * drawer_opened (GObject *actor, gpointer user_data)
 * {
 *   g_debug ("DRAWER_WIDGET: %s", __FUNCTION__);
 * }
 *
 * static void
 * drawer_closed (GObject *actor, gpointer user_data)
 * {
 *   g_debug ("DRAWER_WIDGET: %s", __FUNCTION__);
 * }
 *
 * static void
 * tooltip_hidden (GObject *actor, gpointer user_data)
 * {
 *   g_debug ("DRAWER_WIDGET: %s", __FUNCTION__);
 * }
 *
 * static void
 * tooltip_shown (GObject *actor, gpointer user_data)
 * {
 *   g_debug ("DRAWER_WIDGET: %s", __FUNCTION__);
 * }
 *
 * static void
 * drawer_button_released (GObject *actor, gchar *name, gpointer user_data)
 * {
 *   g_debug ("DRAWER_WIDGET: %s", __FUNCTION__);
 *   g_debug ("DRAWER_WIDGET: button released = %s", name);
 * }
 *
 * ]|
 *
 * Since0.3.0
 */

#include "mildenhall_views_drawer.h"

G_DEFINE_TYPE (MildenhallViewsDrawer, mildenhall_views_drawer, MILDENHALL_DRAWER_TYPE_BASE)

#define MILDENHALL_VIEWS_DRAWER_GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_VIEWS_DRAWER, MildenhallViewsDrawerPrivate))

#define VIEWS_DRAWER_POSITION           660.0,51.0


struct _MildenhallViewsDrawerPrivate
{
	gboolean bDummy;
};


/**
 * mildenhall_views_drawer_new:
 *
 * Creates a views drawer object
 *
* Returns: (transfer full): Mildenhall Views Drawer object
 */
ClutterActor *mildenhall_views_drawer_new (void)
{
  return g_object_new (MILDENHALL_TYPE_VIEWS_DRAWER, NULL);
}


/*****************************************************************************************************
 * Internal functions
 *****************************************************************************************************/

/********************************************************
 * Function : mildenhall_views_drawer_get_property
 * Description: Get a property value 
 * Parameters: The object reference, property Id, 
 *              return location for where the property 
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_views_drawer_get_property (GObject *pObject, guint uinPropertyID, GValue *pValue, GParamSpec *pspec)
{
        if(!MILDENHALL_IS_VIEWS_DRAWER(pObject))
        {
                g_warning("invalid drawer object\n");
                return;
        }

        switch (uinPropertyID)
        {
		default:
                        G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
        }
}

/********************************************************
 * Function : mildenhall_views_drawer_set_property
 * Description: set a property value 
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_views_drawer_set_property (GObject *pObject, guint uinPropertyID, const GValue *pValue, GParamSpec *pspec)
{
        if(! MILDENHALL_IS_VIEWS_DRAWER(pObject))
        {
                g_warning("invalid drawer object\n");
                return;
        }

        switch (uinPropertyID)
        {
		 default:
                        G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
        }
}
/********************************************************
 * Function : mildenhall_button_drawer_dispose
 * Description: Dispose the mildenhall views drawer  object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_views_drawer_dispose (GObject *pObject)
{
        G_OBJECT_CLASS (mildenhall_views_drawer_parent_class)->dispose (pObject);

        /* TBD: free all memory */
        //MildenhallViewsDrawerPrivate *priv = MILDENHALL_VIEWS_DRAWER_GET_PRIVATE(pObject);

}

/********************************************************
 * Function : mildenhall_views_drawer_finalize
 * Description: Finalize the mildenhall views drawer object 
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_views_drawer_finalize (GObject *pObject)
{
        G_OBJECT_CLASS (mildenhall_views_drawer_parent_class)->finalize (pObject);
}

/********************************************************
 * Function : mildenhall_views_drawer_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_views_drawer_class_init (MildenhallViewsDrawerClass *klass)
{
        GObjectClass *pObjectClass = G_OBJECT_CLASS (klass);

        g_type_class_add_private (klass, sizeof (MildenhallViewsDrawerPrivate));

        pObjectClass->get_property = mildenhall_views_drawer_get_property;
        pObjectClass->set_property = mildenhall_views_drawer_set_property;
        pObjectClass->dispose = mildenhall_views_drawer_dispose;
        pObjectClass->finalize = mildenhall_views_drawer_finalize;
}

/********************************************************
 * Function : mildenhall_views_drawer_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void mildenhall_views_drawer_init (MildenhallViewsDrawer *self)
{
        self->priv = MILDENHALL_VIEWS_DRAWER_GET_PRIVATE (self);
	g_object_set(self, "type", MILDENHALL_VIEWS_DRAWER, NULL);
	clutter_actor_set_position(CLUTTER_ACTOR(self), VIEWS_DRAWER_POSITION);
}
