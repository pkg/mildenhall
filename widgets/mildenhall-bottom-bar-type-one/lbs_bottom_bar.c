/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* lbs_bottom_bar.c
 *
 *
 * lbs_bottom_bar.c */

#include "lbs_bottom_bar.h"

#include <thornbury/thornbury.h>

#include "liblightwood-glowshader.h"

/**
 * _enRatingBottomBarProperty:
 * property enums of Lbs Bottom Bar
 */
typedef enum _enLbsBottomBarProperty enLbsBottomBarProperty;
enum _enLbsBottomBarProperty
{
	LBS_PROP_BOTTOM_BAR_FIRST,
	LBS_PROP_BOTTOM_BAR_WIDTH,
	LBS_PROP_BOTTOM_BAR_HEIGHT,
	LBS_PROP_BOTTOM_BAR_X,
	LBS_PROP_BOTTOM_BAR_Y,
	LBS_PROP_BOTTOM_BAR_LEFT_TEXT_FONT,
	LBS_PROP_BOTTOM_BAR_LEFT_TEXT_COLOR,
	LBS_PROP_BOTTOM_BAR_RIGHT_TEXT_FONT,
	LBS_PROP_BOTTOM_BAR_RIGHT_TEXT_COLOR,
	LBS_PROP_BOTTOM_BAR_MIDDLE_TEXT_FONT,
	LBS_PROP_BOTTOM_BAR_MIDDLE_TEXT_COLOR,
	LBS_PROP_BOTTOM_BAR_BACKGROUND,
	LBS_PROP_BOTTOM_BAR_LEFTTEXT,
	LBS_PROP_BOTTOM_BAR_RIGHTTEXT,
	LBS_PROP_BOTTOM_BAR_MIDDLETEXT,
	LBS_PROP_BOTTOM_BAR_LAST
};


#define LBS_BAR_LEFT_TEXT_X 	  "left-text-x"
#define LBS_BAR_LEFT_TEXT_Y 	  "left-text-y"
#define LBS_BAR_LEFT_TEXT_MAX 	  "left-text-max-width"
#define LBS_BAR_LEFT_TEXT_FONT 	  "left-text-default-font"
#define LBS_BAR_LEFT_TEXT_COLOR   "left-text-default-color"
#define LBS_BAR_RIGHT_TEXT_X 	  "right-text-x"
#define LBS_BAR_RIGHT_TEXT_Y 	  "right-text-y"
#define LBS_BAR_RIGHT_TEXT_MAX 	  "right-text-max-width"
#define LBS_BAR_RIGHT_TEXT_FONT   "right-text-default-font"
#define LBS_BAR_RIGHT_TEXT_COLOR  "right-text-default-color"
#define LBS_BAR_MIDDLE_TEXT_X 	  "middle-text-x"
#define LBS_BAR_MIDDLE_TEXT_Y	  "middle-text-y"
#define LBS_BAR_MIDDLE_TEXT_MAX   "middle-text-max-width"
#define LBS_BAR_MIDDLE_TEXT_FONT  "middle-text-default-font"
#define LBS_BAR_MIDDLE_TEXT_COLOR "middle-text-default-color"
#define LBS_BAR_BACKGROUND   	  "default-background"

#define LBS_BOTTOM_BAR_PRINT( a ...)	//g_print(a);

typedef struct _MildenhallLbsBottomBarPrivate
{
	ThornburyModel *pModel ;

	ClutterActor *pLeftText;
	ClutterActor *pRightText;
	ClutterActor *pMiddleText;

	ClutterEffect *glow_effect;
	ClutterEffect *glow_effect1;
	ClutterEffect *glow_effect2;

	ClutterColor pLeftTextColor;
	ClutterColor pRightTextColor;
	ClutterColor pMiddleTextColor;
	ClutterColor BackgroundColor;

	gchar *pRightTextFont;
	gchar *pLeftTextFont;
	gchar *pMiddleTextFont;
	gchar *middleText;

	gfloat fltHeight;
	gfloat fltWidth;
	gfloat fltX;
	gfloat fltY;

	GHashTable *pPrivHash;
} MildenhallLbsBottomBarPrivate;

struct _MildenhallLbsBottomBar
{
  ClutterActor parent;
};

G_DEFINE_TYPE_WITH_PRIVATE (MildenhallLbsBottomBar, mildenhall_lbs_bottom_bar, CLUTTER_TYPE_ACTOR)

/**
 * mildenhall_lbs_bottom_bar_set_lefttext:
 * @self: Lbs Bottom Bar object reference
 * @text: new value
 *
 * set lbs bottom bar lefttext
 *
 **/
void
mildenhall_lbs_bottom_bar_set_lefttext (MildenhallLbsBottomBar *self, const gchar *text)
{
  MildenhallLbsBottomBarPrivate *priv = mildenhall_lbs_bottom_bar_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_LBS_BOTTOM_BAR (self));

  if(!text)
    return;

  clutter_text_set_text (CLUTTER_TEXT (priv->pLeftText), text);

  if (clutter_actor_get_effect (priv->pLeftText, "glow") != NULL)
    return;

  clutter_actor_add_effect_with_name (priv->pLeftText, "glow", priv->glow_effect);
}

/**
 * mildenhall_lbs_bottom_bar_set_righttext:
 * @self: Lbs Bottom Bar object reference
 * @text: new value
 *
 * set lbs bottom bar righttext
 *
 **/
void
mildenhall_lbs_bottom_bar_set_righttext (MildenhallLbsBottomBar *self, const gchar *text)
{
  MildenhallLbsBottomBarPrivate *priv = mildenhall_lbs_bottom_bar_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_LBS_BOTTOM_BAR (self));

  if(!text)
    return;

  clutter_text_set_text (CLUTTER_TEXT(priv->pRightText), text);

  if (clutter_actor_get_effect (priv->pRightText, "glow") != NULL)
    return;
  clutter_actor_add_effect_with_name (priv->pRightText, "glow", priv->glow_effect1);
}

/**
 * mildenhall_lbs_bottom_bar_set_middletext:
 * @self: Lbs Bottom Bar object reference
 * @text: new value
 *
 * set lbs bottom bar middletext
 *
 **/
void
mildenhall_lbs_bottom_bar_set_middletext (MildenhallLbsBottomBar *self, const gchar *text)
{
  MildenhallLbsBottomBarPrivate *priv = mildenhall_lbs_bottom_bar_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_LBS_BOTTOM_BAR (self));

  if(!text)
    return;

  g_free (priv->middleText);
  priv->middleText = g_strdup (text);

  if(priv->fltWidth > 300)
    {
      clutter_text_set_text (CLUTTER_TEXT (priv->pMiddleText), text);
    }

  if (clutter_actor_get_effect (priv->pMiddleText, "glow") != NULL)
    return;

  clutter_actor_add_effect_with_name (priv->pMiddleText, "glow", priv->glow_effect2);
}


/**
 * mildenhall_lbs_bottom_bar_set_width:
 * @self : Lbs Bottom Bar pObject reference
 * @width : new value
 *
 * set lbs bottom bar  width internally
 *
 **/
void
mildenhall_lbs_bottom_bar_set_width (MildenhallLbsBottomBar *self, gfloat width)
{
  gfloat rightX,rightY;
  MildenhallLbsBottomBarPrivate *priv = mildenhall_lbs_bottom_bar_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_LBS_BOTTOM_BAR (self));

  if (priv->fltWidth != width)
    {
      clutter_actor_set_width (CLUTTER_ACTOR (self), width);
      priv->fltWidth = width;
    }

  if(!priv->pRightText)
    return;

  sscanf ((gchar*) g_hash_table_lookup(priv->pPrivHash, LBS_BAR_RIGHT_TEXT_X), "%f", &rightX);
  sscanf ((gchar*) g_hash_table_lookup(priv->pPrivHash, LBS_BAR_RIGHT_TEXT_Y), "%f", &rightY);

  if(priv->fltWidth < 300)
    {
      clutter_text_set_text (CLUTTER_TEXT (priv->pMiddleText), "");
      clutter_actor_set_position (priv->pRightText, (priv->fltWidth - 45.0), rightY);
    }
  else
    {
      clutter_text_set_text (CLUTTER_TEXT (priv->pMiddleText), priv->middleText);
      clutter_actor_set_position (priv->pRightText, rightX, rightY);
      if (clutter_actor_get_effect (priv->pMiddleText, "glow") != NULL)
        return;
      clutter_actor_add_effect_with_name (priv->pMiddleText, "glow", priv->glow_effect2);
    }
}


/**
 * mildenhall_lbs_bottom_bar_set_height:
 * @self: Lbs Bottom Bar pObject reference
 * @height: new value
 *
 * set lbs bottom bar height internally
 *
 **/
void
mildenhall_lbs_bottom_bar_set_height (MildenhallLbsBottomBar *self, gfloat height)
{
  MildenhallLbsBottomBarPrivate *priv = mildenhall_lbs_bottom_bar_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_LBS_BOTTOM_BAR (self));

  if (priv->fltHeight != height)
    {
      clutter_actor_set_height (CLUTTER_ACTOR (self), height);
      priv->fltHeight = height;
    }
}

/**
 * mildenhall_lbs_bottom_bar_set_x:
 * @self : Lbs Bottom Bar pObject reference
 * @posx : new value
 *
 * set lbs bottom bar x internally
 *
 **/
void
mildenhall_lbs_bottom_bar_set_x (MildenhallLbsBottomBar *self, gfloat posx)
{
  MildenhallLbsBottomBarPrivate *priv = mildenhall_lbs_bottom_bar_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_LBS_BOTTOM_BAR (self));

  if(priv->fltX != posx)
    {
      clutter_actor_set_x (CLUTTER_ACTOR (self), posx);
      priv->fltX = posx;
    }
}

/**
 * mildenhall_lbs_bottom_bar_set_y:
 * @self : Lbs Bottom Bar pObject reference
 * @posy : new value
 *
 * set lbs bottom bar y internally
 *
 **/
void
mildenhall_lbs_bottom_bar_set_y (MildenhallLbsBottomBar *self, gfloat posy)
{
  MildenhallLbsBottomBarPrivate *priv = mildenhall_lbs_bottom_bar_get_instance_private (self);

  if(priv->fltY != posy)
    {
      clutter_actor_set_y (CLUTTER_ACTOR (self), posy);
      priv->fltY = posy;
    }
}


/********************************************************
 * Function : v_lbs_bottom_bar_add_style_to_hash
 * Description: maintain style in priv hash
 * Parameter :  MildenhallLbsBottomBarPrivate*, *pKey, pValue
 * Return value: void
 ********************************************************/
static void
v_lbs_bottom_bar_add_style_to_hash (MildenhallLbsBottomBarPrivate *priv, gchar *pKey, gpointer pValue)
{

	if (NULL != pKey || NULL != pValue)
	{
		/* maintain key and value pair for style name and its value */
		if (G_VALUE_HOLDS_DOUBLE(pValue))
		{
			g_hash_table_insert(priv->pPrivHash, pKey,
					g_strdup_printf("%f",g_value_get_double(pValue)));

			LBS_BOTTOM_BAR_PRINT("............ %f...........\n ", g_value_get_double(pValue));
		}
		else if (G_VALUE_HOLDS_STRING(pValue))
		{
			g_hash_table_insert(priv->pPrivHash, pKey,
					(gpointer) g_value_get_string(pValue));
			LBS_BOTTOM_BAR_PRINT("............ %s...........\n ", g_value_get_string(pValue));
		}
	}
}

/********************************************************
 * Function : v_lbs_bottom_bar_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void v_lbs_bottom_bar_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
	gchar *pStyleKey = g_strdup(pKey);
	MildenhallLbsBottomBar *pLbsBottomBar = pUserData;
	MildenhallLbsBottomBarPrivate *priv = mildenhall_lbs_bottom_bar_get_instance_private (pLbsBottomBar);

	if(g_strcmp0(pStyleKey, LBS_BAR_LEFT_TEXT_X) == 0)
	{
		v_lbs_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, LBS_BAR_LEFT_TEXT_Y) == 0)
	{
		v_lbs_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, LBS_BAR_LEFT_TEXT_MAX) == 0)
	{
		v_lbs_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, LBS_BAR_LEFT_TEXT_FONT) == 0)
	{
		v_lbs_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, LBS_BAR_LEFT_TEXT_COLOR) == 0)
	{
		clutter_color_from_string(&priv->pLeftTextColor, g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, LBS_BAR_RIGHT_TEXT_X) == 0)
	{
		v_lbs_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, LBS_BAR_RIGHT_TEXT_Y) == 0)
	{
		v_lbs_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, LBS_BAR_RIGHT_TEXT_MAX) == 0)
	{
		v_lbs_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey , LBS_BAR_RIGHT_TEXT_FONT ) == 0)
	{
		v_lbs_bottom_bar_add_style_to_hash(priv,pStyleKey , pValue);
	}
	else if(g_strcmp0(pStyleKey, LBS_BAR_RIGHT_TEXT_COLOR) == 0)
	{
		clutter_color_from_string(&priv->pRightTextColor , g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, LBS_BAR_MIDDLE_TEXT_X) == 0)
	{
		v_lbs_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, LBS_BAR_MIDDLE_TEXT_Y) == 0)
	{
		v_lbs_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, LBS_BAR_MIDDLE_TEXT_MAX) == 0)
	{
		v_lbs_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, LBS_BAR_MIDDLE_TEXT_FONT) == 0)
	{
		v_lbs_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, LBS_BAR_MIDDLE_TEXT_COLOR) == 0)
	{
		clutter_color_from_string(&priv->pMiddleTextColor, g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, LBS_BAR_BACKGROUND) == 0)
	{

		LBS_BOTTOM_BAR_PRINT(" LBS_BAR_BACKGROUND %s \n",g_value_get_string(pValue));
		clutter_color_from_string(&priv->BackgroundColor,   g_value_get_string(pValue) );

		clutter_actor_set_background_color (CLUTTER_ACTOR(pLbsBottomBar), &priv->BackgroundColor);
	}
}

/********************************************************
 * Function : lbs_bottom_bar_get_property
 * Description: Get property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void lbs_bottom_bar_get_property (GObject *pObject,guint property_id,GValue *pValue,GParamSpec *pSpec)
{
	switch (property_id)
	{
		case LBS_PROP_BOTTOM_BAR_WIDTH:
			break;
		case LBS_PROP_BOTTOM_BAR_HEIGHT:
			break;
		case LBS_PROP_BOTTOM_BAR_X:
			break;
		case LBS_PROP_BOTTOM_BAR_Y:
			break;
		case LBS_PROP_BOTTOM_BAR_LEFT_TEXT_FONT:
			break;
		case LBS_PROP_BOTTOM_BAR_LEFT_TEXT_COLOR:
			break;
		case LBS_PROP_BOTTOM_BAR_RIGHT_TEXT_FONT:
			break;
		case LBS_PROP_BOTTOM_BAR_RIGHT_TEXT_COLOR:
			break;
		case LBS_PROP_BOTTOM_BAR_MIDDLE_TEXT_FONT:
			break;
		case LBS_PROP_BOTTOM_BAR_MIDDLE_TEXT_COLOR:
			break;
		case LBS_PROP_BOTTOM_BAR_BACKGROUND:
			break;
		case LBS_PROP_BOTTOM_BAR_LEFTTEXT:
			break;
		case LBS_PROP_BOTTOM_BAR_RIGHTTEXT:
			break;
		case LBS_PROP_BOTTOM_BAR_MIDDLETEXT:
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, property_id, pSpec);
			break;
	}
}


/********************************************************
 * Function : v_lbs_bottom_bar_set_left_text_font
 * Description: set the left text font
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void
v_lbs_bottom_bar_set_left_text_font (MildenhallLbsBottomBar *self, const GValue *pValue)
{
  MildenhallLbsBottomBarPrivate *priv = mildenhall_lbs_bottom_bar_get_instance_private (self);

  g_free (priv->pLeftTextFont);
  priv->pLeftTextFont = g_value_dup_string (pValue);
  clutter_text_set_font_name (CLUTTER_TEXT (priv->pLeftText), priv->pLeftTextFont );
}

/********************************************************
 * Function : v_lbs_bottom_bar_set_left_text_color
 * Description: set the left text color
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void
v_lbs_bottom_bar_set_left_text_color (MildenhallLbsBottomBar *self, const GValue *pValue)
{
  MildenhallLbsBottomBarPrivate *priv = mildenhall_lbs_bottom_bar_get_instance_private (self);
  gchar *pTextColor = g_value_dup_string (pValue);

  clutter_color_from_string( &priv->pLeftTextColor , pTextColor);

  if (NULL != priv->pLeftText )
    {
      clutter_text_set_color (CLUTTER_TEXT (priv->pLeftText), &priv->pLeftTextColor);
    }

  g_free (pTextColor);
}

/********************************************************
 * Function : v_lbs_bottom_bar_set_right_text_font
 * Description: set the right text font
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void
v_lbs_bottom_bar_set_right_text_font (MildenhallLbsBottomBar *self, const GValue *pValue)
{
  MildenhallLbsBottomBarPrivate *priv = mildenhall_lbs_bottom_bar_get_instance_private (self);

  g_free (priv->pRightTextFont);
  priv->pRightTextFont = g_value_dup_string (pValue);
  if (NULL != priv->pRightText )
    {
      clutter_text_set_font_name (CLUTTER_TEXT (priv->pRightText), priv->pRightTextFont);
    }
}

/********************************************************
 * Function : v_lbs_bottom_bar_set_right_text_color
 * Description: set the right text color
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void
v_lbs_bottom_bar_set_right_text_color (MildenhallLbsBottomBar *self, const GValue *pValue)
{
  MildenhallLbsBottomBarPrivate *priv = mildenhall_lbs_bottom_bar_get_instance_private (self);
  gchar *pTextColor = g_value_dup_string (pValue);
  clutter_color_from_string (&priv->pRightTextColor, pTextColor);

  if (NULL != priv->pRightText)
    {
      clutter_text_set_color (CLUTTER_TEXT (priv->pRightText), &priv->pRightTextColor);
    }

  g_free(pTextColor);
}

/********************************************************
 * Function : v_lbs_bottom_bar_set_middle_text_font
 * Description: set the middle text font
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void
v_lbs_bottom_bar_set_middle_text_font (MildenhallLbsBottomBar *self, const GValue *pValue)
{
  MildenhallLbsBottomBarPrivate *priv = mildenhall_lbs_bottom_bar_get_instance_private (self);

  g_free (priv->pMiddleTextFont);
  priv->pMiddleTextFont = g_value_dup_string (pValue);

  if (NULL != priv->pMiddleText)
    {
      clutter_text_set_font_name (CLUTTER_TEXT (priv->pMiddleText), priv->pMiddleTextFont);
    }
}

/********************************************************
 * Function : v_lbs_bottom_bar_set_middle_text_color
 * Description: set the middle text color
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void
v_lbs_bottom_bar_set_middle_text_color (MildenhallLbsBottomBar *self, const GValue *pValue)
{
  MildenhallLbsBottomBarPrivate *priv = mildenhall_lbs_bottom_bar_get_instance_private (self);
  gchar *pTextColor = g_value_dup_string (pValue);

  clutter_color_from_string (&priv->pMiddleTextColor, pTextColor);

  if (NULL != priv->pMiddleText)
    {
      clutter_text_set_color(CLUTTER_TEXT(priv->pMiddleText ),&priv->pMiddleTextColor );
    }

  g_free(pTextColor);
}

/********************************************************
 * Function : lbs_bottom_bar_dispose
 * Description: Dispose the roller item object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void
mildenhall_lbs_bottom_bar_dispose (GObject *object)
{
  MildenhallLbsBottomBar *self = MILDENHALL_LBS_BOTTOM_BAR (object);
  MildenhallLbsBottomBarPrivate *priv = mildenhall_lbs_bottom_bar_get_instance_private (self);

  g_clear_pointer (&priv->pRightTextFont, g_free);
  g_clear_pointer (&priv->pLeftTextFont, g_free);
  g_clear_pointer (&priv->pMiddleTextFont, g_free);
  g_clear_pointer (&priv->middleText, g_free);

  g_clear_object (&priv->glow_effect);
  g_clear_object (&priv->glow_effect1);
  g_clear_object (&priv->glow_effect2);

  G_OBJECT_CLASS (mildenhall_lbs_bottom_bar_parent_class)->dispose (object);
}

/********************************************************
 * Function : lbs_bottom_bar_finalize
 * Description: finalize the roller item object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void
mildenhall_lbs_bottom_bar_finalize (GObject *object)
{
  G_OBJECT_CLASS (mildenhall_lbs_bottom_bar_parent_class)->finalize (object);
}

/********************************************************
 * Function : lbs_bottom_bar_set_property
 * Description: Set property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void
lbs_bottom_bar_set_property (GObject *object,
                             guint property_id,
                             const GValue *value,
                             GParamSpec *pspec)
{
  MildenhallLbsBottomBar *self = MILDENHALL_LBS_BOTTOM_BAR (object);
  MildenhallLbsBottomBarPrivate *priv = mildenhall_lbs_bottom_bar_get_instance_private (self);

  switch (property_id)
    {
    case LBS_PROP_BOTTOM_BAR_WIDTH:
      mildenhall_lbs_bottom_bar_set_width (self, g_value_get_float (value));
      break;
    case LBS_PROP_BOTTOM_BAR_HEIGHT:
      mildenhall_lbs_bottom_bar_set_height (self, g_value_get_float (value));
      break;
    case LBS_PROP_BOTTOM_BAR_X:
      mildenhall_lbs_bottom_bar_set_x (self, g_value_get_float (value));
      break;
    case LBS_PROP_BOTTOM_BAR_Y:
      mildenhall_lbs_bottom_bar_set_y (self, g_value_get_float (value));
      break;
    case LBS_PROP_BOTTOM_BAR_LEFT_TEXT_FONT:
      v_lbs_bottom_bar_set_left_text_font (self, value);
      break;
    case LBS_PROP_BOTTOM_BAR_LEFT_TEXT_COLOR:
      v_lbs_bottom_bar_set_left_text_color (self, value);
      break;
    case LBS_PROP_BOTTOM_BAR_RIGHT_TEXT_FONT:
      v_lbs_bottom_bar_set_right_text_font (self, value);
      break;
    case LBS_PROP_BOTTOM_BAR_RIGHT_TEXT_COLOR:
      v_lbs_bottom_bar_set_right_text_color (self, value);
      break;
    case LBS_PROP_BOTTOM_BAR_MIDDLE_TEXT_FONT:
      v_lbs_bottom_bar_set_middle_text_font (self, value);
      break;
    case LBS_PROP_BOTTOM_BAR_MIDDLE_TEXT_COLOR:
      v_lbs_bottom_bar_set_middle_text_color (self, value);
      break;
    case LBS_PROP_BOTTOM_BAR_BACKGROUND:
      clutter_color_from_string (&priv->BackgroundColor, g_value_get_string (value));
      clutter_actor_set_background_color (CLUTTER_ACTOR (self), &priv->BackgroundColor);
      break;
    case LBS_PROP_BOTTOM_BAR_LEFTTEXT:
      mildenhall_lbs_bottom_bar_set_lefttext (self, g_value_get_string (value));
      break;
    case LBS_PROP_BOTTOM_BAR_RIGHTTEXT:
      mildenhall_lbs_bottom_bar_set_righttext (self, g_value_get_string (value));
      break;
    case LBS_PROP_BOTTOM_BAR_MIDDLETEXT:
      mildenhall_lbs_bottom_bar_set_middletext (self, g_value_get_string (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

/********************************************************
 * Function : lbs_bottom_bar_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void
mildenhall_lbs_bottom_bar_class_init (MildenhallLbsBottomBarClass *klass)
{
	GObjectClass *pObjectClass = G_OBJECT_CLASS (klass);

	GParamSpec *pSpec = NULL;

	pObjectClass->get_property = lbs_bottom_bar_get_property;
	pObjectClass->set_property = lbs_bottom_bar_set_property;
	pObjectClass->dispose = mildenhall_lbs_bottom_bar_dispose;
	pObjectClass->finalize = mildenhall_lbs_bottom_bar_finalize;

	/**
	 * MildenhallLbsBottomBar:width:
	 *
	 * Width of the Lbs Bottom Bar
	 */
	pSpec = g_param_spec_float ("width",
			"Width",
			"Width of the Lbs Bottom Bar",
			0.0, G_MAXFLOAT,
			0.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, LBS_PROP_BOTTOM_BAR_WIDTH, pSpec);

	/**
	 * MildenhallLbsBottomBar:height:
	 *
	 * height of the Lbs Bottom Bar
	 */
	pSpec = g_param_spec_float ("height",
			"Height",
			"Height of the Lbs Bottom Bar",
			0.0, G_MAXFLOAT,
			0.0,
			G_PARAM_READWRITE);
	g_object_class_install_property ( pObjectClass, LBS_PROP_BOTTOM_BAR_HEIGHT, pSpec);

	/**
	 * MildenhallLbsBottomBar:x:
	 *
	 * x of the Lbs Bottom Bar
	 */
	pSpec = g_param_spec_float ("x",
			"x",
			"x of the Lbs Bottom Bar",
			0.0, G_MAXFLOAT,
			0.0,
			G_PARAM_READWRITE);
	g_object_class_install_property ( pObjectClass, LBS_PROP_BOTTOM_BAR_X, pSpec);

	/**
	 * MildenhallLbsBottomBar:y:
	 *
	 * y of the Lbs Bottom Bar
	 */
	pSpec = g_param_spec_float ("y",
			"y",
			"y of the Lbs Bottom Bar",
			0.0, G_MAXFLOAT,
			0.0,
			G_PARAM_READWRITE);
	g_object_class_install_property ( pObjectClass, LBS_PROP_BOTTOM_BAR_Y, pSpec);

	/**
	 * MildenhallLbsBottomBar:background:
	 *
	 * background
	 * Default: #00000033
	 */
	pSpec = g_param_spec_string("background", "background",
			"background color for the entire widget",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, LBS_PROP_BOTTOM_BAR_BACKGROUND, pSpec);


	/**
	 * MildenhallLbsBottomBar:left-text-font:
	 *
	 * left-text-font
	 * Default: DejaVuSansCondensed 24px
	 */
	pSpec = g_param_spec_string("left-text-font", "left-text-font",
			"font type for the text in left side",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, LBS_PROP_BOTTOM_BAR_LEFT_TEXT_FONT, pSpec);

	/**
	 * MildenhallLbsBottomBar:left-text-color:
	 *
	 * left-text-color
	 * Default: "#98A9B3FF"
	 */
	pSpec = g_param_spec_string("left-text-color", "left-text-color",
			"color for the text in left side",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, LBS_PROP_BOTTOM_BAR_LEFT_TEXT_COLOR, pSpec);

	/**
	 * MildenhallLbsBottomBar:right-text-font:
	 *
	 * right-text-font
	 * Default: DejaVuSansCondensed 24px
	 */
	pSpec = g_param_spec_string("right-text-font", "right-text-font",
			"font type for the text in right side",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, LBS_PROP_BOTTOM_BAR_RIGHT_TEXT_FONT, pSpec);

	/**
	 * MildenhallBottomBar:right-text-color:
	 *
	 * right-text-color
	 * Default: "#98A9B3FF"
	 */
	pSpec = g_param_spec_string("right-text-color", "right-text-color",
			"color for the text in right side",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, LBS_PROP_BOTTOM_BAR_RIGHT_TEXT_COLOR, pSpec);

	/**
	 * MildenhallLbsBottomBar:middle-text-font:
	 *
	 * middle-text-font
	 * Default: DejaVuSansCondensed 24px
	 */
	pSpec = g_param_spec_string("middle-text-font", "middle-text-font",
			"font type for the text in middle side",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, LBS_PROP_BOTTOM_BAR_MIDDLE_TEXT_FONT, pSpec);

	/**
	 * MildenhallLbsBottomBar:middle-text-color:
	 *
	 * middle-text-color
	 * Default: "#98A9B3FF"
	 */
	pSpec = g_param_spec_string("middle-text-color", "middle-text-color",
			"color for the text in middle side",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, LBS_PROP_BOTTOM_BAR_MIDDLE_TEXT_COLOR, pSpec);

	/**
	 * MildenhallLbsBottomBar:left-text:
	 *
	 * left-text
	 *
	 */
	pSpec = g_param_spec_string("left-text", "left-text",
			"text in left",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, LBS_PROP_BOTTOM_BAR_LEFTTEXT, pSpec);

	/**
	 * MildenhallLbsBottomBar:right-text:
	 *
	 * right-text
	 *
	 */
	pSpec = g_param_spec_string("right-text", "right-text",
			"text in right",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, LBS_PROP_BOTTOM_BAR_RIGHTTEXT, pSpec);

	/**
	 * MildenhallLbsBottomBar:middle-text:
	 *
	 * middle-text
	 *
	 */
	pSpec = g_param_spec_string("middle-text", "middle-text",
			"text in middle",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, LBS_PROP_BOTTOM_BAR_MIDDLETEXT, pSpec);


}

static void
mildenhall_lbs_bottom_bar_init (MildenhallLbsBottomBar *self)
{
  MildenhallLbsBottomBarPrivate *priv = mildenhall_lbs_bottom_bar_get_instance_private (self);
  gfloat leftX, leftY, rightX, rightY, midX, midY, leftWid, rightWid, midWid;

	/* get the hash table for style properties */
	GHashTable *pStyleHash = thornbury_style_set(PKGDATADIR"/lbs_bottom_bar_style.json");

	/* pares the hash for styles */
	if(NULL !=  pStyleHash)
	{
		GHashTableIter iter;
		gpointer key, value;

		g_hash_table_iter_init(&iter,  pStyleHash);
		/* iter per layer */
		while (g_hash_table_iter_next (&iter, &key, &value))
		{
			GHashTable *pHash = value;
			if(NULL != pHash)
			{
				priv->pPrivHash = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);
				g_hash_table_foreach(pHash, v_lbs_bottom_bar_parse_style, self);
			}
		}
	}
	/* free the style hash */
	thornbury_style_free( pStyleHash);

  sscanf((gchar*) g_hash_table_lookup (priv->pPrivHash, LBS_BAR_LEFT_TEXT_X), "%f", &leftX);
  sscanf((gchar*) g_hash_table_lookup (priv->pPrivHash, LBS_BAR_LEFT_TEXT_Y), "%f", &leftY);
  sscanf((gchar*) g_hash_table_lookup (priv->pPrivHash, LBS_BAR_RIGHT_TEXT_X),"%f", &rightX);
  sscanf((gchar*) g_hash_table_lookup (priv->pPrivHash, LBS_BAR_RIGHT_TEXT_Y), "%f", &rightY);
  sscanf((gchar*) g_hash_table_lookup (priv->pPrivHash, LBS_BAR_MIDDLE_TEXT_X),"%f", &midX);
  sscanf((gchar*) g_hash_table_lookup (priv->pPrivHash, LBS_BAR_MIDDLE_TEXT_Y),"%f", &midY);
  sscanf((gchar*) g_hash_table_lookup (priv->pPrivHash, LBS_BAR_LEFT_TEXT_MAX),"%f", &leftWid);
  sscanf((gchar*) g_hash_table_lookup (priv->pPrivHash, LBS_BAR_RIGHT_TEXT_MAX),"%f", &rightWid);
  sscanf((gchar*) g_hash_table_lookup (priv->pPrivHash, LBS_BAR_MIDDLE_TEXT_MAX),"%f", &midWid);

  priv->pLeftText = clutter_text_new ();
  clutter_text_set_color (CLUTTER_TEXT (priv->pLeftText), &priv->pLeftTextColor);
  clutter_text_set_font_name (CLUTTER_TEXT (priv->pLeftText), g_hash_table_lookup (priv->pPrivHash, LBS_BAR_LEFT_TEXT_FONT));
  clutter_text_set_line_alignment (CLUTTER_TEXT(priv->pLeftText), PANGO_ALIGN_LEFT);
  clutter_text_set_ellipsize (CLUTTER_TEXT (priv->pLeftText), PANGO_ELLIPSIZE_END);
  clutter_actor_set_position (priv->pLeftText, leftX, leftY);
  clutter_actor_set_width (CLUTTER_ACTOR (priv->pLeftText), leftWid);
  clutter_actor_add_child (CLUTTER_ACTOR (self), priv->pLeftText);
  clutter_actor_show (priv->pLeftText);

  priv->pRightText = clutter_text_new ();
  clutter_text_set_color (CLUTTER_TEXT (priv->pRightText), &priv->pRightTextColor);
  clutter_text_set_font_name (CLUTTER_TEXT (priv->pRightText), g_hash_table_lookup (priv->pPrivHash, LBS_BAR_RIGHT_TEXT_FONT));
  clutter_text_set_line_alignment (CLUTTER_TEXT (priv->pRightText), PANGO_ALIGN_RIGHT);
  clutter_text_set_ellipsize (CLUTTER_TEXT (priv->pRightText), PANGO_ELLIPSIZE_END);
  clutter_actor_set_position (priv->pRightText, rightX, rightY);
  clutter_actor_set_width (CLUTTER_ACTOR (priv->pRightText), rightWid);
  clutter_actor_add_child (CLUTTER_ACTOR (self), priv->pRightText);
  clutter_actor_show (priv->pRightText);

  priv->pMiddleText = clutter_text_new ();
  clutter_text_set_color (CLUTTER_TEXT (priv->pMiddleText), &priv->pMiddleTextColor);
  clutter_text_set_font_name (CLUTTER_TEXT (priv->pMiddleText), g_hash_table_lookup (priv->pPrivHash, LBS_BAR_MIDDLE_TEXT_FONT));
  clutter_text_set_line_alignment (CLUTTER_TEXT (priv->pMiddleText), PANGO_ALIGN_LEFT);
  clutter_text_set_ellipsize (CLUTTER_TEXT (priv->pMiddleText), PANGO_ELLIPSIZE_END);
  clutter_actor_set_position (priv->pMiddleText, midX,midY);
  clutter_actor_set_width (CLUTTER_ACTOR (priv->pMiddleText), midWid);
  clutter_actor_add_child (CLUTTER_ACTOR (self), priv->pMiddleText);
  clutter_actor_show (priv->pMiddleText);
  clutter_actor_set_reactive (CLUTTER_ACTOR (self), TRUE);

  priv->glow_effect = g_object_ref_sink (lightwood_glow_shader_new ());
  priv->glow_effect1 = g_object_ref_sink (lightwood_glow_shader_new ());
  priv->glow_effect2= g_object_ref_sink (lightwood_glow_shader_new ());
}

/**
 * mildenhall_lbs_bottom_bar_new:
 * 
 * Returns: (transfer floating): newly created #MildenhallLbsBottomBar
 */
MildenhallLbsBottomBar *
mildenhall_lbs_bottom_bar_new (void)
{
  return g_object_new (MILDENHALL_TYPE_LBS_BOTTOM_BAR, NULL);
}
