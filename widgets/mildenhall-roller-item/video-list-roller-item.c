/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* video-list-roller-item.c */

#include "video-list-roller-item.h"
#include "liblightwood-fixedroller.h"
#include "liblightwood-expandable.h"
#include "liblightwood-expander.h"

typedef struct
{
  ClutterActor *icon;
  ClutterActor *topText;
  ClutterActor *belowLeftText;
  ClutterActor *belowRightText;
  ClutterActor *verticalLine;
  ClutterActor *horizontalLine;
} MildenhallVideoListRollerItemPrivate;

struct _MildenhallVideoListRollerItem
{
  ClutterActor parent;
};

G_DEFINE_TYPE_WITH_PRIVATE (MildenhallVideoListRollerItem, mildenhall_video_list_roller_item, CLUTTER_TYPE_ACTOR)

enum
{
	PROP_0,
	PROP_ICON,
	PROP_TOP_TEXT,
	PROP_BELOW_LEFT_TEXT,
	PROP_BELOW_RIGHT_TEXT
};


static void
video_list_roller_item_get_property (GObject    *object,
		guint       property_id,
		GValue     *value,
		GParamSpec *pspec)
{
	switch (property_id)
	{
	case PROP_ICON:
	case PROP_TOP_TEXT:
	case PROP_BELOW_LEFT_TEXT:
	case PROP_BELOW_RIGHT_TEXT:
	     /* FIXME: Don't we need to get values of properties? (T1408) */
	     /* Write Only */
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void
video_list_roller_item_set_property (GObject      *object,
		guint         property_id,
		const GValue *value,
		GParamSpec   *pspec)
{
	MildenhallVideoListRollerItem *item = MILDENHALL_VIDEO_LIST_ROLLER_ITEM (object);
	MildenhallVideoListRollerItemPrivate *priv = mildenhall_video_list_roller_item_get_instance_private (item);

	switch (property_id)
	{
	case PROP_ICON:
		if(g_value_get_object(value))
		{
			clutter_actor_set_content (priv->icon, CLUTTER_CONTENT (g_value_get_object (value)));
		}
		break;
	case PROP_TOP_TEXT:
		clutter_text_set_text (CLUTTER_TEXT (priv->topText), g_value_get_string (value));
		break;
	case PROP_BELOW_LEFT_TEXT:
		clutter_text_set_text (CLUTTER_TEXT (priv->belowLeftText), g_value_get_string (value));
		break;
	case PROP_BELOW_RIGHT_TEXT:
		clutter_text_set_text (CLUTTER_TEXT (priv->belowRightText), g_value_get_string (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void
video_list_roller_item_dispose (GObject *object)
{
  G_OBJECT_CLASS (mildenhall_video_list_roller_item_parent_class)->dispose (object);
}

static void
video_list_roller_item_finalize (GObject *object)
{
  G_OBJECT_CLASS (mildenhall_video_list_roller_item_parent_class)->finalize (object);
}

static void
mildenhall_video_list_roller_item_class_init (MildenhallVideoListRollerItemClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GParamSpec *pspec;

	object_class->get_property = video_list_roller_item_get_property;
	object_class->set_property = video_list_roller_item_set_property;
	object_class->dispose = video_list_roller_item_dispose;
	object_class->finalize = video_list_roller_item_finalize;

	pspec = g_param_spec_object ("icon",
			"icon",
			"texture content for the thumbnail icon",
			G_TYPE_OBJECT,
			G_PARAM_WRITABLE);
	g_object_class_install_property (object_class, PROP_ICON, pspec);



	pspec = g_param_spec_string ("top-text",
			"top-text",
			"Top text",
			NULL,
			G_PARAM_WRITABLE);
	g_object_class_install_property (object_class, PROP_TOP_TEXT, pspec);

	pspec = g_param_spec_string ("below-left-text",
			"below-left-text",
			"Below left side text",
			NULL,
			G_PARAM_WRITABLE);
	g_object_class_install_property (object_class, PROP_BELOW_LEFT_TEXT, pspec);

	pspec = g_param_spec_string ("below-right-text",
			"below-right-text",
			"Below right side text",
			NULL,
			G_PARAM_WRITABLE);
	g_object_class_install_property (object_class, PROP_BELOW_RIGHT_TEXT, pspec);


}

static void
mildenhall_video_list_roller_item_init (MildenhallVideoListRollerItem *self)
{
	MildenhallVideoListRollerItemPrivate *priv = mildenhall_video_list_roller_item_get_instance_private (self);

	ClutterColor text_color = { 0x98, 0xA9, 0xB2, 0xFF };
	ClutterColor line_color = {0xFF, 0xFF, 0xFF, 0x33};

	priv->verticalLine = clutter_actor_new ();
	clutter_actor_set_background_color (priv->verticalLine, &line_color);
	clutter_actor_set_size (priv->verticalLine, 1, 97);
	clutter_actor_set_position (priv->verticalLine, 163, 0);
	clutter_actor_add_child (CLUTTER_ACTOR (self), priv->verticalLine);
	clutter_actor_show (priv->verticalLine);

	priv->horizontalLine = clutter_actor_new ();
	clutter_actor_set_background_color (priv->horizontalLine, &line_color);
	clutter_actor_set_size (priv->horizontalLine, 800, 1);
	clutter_actor_set_position (priv->horizontalLine, 0, 97);
	clutter_actor_add_child (CLUTTER_ACTOR (self), priv->horizontalLine);
	clutter_actor_show (priv->horizontalLine);

	priv->icon = clutter_actor_new ();
	clutter_actor_add_child (CLUTTER_ACTOR (self), priv->icon);
	clutter_actor_set_size (priv->icon, 162, 97);
	clutter_actor_show (priv->icon);

	priv->topText = clutter_text_new ();
	clutter_text_set_color (CLUTTER_TEXT (priv->topText), &text_color);
	clutter_text_set_font_name (CLUTTER_TEXT (priv->topText), "DejaVuSansCondensed 27px");
	clutter_actor_set_position (priv->topText, 180, 23);
	clutter_actor_set_width (CLUTTER_ACTOR (priv->topText),385.0);
	clutter_text_set_line_alignment (CLUTTER_TEXT (priv->topText), PANGO_ALIGN_LEFT);
	clutter_actor_add_child (CLUTTER_ACTOR (self), priv->topText);
	clutter_text_set_ellipsize (CLUTTER_TEXT (priv->topText), PANGO_ELLIPSIZE_END);
	clutter_actor_show (priv->topText);

	priv->belowLeftText = clutter_text_new ();
	clutter_text_set_color (CLUTTER_TEXT (priv->belowLeftText), &text_color);
	clutter_text_set_font_name (CLUTTER_TEXT (priv->belowLeftText), "DejaVuSansCondensed 16px");
	clutter_actor_set_position (priv->belowLeftText, 180, 59);
	clutter_actor_set_width (CLUTTER_ACTOR (priv->belowLeftText),280.0);
	clutter_text_set_line_alignment (CLUTTER_TEXT (priv->belowLeftText), PANGO_ALIGN_LEFT);
	clutter_actor_add_child (CLUTTER_ACTOR (self), priv->belowLeftText);
	clutter_actor_show (priv->belowLeftText);

	priv->belowRightText = clutter_text_new ();
	clutter_text_set_color (CLUTTER_TEXT (priv->belowRightText), &text_color);
	clutter_text_set_font_name (CLUTTER_TEXT (priv->belowRightText), "DejaVuSansCondensed 16px");
	clutter_actor_set_position (priv->belowRightText, 550, 59);
	clutter_actor_set_width (CLUTTER_ACTOR (priv->belowRightText), 150.0);
	clutter_text_set_line_alignment (CLUTTER_TEXT (priv->belowRightText), PANGO_ALIGN_LEFT);
	clutter_actor_add_child (CLUTTER_ACTOR (self), priv->belowRightText);
	clutter_actor_show (priv->belowRightText);
	clutter_actor_set_reactive (CLUTTER_ACTOR (self), TRUE);
}

MildenhallVideoListRollerItem *
mildenhall_video_list_roller_item_new (void)
{
  return g_object_new (MILDENHALL_TYPE_VIDEO_LIST_ROLLER_ITEM, NULL);
}
