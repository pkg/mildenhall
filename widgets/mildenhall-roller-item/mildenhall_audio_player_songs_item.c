/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/******************************************************************************
 *  @Filename :   mildenhall_audio_player_songs_item.c
 *  @Project: --
 *-----------------------------------------------------------------------------
 *  @Created on :  Feb 14, 2013
 *------------------------------------------------------------------------------
 *  @Description:  This widget can be used to create roller item type for roller
 *                 having icon/text at one time and text field.
 *
 *
 * Description of FIXES:
 * -----------------------------------------------------------------------------------
 *      Description                             Date                    Name
 *      ----------                              ----                    ----
 *
 *******************************************************************************/

#include "mildenhall_audio_player_songs_item.h"

#include <thornbury/thornbury.h>

#include <liblightwood-roller.h>
#include "liblightwood-fixedroller.h"
#include "liblightwood-expandable.h"
#include "liblightwood-expander.h"
#include "liblightwood-glowshader.h"

#include "mildenhall_roller_container.h"

G_DEFINE_TYPE (MildenhallAudioPlayerSongsItem, mildenhall_audio_player_songs_item, CLUTTER_TYPE_ACTOR)

#define AUDIO_PLAYER_SONGS_ITEM_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_AUDIO_PLAYER_SONGS_ITEM, MildenhallAudioPlayerSongsItemPrivate))

#define MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT( a ...)                //g_print(a)

/* style properties */
#define FOOTER_IMAGE		"footer-image"
#define ARROW_UP		"arrow-up-markup"
#define ARROW_DOWN		"arrow-down-markup"
#define VERTICAL_LINE_X         "vertical-line-x"
#define BOTTOM_LINE_Y     	"bottom-line-y"
#define TEXT_FONT         	"font"
#define TEXT_Y	         	"text-y"
#define LINE_COLOR         	"line-color"
#define TEXT_COLOR         	"text-color"
#define FOOTER_HEIGHT         	"footer-height"
#define ARROW_X      		"arrow-x"
#define ITEM_WIDTH		"width"
#define ITEM_HEIGHT		"height"


/* property */
typedef enum _enAudioPlayerItemProperty enAudioPlayerItemProperty;
enum _enAudioPlayerItemProperty
{
        PROP_FIRST,
	//PROP_ID,
        PROP_ICON,
        PROP_NUMBER,
	PROP_TEXT,
	PROP_ROW,
	PROP_FOCUSED,
	PROP_FOOTER,
	PROP_ARROW_UP,
	PROP_SHOW_ARROW,
	PROP_HIGHLIGHT_ARROW,
	PROP_HIGHLIGHT_SHOW,
        PROP_LAST
};

/* private members */
struct _MildenhallAudioPlayerSongsItemPrivate
{
	ClutterActor *pIcon;
	ClutterActor *pText;
	ClutterActor *pNumber;
	ClutterActor *pVerticalLine;
	ClutterActor *pBottomLine;
	ClutterActor *pTopLine;
	ClutterActor *pFooterBg;
	ClutterActor *pArrow;
	ClutterActor *pHighlightArrow;

	gboolean bFooter;
	gboolean bArrowUp;
	gboolean bDisposeRunning;
	//gchar *pID;
	gboolean row_called;

	/* style property */
	gchar *pFooterImagePath;
	gchar *pArrowUpMarkup;
	gchar *pArrowDownMarkup;
	gchar *pIconPath;
	gchar *pFont;
	gfloat flVerticalLineX;
	gfloat flTextY;
	gfloat flBottomLineY;
	gfloat flArrowX;
	gfloat flFooterHeight;
	gfloat flHeight;
	gfloat flWidth;

	ClutterColor textColor;
	ClutterColor lineColor;

	ClutterEffect *glow_effect_1;
	ClutterEffect *glow_effect_2;
};

static void mildenhall_audio_player_songs_item_set_num_text(MildenhallAudioPlayerSongsItem *pAudioPlayerSongsItem, const gchar *pText);
static gboolean language_change(gpointer user_data);
static void create_footer(MildenhallAudioPlayerSongsItem *pAudioPlayerSongsItem, gfloat flRollerWidth);

/*******************************************************************************
 * Internal Functions
 ******************************************************************************/

/*******************************************************
 * Function : on_notify_language
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void on_notify_language ( GObject    *object,
                                 GParamSpec *pspec,
                                 gpointer    user_data)
{
        language_change(user_data);
}

/*******************************************************
 * Function : language_change
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static gboolean language_change(gpointer user_data)
{
         MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("%s\n", __FUNCTION__);

        if(! MILDENHALL_IS_AUDIO_PLAYER_SONGS_ITEM(user_data))
                return FALSE;

        MildenhallAudioPlayerSongsItem *pAudioPlayerItem = MILDENHALL_AUDIO_PLAYER_SONGS_ITEM(user_data);
        MildenhallAudioPlayerSongsItemPrivate *priv = AUDIO_PLAYER_SONGS_ITEM_PRIVATE(pAudioPlayerItem);

        //gchar *pNumber = (gchar*)clutter_text_get_text(CLUTTER_TEXT(priv->pNumber) );
        gchar *pText = (gchar*)clutter_text_get_text(CLUTTER_TEXT(priv->pText) );

        MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("get text =%s####\n get text = %s####\n",  gettext(pNumber), gettext(pText));

        if(NULL != pText && priv->bFooter)
        {
                gchar *text = (gchar*)gettext(pText);
                clutter_text_set_text(CLUTTER_TEXT(priv->pText), g_strdup(text) );
        }

        MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("%f\n", clutter_actor_get_height(CLUTTER_ACTOR(pAudioPlayerItem)) );
        return FALSE;
}

/****************************************************
 * Function : draw_line
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static ClutterActor *draw_line(gfloat flX, gfloat flY, gfloat flWidth, gfloat flHeight, ClutterColor color)
{
	//ClutterColor lineColor = {0xFF, 0xFF, 0xFF, 0x33};

        ClutterActor *pLine = clutter_actor_new();
	g_object_set(pLine, "background-color", &color, NULL);
        clutter_actor_set_position (pLine, flX, flY);
        clutter_actor_set_size (pLine, flWidth, flHeight);

        return pLine;
}

/****************************************************
 * Function : mildenhall_audio_player_songs_item_set_icon
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void mildenhall_audio_player_songs_item_set_icon(MildenhallAudioPlayerSongsItem *pAudioPlayerSongsItem, const gchar *pIconPath)
{
	MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("%s\n", __FUNCTION__);

	MildenhallAudioPlayerSongsItemPrivate *priv = AUDIO_PLAYER_SONGS_ITEM_PRIVATE(pAudioPlayerSongsItem);
	if(priv->pIconPath)
	{
		g_free(priv->pIconPath);
		priv->pIconPath = NULL;
	}
	priv->pIconPath = g_strdup(pIconPath);

	if(priv->pIcon)
	{
		thornbury_ui_texture_set_from_file (priv->pIcon, pIconPath, 0, 0, TRUE, FALSE);
	}
	else
	{
		priv->pIcon = thornbury_ui_texture_create_new(pIconPath, 0, 0, TRUE, FALSE);
		gfloat x = (priv->flVerticalLineX - clutter_actor_get_width(priv->pIcon)) / 2;
		gfloat y = (priv->flHeight - clutter_actor_get_height(priv->pIcon)) / 2;
		clutter_actor_set_position(priv->pIcon, x, y);
		clutter_actor_add_child(CLUTTER_ACTOR(pAudioPlayerSongsItem), priv->pIcon);
	}
}

/****************************************************
 * Function : mildenhall_audio_player_songs_item_set_num_text
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void mildenhall_audio_player_songs_item_set_num_text(MildenhallAudioPlayerSongsItem *pAudioPlayerSongsItem, const gchar *pText)
{
	MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("%s\n", __FUNCTION__);

	MildenhallAudioPlayerSongsItemPrivate *priv = AUDIO_PLAYER_SONGS_ITEM_PRIVATE(pAudioPlayerSongsItem);

	clutter_text_set_text (CLUTTER_TEXT (priv->pNumber), pText);
	/* if width exceeds, set the max */
	if(clutter_actor_get_width(priv->pNumber) > 54)
		clutter_actor_set_width(priv->pNumber, 54);
	/* set the position */
	clutter_actor_set_x(priv->pNumber, (priv->flVerticalLineX - clutter_actor_get_width(priv->pNumber)) / 2);

}

/****************************************************
 * Function : mildenhall_audio_player_songs_item_set_text
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void mildenhall_audio_player_songs_item_set_text(MildenhallAudioPlayerSongsItem *pAudioPlayerSongsItem, const gchar *pText)
{
	MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("%s\n", __FUNCTION__);

	MildenhallAudioPlayerSongsItemPrivate *priv = AUDIO_PLAYER_SONGS_ITEM_PRIVATE(pAudioPlayerSongsItem);

	clutter_text_set_text (CLUTTER_TEXT (priv->pText), pText);

}

/****************************************************
 * Function : mildenhall_audio_player_songs_item_set_show_arrow
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void mildenhall_audio_player_songs_item_set_show_arrow(MildenhallAudioPlayerSongsItem *pAudioPlayerSongsItem, gboolean bShowArrow)
{
	MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("%s\n", __FUNCTION__);
	if(bShowArrow)
		clutter_actor_show(pAudioPlayerSongsItem->priv->pArrow);
	else
		clutter_actor_hide(pAudioPlayerSongsItem->priv->pArrow);
}

/****************************************************
 * Function : mildenhall_audio_player_songs_item_set_focus
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void mildenhall_audio_player_songs_item_set_focus(MildenhallAudioPlayerSongsItem *pAudioPlayerSongsItem, gboolean bFocus)
{
	MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("%s\n", __FUNCTION__);
	MildenhallAudioPlayerSongsItemPrivate *priv = AUDIO_PLAYER_SONGS_ITEM_PRIVATE(pAudioPlayerSongsItem);
	if (bFocus)
	{
		//MildenhallAudioPlayerSongsItemClass *klass = MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_GET_CLASS (pObject);
		if (clutter_actor_get_effect (priv->pText, "glow") == NULL)
			clutter_actor_add_effect_with_name (priv->pText, "glow", pAudioPlayerSongsItem->priv->glow_effect_1);
		if (clutter_actor_get_effect (priv->pNumber, "glow") == NULL)
			clutter_actor_add_effect_with_name (priv->pNumber, "glow", pAudioPlayerSongsItem->priv->glow_effect_2);
	}
	else
	{
		if (clutter_actor_get_effect (priv->pText, "glow") != NULL)
			clutter_actor_remove_effect_by_name (priv->pText, "glow");
		if (clutter_actor_get_effect (priv->pNumber, "glow") != NULL)
			clutter_actor_remove_effect_by_name (priv->pNumber, "glow");
	}
}

/****************************************************
 * Function : mildenhall_audio_player_songs_item_set_highlight_show
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void mildenhall_audio_player_songs_item_set_highlight_show(MildenhallAudioPlayerSongsItem *pAudioPlayerSongsItem, gboolean bShowArrow)
{
	MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("%s\n", __FUNCTION__);
	MildenhallAudioPlayerSongsItemPrivate *priv = AUDIO_PLAYER_SONGS_ITEM_PRIVATE(pAudioPlayerSongsItem);
	if(bShowArrow)
	{
		clutter_actor_show(priv->pHighlightArrow);
		clutter_actor_hide(priv->pNumber);
	}
	else
	{
		clutter_actor_hide(priv->pHighlightArrow);
		clutter_actor_show(priv->pNumber);
	}
}

/****************************************************
 * Function : mildenhall_audio_player_songs_item_set_footer
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void mildenhall_audio_player_songs_item_set_footer(MildenhallAudioPlayerSongsItem *pAudioPlayerSongsItem, gboolean bFooter)
{
	MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("%s\n", __FUNCTION__);
        MildenhallAudioPlayerSongsItemPrivate *priv = AUDIO_PLAYER_SONGS_ITEM_PRIVATE(pAudioPlayerSongsItem);

	pAudioPlayerSongsItem->priv->bFooter = bFooter;
	if(priv->bFooter)
	{
		gfloat flRollerWidth;
		ClutterActor *parent = clutter_actor_get_parent (CLUTTER_ACTOR (pAudioPlayerSongsItem));
		if(parent != NULL)
		{
			if(FALSE == priv->row_called)
			{
				if (MILDENHALL_IS_ROLLER_CONTAINER (clutter_actor_get_parent (parent)))
				{
					ClutterActor *actor = clutter_actor_get_parent(parent);
					g_signal_connect(MILDENHALL_ROLLER_CONTAINER (actor) ,
							"notify::language",
							G_CALLBACK (on_notify_language),
							pAudioPlayerSongsItem);
				}
			}

			priv->row_called=TRUE;

			clutter_actor_get_preferred_width (CLUTTER_ACTOR (parent), -1, &flRollerWidth, NULL);
			create_footer(pAudioPlayerSongsItem, flRollerWidth);
		}
	}
	else
	{
		if(priv->pFooterBg)
			clutter_actor_hide(priv->pFooterBg);
	}
}

/****************************************************
 * Function : mildenhall_audio_player_songs_item_set_row
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void mildenhall_audio_player_songs_item_set_row(MildenhallAudioPlayerSongsItem *pAudioPlayerSongsItem, guint inRow )
{
	MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("%s\n", __FUNCTION__);
        MildenhallAudioPlayerSongsItemPrivate *priv = AUDIO_PLAYER_SONGS_ITEM_PRIVATE(pAudioPlayerSongsItem);

	ClutterActor *parent = clutter_actor_get_parent (CLUTTER_ACTOR (pAudioPlayerSongsItem));
	if(parent != NULL)
	{
		ThornburyModel * model = lightwood_roller_get_model(LIGHTWOOD_ROLLER(parent));
		gfloat flRollerWidth;
		clutter_actor_get_preferred_width (CLUTTER_ACTOR (parent), -1, &flRollerWidth, NULL);
		MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("parent width = %f\n", flRollerWidth);
		clutter_actor_set_size(priv->pTopLine, (guint)flRollerWidth, 1);
		clutter_actor_show(priv->pTopLine);
		/* reset the label width */
		clutter_actor_set_width(priv->pText, flRollerWidth - (priv->flVerticalLineX + 25) );

		if (parent != NULL &&
				model != NULL &&
				inRow == thornbury_model_get_n_rows (model) - 1)
		{
			/* add bottom line for first item */
			clutter_actor_set_size(priv->pBottomLine, (guint)flRollerWidth, 1);
			clutter_actor_show (priv->pBottomLine);
		}
		else
			clutter_actor_hide (priv->pBottomLine);
	}
}

/****************************************************
 * Function : add_footer_sort_arrow
 * Description: add arrow to footer
 * Parameters: Pointer to item and flag to indicate
 *             if it is an up or a down arrow
 * Return value: void
 ********************************************************/
static void add_footer_sort_arrow(MildenhallAudioPlayerSongsItem *pAudioPlayerSongsItem, gboolean upArrow)
{
        MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("%s, %d \n", __FUNCTION__, __LINE__);
        if(NULL == pAudioPlayerSongsItem)
                return;
	MildenhallAudioPlayerSongsItemPrivate *priv = AUDIO_PLAYER_SONGS_ITEM_PRIVATE(pAudioPlayerSongsItem);

	if(NULL == priv->pArrow)
	{
       		priv->pArrow = clutter_text_new ();
        	clutter_text_set_editable (CLUTTER_TEXT (priv->pArrow), FALSE);
        	clutter_text_set_font_name (CLUTTER_TEXT (priv->pArrow),priv->pFont );
        	clutter_text_set_color (CLUTTER_TEXT (priv->pArrow), &priv->textColor);
        	clutter_text_set_use_markup (CLUTTER_TEXT (priv->pArrow), TRUE);
	        clutter_actor_add_child(CLUTTER_ACTOR(pAudioPlayerSongsItem), priv->pArrow);
        	clutter_actor_set_position(priv->pArrow,priv->flArrowX, /* 190,*/  priv->flTextY);

	}
	priv->bArrowUp = upArrow;

        if(upArrow)
        {
                clutter_text_set_markup (CLUTTER_TEXT (priv->pArrow), priv->pArrowUpMarkup);//"&#x25b5;");
        }
        else
        {
                clutter_text_set_markup (CLUTTER_TEXT (priv->pArrow), priv->pArrowDownMarkup);//"&#x25bf;");
        }
        clutter_actor_show(priv->pArrow);
}

/****************************************************
 * Function : create_footer
 * Description:
 * Parameters:
 * Return value: void
 ********************************************************/
static void create_footer(MildenhallAudioPlayerSongsItem *pAudioPlayerSongsItem, gfloat flRollerWidth)
{
	MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("%s, %d \n", __FUNCTION__, __LINE__);
	MildenhallAudioPlayerSongsItemPrivate *priv = AUDIO_PLAYER_SONGS_ITEM_PRIVATE(pAudioPlayerSongsItem);

	if(NULL == priv->pFooterBg && NULL != priv->pFooterImagePath)
	{
		MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("#######%s %f\n", priv->pFooterImagePath, flRollerWidth);
		priv->pFooterBg = thornbury_ui_texture_create_new(priv->pFooterImagePath/*EXAMPLES_DATADIR"/sortbottom.png"*/ , flRollerWidth, priv->flFooterHeight, FALSE, TRUE );
		clutter_actor_insert_child_at_index(CLUTTER_ACTOR(pAudioPlayerSongsItem), priv->pFooterBg, 0);
		/* hide top and bottom line */
		if(priv->pTopLine)
			clutter_actor_hide(priv->pTopLine);
		if(priv->pBottomLine)
			clutter_actor_hide(pAudioPlayerSongsItem->priv->pBottomLine);

		/* draw two horizontal line */
		ClutterActor *pLineH1 = draw_line(0, 0, flRollerWidth, 1, priv->lineColor);
		ClutterActor *pLineH2 = draw_line(0, 7, flRollerWidth, 1, priv->lineColor);

		clutter_actor_add_child(CLUTTER_ACTOR(pAudioPlayerSongsItem), pLineH1);
		clutter_actor_add_child(CLUTTER_ACTOR(pAudioPlayerSongsItem), pLineH2);
		/* reduce vertical line length */
		clutter_actor_set_y(pAudioPlayerSongsItem->priv->pVerticalLine, 8);
		clutter_actor_set_height(pAudioPlayerSongsItem->priv->pVerticalLine, clutter_actor_get_height(pAudioPlayerSongsItem->priv->pVerticalLine) - 7 );
		clutter_actor_show(pAudioPlayerSongsItem->priv->pFooterBg);
		/* add sort arrow */
		add_footer_sort_arrow(pAudioPlayerSongsItem, TRUE);
	}
}

/********************************************************
 * Function : v_audio_player_item_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void v_audio_player_item_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
	MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("%s, %d \n", __FUNCTION__, __LINE__);
	gchar *pStyleKey = NULL;
	pStyleKey = g_strdup(pKey);

        MildenhallAudioPlayerSongsItem *pAudioPlayerSongsItem = MILDENHALL_AUDIO_PLAYER_SONGS_ITEM(pUserData);
	MildenhallAudioPlayerSongsItemPrivate *priv = AUDIO_PLAYER_SONGS_ITEM_PRIVATE(pAudioPlayerSongsItem);


	if (g_strcmp0(pStyleKey, FOOTER_IMAGE) == 0)
        {
                 priv->pFooterImagePath = g_strdup_printf(EXAMPLES_DATADIR"/%s", (gchar*)g_value_get_string(pValue));
        }
	else if(g_strcmp0(pStyleKey, ARROW_UP) == 0)
	{
		priv->pArrowUpMarkup = g_strdup(g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, ARROW_DOWN) == 0)
	{
		priv->pArrowDownMarkup = g_strdup( g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, VERTICAL_LINE_X) == 0)
	{
                 priv->flVerticalLineX = g_value_get_double(pValue);
	}
	else if(g_strcmp0(pStyleKey, BOTTOM_LINE_Y) == 0)
	{
                 priv->flBottomLineY = g_value_get_double(pValue);
	}
	else if(g_strcmp0(pStyleKey, TEXT_Y) == 0)
	{
                 priv->flTextY = g_value_get_double(pValue);
	}
	if(g_strcmp0(pStyleKey, LINE_COLOR) == 0)
        {
                 clutter_color_from_string(&priv->lineColor, g_value_get_string(pValue));
        }
	if(g_strcmp0(pStyleKey, TEXT_COLOR) == 0)
        {
                 clutter_color_from_string(&priv->textColor, g_value_get_string(pValue));
        }
	else if(g_strcmp0(pStyleKey, ITEM_HEIGHT) == 0)
        {
                 priv->flHeight = g_value_get_double(pValue);
        }
	else if(g_strcmp0(pStyleKey, ITEM_WIDTH) == 0)
        {
                 priv->flWidth = g_value_get_double(pValue);
        }
	else if(g_strcmp0(pStyleKey, TEXT_FONT) == 0)
	{
                priv->pFont = g_strdup(g_value_get_string(pValue));
	}
        else if(g_strcmp0(pStyleKey, ARROW_X) == 0)
        {
                 priv->flArrowX = g_value_get_double(pValue);
        }
        else if(g_strcmp0(pStyleKey, FOOTER_HEIGHT) == 0)
        {
                 priv->flFooterHeight = g_value_get_double(pValue);
        }

	if(NULL != pStyleKey)
	{
		g_free(pStyleKey);
		pStyleKey = NULL;
	}
}

/********************************************************
 * Function : mildenhall_audio_player_songs_item_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_audio_player_songs_item_set_property (GObject *pObject, guint uinPropertyID, const GValue *pValue, GParamSpec *pspec)
{
	if(!MILDENHALL_IS_AUDIO_PLAYER_SONGS_ITEM(pObject))
        {
                g_warning("invalid item object\n");
                return;
        }

	MildenhallAudioPlayerSongsItem *pAudioPlayerSongsItem = MILDENHALL_AUDIO_PLAYER_SONGS_ITEM(pObject);
	gboolean bArrowUp = FALSE;
	gboolean bShowArrow = FALSE;

	switch (uinPropertyID)
	{
		//case PROP_ID:
                  //      mildenhall_audio_player_songs_item_set_id(pAudioPlayerSongsItem, g_value_get_string(pValue));
		//	break;
		case PROP_ICON:
			mildenhall_audio_player_songs_item_set_icon(pAudioPlayerSongsItem, g_value_get_string(pValue));
			break;
		case PROP_NUMBER:
			mildenhall_audio_player_songs_item_set_num_text(pAudioPlayerSongsItem, g_value_get_string(pValue));
			break;
		case PROP_TEXT:
			mildenhall_audio_player_songs_item_set_text(pAudioPlayerSongsItem, g_value_get_string(pValue));
			break;
		case PROP_ROW:
			mildenhall_audio_player_songs_item_set_row(pAudioPlayerSongsItem, g_value_get_uint (pValue) );
			break;
                case PROP_FOCUSED:
			mildenhall_audio_player_songs_item_set_focus(pAudioPlayerSongsItem, g_value_get_boolean (pValue));
			break;
		case PROP_FOOTER:
			mildenhall_audio_player_songs_item_set_footer(pAudioPlayerSongsItem, g_value_get_boolean (pValue));
			break;
		case PROP_ARROW_UP:
			bArrowUp = g_value_get_boolean(pValue);
			add_footer_sort_arrow(pAudioPlayerSongsItem, bArrowUp);
			break;
		 case PROP_SHOW_ARROW:
			mildenhall_audio_player_songs_item_set_show_arrow(pAudioPlayerSongsItem, g_value_get_boolean (pValue));
                        break;
		case PROP_HIGHLIGHT_SHOW:
			mildenhall_audio_player_songs_item_set_highlight_show(pAudioPlayerSongsItem, g_value_get_boolean(pValue));
			break;
		case PROP_HIGHLIGHT_ARROW:
			bShowArrow = g_value_get_boolean(pValue);
                        if(bShowArrow)
			{
				thornbury_ui_texture_set_from_file (pAudioPlayerSongsItem->priv->pHighlightArrow, EXAMPLES_DATADIR"/icon_fonts_arrowfill_right.png", 0, 0, TRUE, FALSE);
			}
                        else
			{
				thornbury_ui_texture_set_from_file (pAudioPlayerSongsItem->priv->pHighlightArrow, EXAMPLES_DATADIR "/icon_pause.png", 20, 20, TRUE, FALSE);
			}
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}

/********************************************************
 * Function : mildenhall_audio_player_songs_item_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_audio_player_songs_item_get_property (GObject *pObject, guint uinPropertyID, GValue *pValue, GParamSpec *pspec)
{
	MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("%s\n", __FUNCTION__);
	if(!MILDENHALL_IS_AUDIO_PLAYER_SONGS_ITEM(pObject))
        {
                g_warning("invalid item object\n");
                return;
        }

    //    MildenhallAudioPlayerSongsItem *pAudioPlayerSongsItem = MILDENHALL_AUDIO_PLAYER_SONGS_ITEM(pObject);

	switch (uinPropertyID)
	{
		case PROP_ICON:
                        break;
                case PROP_TEXT:
                        break;
                case PROP_NUMBER:
                        break;
		case PROP_ARROW_UP:
			break;
		case PROP_HIGHLIGHT_ARROW:
			break;
		//case PROP_ID:
                  //      g_value_set_string(pValue, mildenhall_audio_player_songs_item_get_id(pAudioPlayerSongsItem));
                    //    break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}

/********************************************************
 * Function : mildenhall_audio_player_songs_item_finalize
 * Description: Finalize the meta info heade object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_audio_player_songs_item_finalize (GObject *pObject)
{
	MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("%s\n", __FUNCTION__);
	G_OBJECT_CLASS (mildenhall_audio_player_songs_item_parent_class)->finalize (pObject);
}

/********************************************************
 * Function : mildenhall_audio_player_songs_item_dispose
 * Description: Dispose the info roller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_audio_player_songs_item_dispose (GObject *pObject)
{
	MILDENHALL_AUDIO_PLAYER_SONGS_ITEM_PRINT("%s\n", __FUNCTION__);
	MildenhallAudioPlayerSongsItemPrivate *priv = AUDIO_PLAYER_SONGS_ITEM_PRIVATE(MILDENHALL_AUDIO_PLAYER_SONGS_ITEM(pObject));
	if(priv->bDisposeRunning)
		return;

	priv->bDisposeRunning = TRUE;
	if(priv->pFooterImagePath)
	{
		g_free(priv->pFooterImagePath);
		priv->pFooterImagePath = NULL;
	}
	if(priv->pArrowUpMarkup)
	{
		g_free(priv->pArrowUpMarkup);
		priv->pArrowUpMarkup = NULL;
	}
	if(priv->pArrowDownMarkup)
	{
		g_free(priv->pArrowDownMarkup);
		priv->pArrowDownMarkup = NULL;
	}
	if(NULL != priv->pIconPath)
	{
		g_free(priv->pIconPath);
		priv->pIconPath = NULL;

	}
# if 0
	if(NULL != priv->pText)
		clutter_actor_destroy(priv->pText);
	if(NULL != priv->pNumber)
		clutter_actor_destroy(priv->pNumber);
# endif

	g_clear_object (&priv->glow_effect_1);
	g_clear_object (&priv->glow_effect_2);

	G_OBJECT_CLASS (mildenhall_audio_player_songs_item_parent_class)->dispose (pObject);

}

/********************************************************
 * Function : mildenhall_audio_player_songs_item_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_audio_player_songs_item_class_init (MildenhallAudioPlayerSongsItemClass *klass)
{
	GObjectClass *pObjectClass = G_OBJECT_CLASS (klass);
	//ClutterActorClass *pActorClass = CLUTTER_ACTOR_CLASS (klass);

	GParamSpec *pspec = NULL;

	g_type_class_add_private (klass, sizeof (MildenhallAudioPlayerSongsItemPrivate));

	pObjectClass->get_property = mildenhall_audio_player_songs_item_get_property;
	pObjectClass->set_property = mildenhall_audio_player_songs_item_set_property;
	pObjectClass->dispose = mildenhall_audio_player_songs_item_dispose;
	pObjectClass->finalize = mildenhall_audio_player_songs_item_finalize;

	//pActorClass->paint = actor_paint;
        //pActorClass->allocate = actor_allocate;

	 /**
         * MildenhallAudioPlayerSongsItem:icon:
         *
         * image path
         *
         * Default: NULL
         */
	pspec = g_param_spec_string ("icon",
                              "Thumb-Icon",
                              "texture path for the thumbnail icon",
                              NULL,
                              G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_ICON, pspec);

	/**
         * MildenhallAudioPlayerSongsItem:text:
         *
         * main text
         *
         * Default: NULL
         */
	pspec = g_param_spec_string ("text",
                               "Text",
                               "Text for roller item",
                               NULL,
                               G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_TEXT, pspec);

	/**
         * MildenhallAudioPlayerSongsItem:number:
         *
         * text to be shown on left side
         *
         * Default: NULL
         */
	pspec = g_param_spec_string ("number",
                               "Number-Text",
                               "Number-Text for roller item",
                               NULL,
                               G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_NUMBER, pspec);

	/**
         * MildenhallAudioPlayerSongsItem:row:
         *
         * item row number
         *
         * Default: 0
         */
	pspec = g_param_spec_uint ("row",
                        "row number",
                        "row number",
                        0, G_MAXUINT,
                        0,
                        G_PARAM_WRITABLE);
        g_object_class_install_property (pObjectClass, PROP_ROW, pspec);

	 /**
         * MildenhallAudioPlayerSongsItem:focused:
         *
         * Whether this actor should be rendered as focused
         *
         * Default: FALSE
         */
        pspec = g_param_spec_boolean ("focused",
                        "focused",
                        "Whether this actor should be rendered as focused",
                        FALSE,
                        G_PARAM_WRITABLE);
        g_object_class_install_property (pObjectClass, PROP_FOCUSED, pspec);

	 /**
         * MildenhallAudioPlayerSongsItem:footer:
         *
         * Whether this item is for roller Footer
         *
         * Default: FALSE
         */
        pspec = g_param_spec_boolean ("footer",
                        "Footer",
                        "Whether this item is for roller Footer",
                        FALSE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_FOOTER, pspec);

	 /**
         * MildenhallAudioPlayerSongsItem:arrow-up:
         *
         * Whether footer arrow direction is up or down
         *
         * Default: TRUE
         */
        pspec = g_param_spec_boolean ("arrow-up",
                        "Arrow-Up",
                        "Whether footer arrow direction is up or down",
                        TRUE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_ARROW_UP, pspec);

	/**
         * MildenhallAudioPlayerSongsItem:show-arrow:
         *
         * Whether footer arrow needs to be shown
         *
         * Default: TRUE
         */
	pspec = g_param_spec_boolean ("show-arrow",
                        "Arrow-Show",
                        "Whether footer arrow needs to be shown",
                        TRUE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_SHOW_ARROW, pspec);

	/**
         * MildenhallAudioPlayerSongsItem:highlight-show:
         *
         * Whether to show arrow instead of left number text
         *
         * Default: FALSE
         */
	pspec = g_param_spec_boolean ("highlight-show",
                        "Highlight-Show",
                        "Whether to show arrow instead of text",
                        FALSE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_HIGHLIGHT_SHOW, pspec);

	/**
         * MildenhallAudioPlayerSongsItem:highlight-arrow:
         *
         * Whether to show play/pause symbol
         *
         * Default: FALSE
         */
	pspec = g_param_spec_boolean ("highlight-arrow",
                        "Highlight-Arrow",
                        "Whether to show play/pause symbol",
                        FALSE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_HIGHLIGHT_ARROW, pspec);

}

/********************************************************
 * Function : mildenhall_audio_player_songs_item_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void mildenhall_audio_player_songs_item_init (MildenhallAudioPlayerSongsItem *pSelf)
{
	pSelf->priv = AUDIO_PLAYER_SONGS_ITEM_PRIVATE (pSelf);

	pSelf->priv->pIcon = NULL;
	pSelf->priv->pText = NULL;
	pSelf->priv->pNumber = NULL;
	pSelf->priv->pVerticalLine = NULL;
	pSelf->priv->pBottomLine = NULL;
	pSelf->priv->pFooterBg = NULL;
	pSelf->priv->pIconPath = NULL;
	pSelf->priv->pArrow = NULL;
	pSelf->priv->row_called = FALSE;
	//pSelf->priv->pID = NULL;

	pSelf->priv->bFooter = FALSE;
	pSelf->priv->bDisposeRunning = FALSE;

	GHashTable *pStyleHash = thornbury_style_set(PKGDATADIR"/mh_ap_songs_item_style.json");

        /* pares the hash for styles */
        if (NULL != pStyleHash)
        {
                GHashTableIter iter;
                gpointer key, value;

                g_hash_table_iter_init(&iter, pStyleHash);
                /* iter per layer */
                while (g_hash_table_iter_next(&iter, &key, &value))
                {
                        GHashTable *pHash = value;
                        if (NULL != pHash)
                        {
                                g_hash_table_foreach(pHash, v_audio_player_item_parse_style,
                                                     pSelf);
                        }
                }
        }
        /* free the style hash */
        thornbury_style_free(pStyleHash);

	pSelf->priv->pNumber = clutter_text_new ();
        clutter_text_set_color (CLUTTER_TEXT (pSelf->priv->pNumber ), &pSelf->priv->textColor);
        clutter_text_set_font_name (CLUTTER_TEXT (pSelf->priv->pNumber ), pSelf->priv->pFont);
        clutter_text_set_ellipsize(CLUTTER_TEXT (pSelf->priv->pNumber ), PANGO_ELLIPSIZE_END);
	clutter_actor_set_y(pSelf->priv->pNumber, pSelf->priv->flTextY);
        //clutter_actor_set_position (sample_item->num, 18, 15);
        clutter_actor_add_child (CLUTTER_ACTOR (pSelf), pSelf->priv->pNumber );
        clutter_actor_show (pSelf->priv->pNumber);

	pSelf->priv->pText = clutter_text_new ();
        clutter_text_set_color (CLUTTER_TEXT (pSelf->priv->pText ), &pSelf->priv->textColor);
        clutter_text_set_font_name (CLUTTER_TEXT (pSelf->priv->pText ), pSelf->priv->pFont);
        clutter_text_set_ellipsize(CLUTTER_TEXT (pSelf->priv->pText ), PANGO_ELLIPSIZE_END);
	clutter_actor_set_position (pSelf->priv->pText, 79, pSelf->priv->flTextY);
        clutter_actor_add_child (CLUTTER_ACTOR (pSelf), pSelf->priv->pText );
        clutter_actor_show (pSelf->priv->pText);

	/* create highlight arrow and hide */
	pSelf->priv->pHighlightArrow = thornbury_ui_texture_create_new(EXAMPLES_DATADIR "/icon_fonts_arrowfill_right.png", 0, 0, TRUE, FALSE);
	clutter_actor_add_child(CLUTTER_ACTOR(pSelf), pSelf->priv->pHighlightArrow);
	clutter_actor_set_position(pSelf->priv->pHighlightArrow, 22,  24);
	clutter_actor_hide (pSelf->priv->pHighlightArrow);

	/* create separator */
	pSelf->priv->pVerticalLine = draw_line(pSelf->priv->flVerticalLineX, 0, 1, pSelf->priv->flHeight, pSelf->priv->lineColor);
	clutter_actor_add_child(CLUTTER_ACTOR(pSelf), pSelf->priv->pVerticalLine);

	/* create bottom separator */
	pSelf->priv->pTopLine = draw_line(0, 1, pSelf->priv->flWidth, 1, pSelf->priv->lineColor);
	clutter_actor_add_child(CLUTTER_ACTOR(pSelf), pSelf->priv->pTopLine);

	/* create last bottom separator */
	pSelf->priv->pBottomLine = draw_line(0, pSelf->priv->flBottomLineY, pSelf->priv->flWidth, 1, pSelf->priv->lineColor);
	clutter_actor_add_child(CLUTTER_ACTOR(pSelf), pSelf->priv->pBottomLine );

	clutter_actor_set_reactive (CLUTTER_ACTOR (pSelf), TRUE);

	pSelf->priv->glow_effect_1 = g_object_ref_sink (lightwood_glow_shader_new ());
	pSelf->priv->glow_effect_2 = g_object_ref_sink (lightwood_glow_shader_new ());

}

/**
 * mildenhall_audio_player_songs_item_new:
 * Returns: audio player item object
 *
 * Creates a audio player item object
 */
MildenhallAudioPlayerSongsItem *mildenhall_audio_player_songs_item_new (void)
{
	return g_object_new (MILDENHALL_TYPE_AUDIO_PLAYER_SONGS_ITEM, NULL);
}
