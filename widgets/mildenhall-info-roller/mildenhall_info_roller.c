/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_info_roller
 * @title: MildenhallInfoRoller
 * @short_description: It is a combination of drawer and either a fixed roller
 * or a variable roller.
 * @see_also: #ClutterActor, #ThornburyItemFactory
 *
 * #MildenhallInfoRoller widget is a kind of a drawer which contains elements
 * arranged in inside a roller. The roller can be of variable/fixed.
 * It supports model using which the applications can fill the data to the model.
 *
 * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
 *
 * A private enum to set column of #ThornburyModel
 * |[<!-- language="C" -->
 * enum
 * {
 *   COLUMN_LEFT_ICON_TEXT,
 *   COLUMN_MID_TEXT,
 *   COLUMN_RIGHT_ICON_TEXT,
 *   COLUMN_LAST
 * };
 *
 * ]|
 *
 * How to create #MildenhallInfoRoller
 * |[<!-- language="C" -->
 *   g_autofree gchar *image = NULL;
 *   g_autofree gchar *overlay_img_file = NULL;
 *   GObject *object = NULL;
 *   ClutterActor *roller_actor = NULL;
 *   ThornburyModel *header_model = NULL;
 *   ThornburyModel *model = NULL;
 *   g_autofree gchar *launcher_video_file = NULL;
 *   g_autofree gchar *content_video_file = NULL;
 *
 *   launcher_video_file = g_build_filename ("/usr/share/mildenhall",
 *                                          "mildenhallroller",
 *                                          "01_appl_launcher_2009.03.09.avi",
 *                                          NULL);
 *   content_video_file = g_build_filename ("/usr/share/mildenhall",
 *                                         "mildenhallroller",
 *                                         "03_content_roll_2009.03.09.avi",
 *                                         NULL);
 *   Thornburyitem_factory *item_factory = thornbury_item_factory_generate_widget_with_props (
 *                     MILDENHALL_TYPE_INFO_ROLLER,
 *                     "/usr/share/mildenhall/mildenhall_info_roller_prop.json");
 *
 *   g_object_get (item_factory, "object", &object, NULL);
 *   roller_actor = CLUTTER_ACTOR (object);
 *
 *   g_signal_connect (G_OBJECT (roller_actor), "info-roller-item-selected",
 * 		    G_CALLBACK (item_selected_cb), NULL);
 *   g_signal_connect (G_OBJECT (roller_actor), "info-roller-animated",
 * 		    G_CALLBACK (animated_cb), NULL);
 *   g_signal_connect (G_OBJECT (roller_actor), "info-roller-up-animation-started",
 * 		    G_CALLBACK (up_animation_started_cb), NULL);
 *   g_signal_connect (G_OBJECT (roller_actor), "info-roller-down-animation-started",
 * 		    G_CALLBACK (down_animation_started_cb), NULL);
 *
 *   header_model = (ThornburyModel*) thornbury_list_model_new (COLUMN_LAST,
 *                                                             G_TYPE_POINTER, NULL,
 *                                                             G_TYPE_STRING, NULL,
 *                                                             G_TYPE_STRING, NULL, -1);
 *   thornbury_model_append (header_model,
 *                          COLUMN_LEFT_ICON_TEXT, "/usr/share/mildenhall/test-drawer-base/icon_music_AC.png",
 *                          COLUMN_MID_TEXT, "ALBUMS", COLUMN_RIGHT_ICON_TEXT,
 *                          NULL, -1);
 *   g_object_set (roller_actor, "header-model", header_model, NULL);
 *
 *   model = (ThornburyModel *)thornbury_list_model_new (COLUMN_LAST,
 *                                                      G_TYPE_STRING, NULL,
 *                                                      G_TYPE_STRING, NULL,
 *                                                      G_TYPE_STRING, NULL,
 *                                                      G_TYPE_BOOLEAN, NULL,
 *                                                      G_TYPE_STRING, NULL,
 *                                                      G_TYPE_FLOAT, NULL,
 *                                                      G_TYPE_STRING, NULL,
 *                                                      G_TYPE_STRING, NULL,
 *                                                      G_TYPE_STRING, NULL,
 *                                                      -1);
 *   for (i = 0; i < num_of_items; i++)
 *   {
 *    g_autofree gchar *column_name = g_strdup_printf ("item number %i", i);
 *    g_autofree gchar *column_text = g_strdup_printf ("text for item %i", i);
 *    thornbury_model_append (model,
 *                           COLUMN_NAME, column_name,
 *                           COLUMN_ICON, i % 2 == 0 ? icon1 : icon2,
 *                           COLUMN_LABEL, column_name,
 *                           COLUMN_TOGGLE, i % 2,
 *                           COLUMN_VIDEO, i % 2 == 0 ? launcher_video_file : content_video_file,
 *                           COLUMN_EXTRA_HEIGHT, (i % 100.0),
 *                           COLUMN_COVER, covers[i % 3],
 *                           COLUMN_THUMB,NULL,
 *                           COLUMN_LONG_TEXT, column_text,
 *                           -1);
 *   }
 *   mildenhall_info_roller_add_attribute (MILDENHALL_INFO_ROLLER (roller_actor),
 *                                        "name", 0);
 *   mildenhall_info_roller_add_attribute (MILDENHALL_INFO_ROLLER (roller_actor),
 *                                        "label", 2);
 *   g_object_set (roller_actor,
 *                "item-type", TYPE_SAMPLE_VARIABLE_ITEM,
 *                "model", model,
 *                NULL);
 *  clutter_actor_add_child (stage, roller_actor);
 *
 * ]|
 *
 * Example for #MildenhallInfoRoller::info-roller-item-selected, #MildenhallInfoRoller::info-roller-animated,
 * #MildenhallInfoRoller::info-roller-up-animation-started, #MildenhallInfoRoller::info-roller-down-animation-started
 * callback implementations.
 *
 * |[<!-- language="C" -->
 * static void item_selected_cb (MildenhallInfoRoller *roller, guint row, gpointer data)
 * {
 *   g_debug ("selected row = %d", row);
 * }
 *
 * static void down_animation_started_cb (MildenhallInfoRoller *roller, gpointer data)
 * {
 *   g_debug ("%s", __FUNCTION__);
 * }
 *
 * static void up_animation_started_cb (MildenhallInfoRoller *roller, gpointer data)
 * {
 *   g_debug ("%s", __FUNCTION__);
 * }
 *
 * static void animated_cb (MildenhallInfoRoller *roller, gpointer data)
 * {
 *   g_debug ("%s", __FUNCTION__);
 * }
 *
 * ]|
 *
 * Since: 0.3.0
 */

#include "mildenhall_info_roller.h"
#include "sample-variable-item.h"

/***********************************************************************************
        @Macro Definitions
************************************************************************************/
#define INFO_ROLLER_DEBUG(...)   //g_print( __VA_ARGS__)

/* property enums */
enum _enInfoRollerProperty
{
        PROP_FIRST,
        PROP_WIDTH,
        PROP_HEIGHT,
	PROP_FIXED_ROLLER,
	PROP_ITEM_TYPE,
	PROP_HEADER_ICON_LEFT,
	PROP_HEADER_ICON_RIGHT,
	PROP_MODEL,
        PROP_HEADER_MODEL,
	PROP_SHOW,
	PROP_LIST_ROLLER,
        PROP_LAST
};

/* The signals emitted by the mildenhall meta info header */
enum _enInfRolleroSignals
{
        SIG_FIRST,
        SIG_INFO_ROLLER_ANIMATED,                       /* Emitted when animation cpmpletes */
        SIG_INFO_ROLLER_UP_ANIMATION_STARTED,              /* Emitted when animation starts */
        SIG_INFO_ROLLER_DOWN_ANIMATION_STARTED,              /* Emitted when animation starts */
	SIG_INFO_ROLLER_ITEM_SELECTED,			/* Emitted when info roller item gets selected */
        SIG_LAST
};

static guint32 info_roller_signals[SIG_LAST] = {0,};

//G_DEFINE_TYPE (MildenhallInfoRoller, mildenhall_info_roller, MILDENHALL_TYPE_ROLLER_CONTAINER)
G_DEFINE_TYPE (MildenhallInfoRoller, mildenhall_info_roller, CLUTTER_TYPE_ACTOR)

#define INFO_ROLLER_GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_INFO_ROLLER, MildenhallInfoRollerPrivate))

struct _MildenhallInfoRollerPrivate
{
	gfloat flWidth;                 // width of the info-roller
        //gfloat flHeight;                // height of the info-roller
	gfloat flRollerImageHeight;
	gfloat flSeamlessBottomHeight;
	gfloat flLineWidth;
	gfloat flRollerOffset;
	gfloat flHeaderOffset;

	guint uinAnimationDuration;
	gint inThreshold;

	ClutterActor *pHeader;
	ClutterActor *pBottomBar;
	ClutterActor *pRoller;
	ClutterActor *pRollerGrp;
	ClutterActor *pArrow;
	ClutterAnimation *pBottombarAnimation;
	ClutterColor lineColor;

	gboolean bFixedRoller;
	gboolean bBottomBarPressed;
	gboolean bInfoRollerAnimate;
	gboolean bInfoRollerDown;
	gboolean bRollerShown;
	gboolean bAnimationRunning;
	gboolean bListRoller;

	ThornburyModel *pHeaderModel;
	ThornburyModel *pModel;

	gchar *pSeamlessBottomFilePath;
	gchar *pShadowBottomFilePath;
	gchar *pArrowUpFilePath;
	gchar *pArrowDownFilePath;
	//gchar *pRollerActiveFilePath;
	//gchar *pRollerNormalFilePath;
};

/***************************************************************************************************************************
 * Internal Functions
 ***************************************************************************************************************************/
static void attach_animation(MildenhallInfoRoller *pInfoRoller);
static void swipe_cb(ClutterGestureAction *pAction, ClutterActor *pHeader, ClutterSwipeDirection swipeDirection, gpointer pUserData );


/********************************************************
 * Function : gesture_end
 * Description: callback on gesture end
 * Parameters:  ClutterGestureAction *, header/footer,
                userData
 * Return value: void
 ********************************************************/
static void gesture_end(ClutterGestureAction *pAction, ClutterActor *pActor, gpointer pUserData)
{
        MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (MILDENHALL_INFO_ROLLER (pUserData));
        gfloat flPressX, flPressY;
        gfloat flReleaseX, flReleaseY;
        ClutterSwipeDirection enDirection = 0;

	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(!MILDENHALL_IS_INFO_ROLLER (pUserData) )
        {
                g_warning("invalid info roller object\n");
                return;
        }

        clutter_gesture_action_get_press_coords (pAction,
                        0,
                        &flPressX, &flPressY);

        clutter_gesture_action_get_release_coords (pAction,
                        0,
                        &flReleaseX, &flReleaseY);

        INFO_ROLLER_DEBUG("INFO_ROLLER_DEBUG:press X = %f press Y = %f release X = %f release Y = %f\n", flPressX, flPressY, flReleaseX, flReleaseY);

        if ( (flReleaseX - flPressX > priv->inThreshold)
                        || (flReleaseY - flPressY > priv->inThreshold)
                        || (flPressX - flReleaseX > priv->inThreshold)
                        ||(flPressY - flReleaseY > priv->inThreshold) )
        {

		 //gdouble diff;
                /* get the difference of x and y */
                //diff = (flReleaseY - flPressY)/ (flReleaseX - flPressX);
                /* calculate the angle */
                //angle = atan(diff) * 180 / 3.14159265;

               // INFO_ROLLER_DEBUG("angle = %f \n", angle);

                /* angles on swipe event

                        -90
                  +45   |     -45
                        |
                -0 _____|_____ +0
			|
			|
		-45   	|     +45
			+90
		*/

		/* for up/down direction, angle varies between -0 to -90 and 0 to 90 */
		/* if pressed Y coord is greater than the released one, direction will be up
		 * otherwise down */
		if(flPressY >= flReleaseY)
		{
			INFO_ROLLER_DEBUG("direction is up\n");
			enDirection = CLUTTER_SWIPE_DIRECTION_UP;
		}
		else
		{
			INFO_ROLLER_DEBUG("direction is down\n");
			enDirection = CLUTTER_SWIPE_DIRECTION_DOWN;
		}

		swipe_cb(pAction, pActor, enDirection, pUserData);
        }

}

/********************************************************
 * Function : swipe_cb
 * Description: callabck on swipe action
 * Parameters: ClutterActor *, swipeDirection, pUserData
 * Return value: void
 ********************************************************/
//static void swipe_cb(ClutterSwipeAction *pAction, ClutterActor *pHeader, ClutterSwipeDirection swipeDirection, gpointer pUserData )
static void swipe_cb(ClutterGestureAction *pAction, ClutterActor *pHeader, ClutterSwipeDirection swipeDirection, gpointer pUserData )
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (MILDENHALL_INFO_ROLLER (pUserData));
	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(!MILDENHALL_IS_INFO_ROLLER (pUserData) )
        {
                g_warning("invalid info roller object\n");
                return;
        }

	priv->bBottomBarPressed = FALSE;
	if(swipeDirection ==CLUTTER_SWIPE_DIRECTION_UP)
        {
		if ( priv->bInfoRollerDown)
		{
			INFO_ROLLER_DEBUG("CLUTTER_SWIPE_DIRECTION_UP\n");
			attach_animation(MILDENHALL_INFO_ROLLER(pUserData));
		}
	}
	else if(swipeDirection ==CLUTTER_SWIPE_DIRECTION_DOWN)
        {
		if (! priv->bInfoRollerDown)
		{
		 	INFO_ROLLER_DEBUG("CLUTTER_SWIPE_DIRECTION_DOWN\n");
			attach_animation(MILDENHALL_INFO_ROLLER(pUserData));
		}
        }
	else
	{
		INFO_ROLLER_DEBUG("%d\n", swipeDirection);	/* do nothing */
	}

}

/****************************************************
 * Function : info_roller_item_activated_cb
 * Description: roller item activated callback
 * Parameters: rollerContainer*, uinRow, pUserData
 *
 * Return value: void
 *******************************************************/
static void info_roller_item_activated_cb (MildenhallRollerContainer *pCont, guint uinRow, gpointer pUserData)
{
	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(!MILDENHALL_IS_INFO_ROLLER (pUserData) )
        {
                g_warning("invalid info roller object\n");
                return;
        }
        /* emit the info roller item selected signal */
	g_signal_emit (G_OBJECT(pUserData), info_roller_signals[SIG_INFO_ROLLER_ITEM_SELECTED], 0, uinRow);
}



/****************************************************
 * Function : info_roller_timeline_completed_cb
 * Description: Callback on animation completed
 * Parameters: pAnimation, pUserData
 *
 * Return value: void
 *******************************************************/
static void info_roller_timeline_completed_cb(ClutterAnimation *pAnimation, gpointer pUserData)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (MILDENHALL_INFO_ROLLER (pUserData));
	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(!MILDENHALL_IS_INFO_ROLLER (pUserData) )
        {
                g_warning("invalid info roller object\n");
                return;
        }

        if(priv->bInfoRollerDown)
	{
		if(priv->pArrow && priv->pArrowUpFilePath)
		{
                	thornbury_ui_texture_set_from_file(priv->pArrow, priv->pArrowUpFilePath, 0, 0, FALSE, TRUE);
		}
		priv->bAnimationRunning = FALSE;
	}
        else
	{
		if(priv->pArrow && priv->pArrowDownFilePath)
		{
                	thornbury_ui_texture_set_from_file(priv->pArrow, priv->pArrowDownFilePath, 0, 0, FALSE, TRUE);
		//	clutter_actor_set_opacity(priv->pRoller, 0);
		}
	}
	/* reset the pressed flag */
	priv->bBottomBarPressed = FALSE;


	priv->pBottombarAnimation = NULL;
 	/* nfo roller animated  signal emission */
	g_signal_emit(MILDENHALL_INFO_ROLLER(pUserData), info_roller_signals[SIG_INFO_ROLLER_ANIMATED], 0, NULL);

        return;
}

/****************************************************
 * Function : bottom_bar_pullupdown_start
 * Description: Callback on button press on header/bottom bar
 * Parameters: pActor,pEvent,pUserData
 *
 * Return value: gboolean (signal handled or not)
 *******************************************************/
static gboolean bottom_bar_pullupdown_start(ClutterActor *pActor, ClutterEvent *pEvent, gpointer pUserData)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (MILDENHALL_INFO_ROLLER (pUserData));
	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(!MILDENHALL_IS_INFO_ROLLER (pUserData) )
        {
                g_warning("invalid info roller object\n");
                return FALSE;
        }

	/* update the bottom press flag */
	priv->bBottomBarPressed = TRUE;

	if(priv->pRoller)
	{

		ThornburyModel *pModel = NULL;
		g_object_get(priv->pRoller, "model", &pModel, NULL);
		if(NULL !=pModel && (thornbury_model_get_n_rows(pModel) >= 2) )
			g_object_set(priv->pRoller, "focused-row", 1, NULL);
	}
	return TRUE;
}

/****************************************************
 * Function : bottom_bar_pullupdown
 * Description: Callback on button release on header/bottom bar
 * Parameters: pActor,pEvent,pUserData
 *
 * Return value: gboolean (signal handled or not)
 *******************************************************/
static gboolean bottom_bar_pullupdown(ClutterActor *pActor, ClutterEvent *pEvent, gpointer pUserData)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (MILDENHALL_INFO_ROLLER (pUserData));
	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(!MILDENHALL_IS_INFO_ROLLER (pUserData) )
        {
                g_warning("invalid info roller object\n");
                return FALSE;
        }

	if(priv->bBottomBarPressed == FALSE)
		return FALSE;

	/* update the pressed flag */
	priv->bBottomBarPressed = TRUE;

	/* check if roller group is created */
	if(! priv->pRollerGrp)
		return FALSE;

	/* attach animation */
	attach_animation(MILDENHALL_INFO_ROLLER(pUserData));

	return FALSE;

}

/****************************************************
 * Function : header_event_cb
 * Description: Callback on button press/release on header/bottom bar
 * Parameters: pActor,pEvent,pUserData
 *
 * Return value: gboolean (signal handled or not)
 *******************************************************/
static gboolean header_event_cb(ClutterActor *pActor, ClutterEvent *pEvent, gpointer pUserData)
{
	if (pEvent->type == CLUTTER_BUTTON_PRESS || pEvent->type == CLUTTER_TOUCH_BEGIN)
		bottom_bar_pullupdown_start(pActor, pEvent, pUserData);
	else if (pEvent->type == CLUTTER_BUTTON_RELEASE || pEvent->type == CLUTTER_TOUCH_END)
		bottom_bar_pullupdown(pActor, pEvent, pUserData);
	else
	{
		return FALSE;
	}
        return FALSE;
}

/***********************************************************
 * Function : attach_animation
 * Description: attach up/down animation based on roller state
 * Parameters:  pInfoRoller*
 *
 ************************************************************/
static void attach_animation(MildenhallInfoRoller *pInfoRoller)
{
        MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);

        /*Create the path for movement and attach a behavior path for the info actor */
        gfloat x = clutter_actor_get_x (priv->pRollerGrp);
        gfloat y = clutter_actor_get_y (priv->pRollerGrp);

	INFO_ROLLER_DEBUG ("%s\n", __FUNCTION__);

	/* if roller is already down, move it up */
	if ( priv->bInfoRollerDown)
	{
		y = 0.0;
		INFO_ROLLER_DEBUG("pull up y position = %f\n", y);
		 /* update the show flag */
                priv->bRollerShown = FALSE;

	}
	/* otherwise move it down to y position */
	else
	{
		y = priv->flRollerImageHeight;
		clutter_actor_set_opacity(priv->pRoller, 0xff);
		/* update the show flag */
                priv->bRollerShown = TRUE;
		/* update the show animation flag */
		priv->bAnimationRunning = TRUE;
		INFO_ROLLER_DEBUG("pull down y position = %f\n", y);
	}

	priv->bAnimationRunning = TRUE;

	/* if animate flag is set, pull up/down with animation */
	if(priv->bInfoRollerAnimate == FALSE)
	{
		clutter_actor_detach_animation(priv->pRollerGrp);
		clutter_actor_set_y(priv->pRollerGrp, y);
		priv->bInfoRollerDown = ! priv->bInfoRollerDown;
		info_roller_timeline_completed_cb(NULL, pInfoRoller);
		priv->bInfoRollerAnimate = TRUE;
	}
	else
	{

		/* roller pull up/down with animation */
		clutter_actor_detach_animation(priv->pRollerGrp);
		priv->pBottombarAnimation =  clutter_actor_animate (priv->pRollerGrp,
				CLUTTER_EASE_OUT_SINE,
				priv->uinAnimationDuration,
				"x", x,
				"y", y,
				NULL);

		 g_signal_connect_after (priv->pBottombarAnimation, "completed",
                                G_CALLBACK (info_roller_timeline_completed_cb),
                                pInfoRoller);
		if(priv->bInfoRollerDown)
		{
			/* animation started signal emission */
			g_signal_emit(pInfoRoller, info_roller_signals[SIG_INFO_ROLLER_UP_ANIMATION_STARTED], 0, NULL);
		}
		else
		{
			/* animation started signal emission */
                        g_signal_emit(pInfoRoller, info_roller_signals[SIG_INFO_ROLLER_DOWN_ANIMATION_STARTED], 0, NULL);
		}
		/*Change the state of the metaRollerDown flag*/
		priv->bInfoRollerDown = ! priv->bInfoRollerDown;
	}
}

/****************************************************
 * Function : create_bottom_bar
 * Description: create roller bottom bar
 * Parameters: pInfoRoller
 *
 * Return value: pBottomBar*
 *******************************************************/
static ClutterActor *create_bottom_bar(MildenhallInfoRoller *pInfoRoller)
{
        MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
        ClutterActor *pTexture = NULL;
        ClutterActor *pLine = NULL;
        ClutterActor *pLine2 = NULL;
        /*Create the meta bottom bar group, set a seamless and all lines and the arrow*/
        ClutterActor *pBottomBar = clutter_actor_new ();

	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(!MILDENHALL_IS_INFO_ROLLER (pInfoRoller) )
        {
                g_warning("invalid info roller object\n");
                return FALSE;
        }

	if(priv->pSeamlessBottomFilePath)
	{
		pTexture = thornbury_ui_texture_create_new(priv->pSeamlessBottomFilePath, priv->flWidth,priv->flSeamlessBottomHeight, FALSE, TRUE);
		clutter_actor_set_position(pTexture, 0.0, 0.0);
		clutter_actor_add_child(pBottomBar, pTexture);
	}

	/* add top line */
	pLine = clutter_actor_new();
	g_object_set(pLine, "x",  0.0, "y",  0.0, "width",  priv->flWidth, "height", priv->flLineWidth, "background-color",  &priv->lineColor, NULL);
	clutter_actor_add_child(pBottomBar, pLine);
	/* add one more top line to match the drawer color */
	pLine2 = clutter_actor_new();
	g_object_set(pLine2, "x",  0.0, "y",  0.0, "width",  priv->flWidth, "height", priv->flLineWidth, "background-color",  &priv->lineColor, NULL);
	clutter_actor_add_child(pBottomBar, pLine2);

	/* bottom shadow */
	if(priv->pShadowBottomFilePath)
	{
		ClutterActor *pBottomShadow =  thornbury_ui_texture_create_new(priv->pShadowBottomFilePath, priv->flWidth, priv->flSeamlessBottomHeight, FALSE, TRUE);
		if(pBottomShadow)
		{
			clutter_actor_add_child(pBottomBar, pBottomShadow);
			clutter_actor_set_position(pBottomShadow, 0, clutter_actor_get_height(pTexture) );
		}
	}

	/* mid arrow */
	if(priv->pArrowDownFilePath)
	{
		priv->pArrow = thornbury_ui_texture_create_new(priv->pArrowDownFilePath, 0, 0, FALSE, TRUE);
		if(priv->pArrow)
		{
			gint flX = (priv->flWidth - clutter_actor_get_width(priv->pArrow)) / 2;
			clutter_actor_set_position(priv->pArrow,
					(gfloat)flX,
					(priv->flSeamlessBottomHeight - clutter_actor_get_height(priv->pArrow)) / 2);
			clutter_actor_add_child(pBottomBar, priv->pArrow);
		}
	}
        return pBottomBar;


}

/********************************************************
 * Function : create_roller
 * Description: create fix/variable roller
 * Parameter :  MildenhallInfoRoller *, gboolean
 * Return value: void
 ********************************************************/
static void create_roller(MildenhallInfoRoller *pInfoRoller, gboolean bFixedRoller)
{
	MildenhallInfoRollerPrivate  *priv = INFO_ROLLER_GET_PRIVATE(pInfoRoller);

	if(bFixedRoller)
		priv->pRoller = g_object_new (MILDENHALL_TYPE_ROLLER_CONTAINER,
				"width", priv->flWidth,
				"height", priv->flRollerImageHeight + priv->flRollerOffset,
				"roller-type", MILDENHALL_FIXED_ROLLER_CONTAINER,
				"roll-over", priv->bListRoller?FALSE:TRUE,
				NULL);
	else
		priv->pRoller = g_object_new (MILDENHALL_TYPE_ROLLER_CONTAINER,
				"width", priv->flWidth,
				"height", priv->flRollerImageHeight + priv->flRollerOffset,
				"roller-type", MILDENHALL_VARIABLE_ROLLER_CONTAINER,
				"roll-over", priv->bListRoller?FALSE:TRUE,
				NULL);

	/* update the bg for info roller */
	if(priv->pRoller)
	{
		clutter_actor_add_child(priv->pRollerGrp, priv->pRoller);

		/* set the position */
		clutter_actor_set_clip(priv->pRoller, 0, 0, priv->flWidth, priv->flRollerImageHeight);
		clutter_actor_set_position(priv->pRoller, 0.0, -(priv->flRollerImageHeight - priv->flHeaderOffset + 2) );
		//clutter_actor_set_opacity(priv->pRoller, 0);
		g_signal_connect (G_OBJECT (priv->pRoller), "roller-item-activated", G_CALLBACK (info_roller_item_activated_cb), pInfoRoller);
	}
}

static void row_changed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallInfoRoller *pInfoRoller)
{
  MildenhallInfoRollerPrivate  *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);

  mildenhall_roller_container_refresh (MILDENHALL_ROLLER_CONTAINER (priv->pRoller));
}

/********************************************************
 * Function : mildenhall_info_roller_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void mildenhall_info_roller_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
        MildenhallInfoRoller *pInfoRoller = pUserData;
        MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
        gchar *pStyleKey = g_strdup (pKey);

	INFO_ROLLER_DEBUG ("%s\n", __FUNCTION__);
	g_return_if_fail (MILDENHALL_IS_INFO_ROLLER (pUserData));

        if(g_strcmp0(pStyleKey, "seamless-bottom-image") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pSeamlessBottomFilePath = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        INFO_ROLLER_DEBUG("seamless-top-image = %s\n", priv->pSeamlessBottomFilePath);
                }
        }
        else if(g_strcmp0(pStyleKey, "shadow-bottom-image") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pShadowBottomFilePath = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        INFO_ROLLER_DEBUG("seamless-top-image = %s\n", priv->pShadowBottomFilePath);
                }
        }
        else if(g_strcmp0(pStyleKey, "arrow-up") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pArrowUpFilePath = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        INFO_ROLLER_DEBUG("arrow-up = %s\n", priv->pArrowUpFilePath);
                }
        }
        else if(g_strcmp0(pStyleKey, "arrow-down") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pArrowDownFilePath = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        INFO_ROLLER_DEBUG("arrow-down = %s\n", priv->pArrowDownFilePath);
                }
        }
	else if(g_strcmp0(pStyleKey, "seamless-bottom-height") == 0)
        {
                priv->flSeamlessBottomHeight = g_value_get_double(pValue);
                INFO_ROLLER_DEBUG("seamless-bottom-height = %f\n", priv->flSeamlessBottomHeight);
        }

	else if(g_strcmp0(pStyleKey, "roller-bg-height") == 0)
        {
                priv->flRollerImageHeight = g_value_get_double(pValue);
                INFO_ROLLER_DEBUG("roller-bg-height = %f\n", priv->flRollerImageHeight);
        }
	else if(g_strcmp0(pStyleKey, "roller-bg-width") == 0)
        {
                priv->flWidth = g_value_get_double(pValue);
                INFO_ROLLER_DEBUG("roller-bg-width = %f\n", priv->flWidth);
        }
        else if(g_strcmp0(pStyleKey, "line-color") == 0)
        {
                clutter_color_from_string(&priv->lineColor,  g_value_get_string(pValue));
        }
	else if(g_strcmp0(pStyleKey, "line-width") == 0)
        {
                priv->flLineWidth = g_value_get_double(pValue);
                INFO_ROLLER_DEBUG("line-width = %f\n", priv->flLineWidth);
        }
	else if(g_strcmp0(pStyleKey, "roller-clip-area") == 0)
        {
                priv->flRollerOffset = g_value_get_double(pValue);
                INFO_ROLLER_DEBUG("roller-clip-area = %f\n", priv->flRollerOffset);
        }
	else if(g_strcmp0(pStyleKey, "header-clip-area") == 0)
        {
                priv->flHeaderOffset = g_value_get_double(pValue);
                INFO_ROLLER_DEBUG("header-clip-area = %f\n", priv->flHeaderOffset);
        }
	else if(g_strcmp0(pStyleKey, "animation-duration") == 0)
        {
                priv->uinAnimationDuration = g_value_get_int64(pValue);
                INFO_ROLLER_DEBUG("animation-duration = %d\n", priv->uinAnimationDuration);

        }
	else
	{
		;
	}

	if(NULL != pStyleKey)
	{
		g_free(pStyleKey);
		pStyleKey = NULL;
	}
}

/********************************************************
 * Function : mildenhall_info_roller_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_info_roller_get_property (GObject *pObject,
					guint       uinPropertyID,
        				GValue     *pValue,
                               		GParamSpec *pspec)
{
        if(!MILDENHALL_IS_INFO_ROLLER(pObject))
        {
                g_warning("invalid roller object\n");
                return;
        }
	switch (uinPropertyID)
    	{
		case PROP_WIDTH:
			g_value_set_float (pValue,  mildenhall_info_roller_get_width(MILDENHALL_INFO_ROLLER (pObject)));
			break;
                case PROP_HEIGHT:
			g_value_set_float (pValue,  mildenhall_info_roller_get_height(MILDENHALL_INFO_ROLLER (pObject)));
                        break;
                case PROP_HEADER_MODEL:
                        g_value_set_object (pValue, mildenhall_info_roller_get_header_model(MILDENHALL_INFO_ROLLER (pObject)) );
                        break;
		case PROP_HEADER_ICON_LEFT:
                        g_value_set_boolean (pValue,  mildenhall_info_roller_get_header_icon_left(MILDENHALL_INFO_ROLLER (pObject)));
                        break;
                case PROP_HEADER_ICON_RIGHT:
                        g_value_set_boolean (pValue,  mildenhall_info_roller_get_header_icon_right(MILDENHALL_INFO_ROLLER (pObject)));
                        break;
                case PROP_MODEL:
                        g_value_set_object (pValue, mildenhall_info_roller_get_model(MILDENHALL_INFO_ROLLER (pObject)) );
                        break;
                case PROP_ITEM_TYPE:
                        g_value_set_gtype(pValue, mildenhall_info_roller_get_item_type(MILDENHALL_INFO_ROLLER (pObject)) );
                        break;
                 case PROP_FIXED_ROLLER:
                        g_value_set_boolean (pValue, mildenhall_info_roller_get_fix_roller (MILDENHALL_INFO_ROLLER (pObject)) );
                        break;
                 case PROP_SHOW:
                        g_value_set_boolean (pValue, mildenhall_info_roller_get_show (MILDENHALL_INFO_ROLLER (pObject)) );
                        break;
				case PROP_LIST_ROLLER:
						g_value_set_boolean (pValue, mildenhall_info_roller_get_list_roller(MILDENHALL_INFO_ROLLER (pObject)) );
						break;
    		default:
      			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
    	}
}

/********************************************************
 * Function : mildenhall_info_roller_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_info_roller_set_property (GObject *pObject,
					guint         uinPropertyID,
	                                const GValue *pValue,
        	                        GParamSpec   *pspec)
{
	if(!MILDENHALL_IS_INFO_ROLLER(pObject))
        {
                g_warning("invalid roller object\n");
                return;
        }
	switch (uinPropertyID)
    	{
		case PROP_WIDTH:
                        break;
                case PROP_HEIGHT:
                        break;
		case PROP_HEADER_MODEL:
                        mildenhall_info_roller_set_header_model(MILDENHALL_INFO_ROLLER (pObject), g_value_get_object (pValue));
                        break;
		case PROP_HEADER_ICON_LEFT:
                        mildenhall_info_roller_set_header_icon_left(MILDENHALL_INFO_ROLLER (pObject), g_value_get_boolean (pValue));
                        break;
                case PROP_HEADER_ICON_RIGHT:
                        mildenhall_info_roller_set_header_icon_right(MILDENHALL_INFO_ROLLER (pObject), g_value_get_boolean (pValue));
                        break;
		case PROP_MODEL:
                        mildenhall_info_roller_set_model(MILDENHALL_INFO_ROLLER (pObject), g_value_get_object (pValue));
                        break;
		case PROP_ITEM_TYPE:
			mildenhall_info_roller_set_item_type(MILDENHALL_INFO_ROLLER (pObject), g_value_get_gtype(pValue));
		        break;
		 case PROP_FIXED_ROLLER:
                          mildenhall_info_roller_set_fix_roller (MILDENHALL_INFO_ROLLER (pObject), g_value_get_boolean (pValue));
                        break;
		 case PROP_SHOW:
                        mildenhall_info_roller_set_show(MILDENHALL_INFO_ROLLER (pObject), g_value_get_boolean (pValue));
                        break;
		 case PROP_LIST_ROLLER:
			mildenhall_info_roller_set_list_roller(MILDENHALL_INFO_ROLLER (pObject), g_value_get_boolean (pValue));
			break;
		default:
      			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
    	}
}

/********************************************************
 * Function : mildenhall_info_roller_dispose
 * Description: Dispose the info roller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_info_roller_dispose (GObject *pObject)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE(MILDENHALL_INFO_ROLLER(pObject));
	if(priv->pSeamlessBottomFilePath)
	{
		g_free(priv->pSeamlessBottomFilePath);
		priv->pSeamlessBottomFilePath = NULL;
	}
	if(priv->pShadowBottomFilePath)
	{
		g_free(priv->pShadowBottomFilePath);
		priv->pShadowBottomFilePath = NULL;
	}
	if(priv->pArrowUpFilePath)
	{
		g_free(priv->pArrowUpFilePath);
		priv->pArrowUpFilePath = NULL;
	}
	if(priv->pArrowDownFilePath)
	{
		g_free(priv->pArrowDownFilePath);
		priv->pArrowDownFilePath = NULL;
	}
	if(NULL != priv->pHeader)
	{
		clutter_actor_destroy(priv->pHeader);
		priv->pHeader = NULL;
	}
	if(NULL != priv->pRoller)
	{
		clutter_actor_destroy(priv->pRoller);
		priv->pRoller = NULL;
	}
	if(NULL != priv->pBottomBar)
	{
		clutter_actor_destroy(priv->pBottomBar);
		priv->pBottomBar = NULL;
	}
	G_OBJECT_CLASS (mildenhall_info_roller_parent_class)->dispose (pObject);
}

/********************************************************
 * Function : mildenhall_info_roller_finalize
 * Description: Finalize the meta info heade object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_info_roller_finalize (GObject *pObject)
{
	G_OBJECT_CLASS (mildenhall_info_roller_parent_class)->finalize (pObject);
}

/********************************************************
 * Function : mildenhall_info_roller_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_info_roller_class_init (MildenhallInfoRollerClass *klass)
{
	GObjectClass *pObjectClass = G_OBJECT_CLASS (klass);
	//ClutterActorClass *pActorClass = CLUTTER_ACTOR_CLASS (klass);

	GParamSpec *pspec = NULL;

	g_type_class_add_private (klass, sizeof (MildenhallInfoRollerPrivate));

	pObjectClass->get_property = mildenhall_info_roller_get_property;
	pObjectClass->set_property = mildenhall_info_roller_set_property;
	pObjectClass->dispose = mildenhall_info_roller_dispose;
	pObjectClass->finalize = mildenhall_info_roller_finalize;

	/**
         *InfoRoller :width:
         *
         * Width of the info roller
 	 * Read Only property
	 *
         */
        pspec = g_param_spec_float ("width",
                        "Width",
                        "Width of theinfo roller ",
                        0.0, G_MAXFLOAT,
                        0.0,
                        G_PARAM_READABLE);
        g_object_class_install_property (pObjectClass, PROP_WIDTH, pspec);

        /**
         * InfoRoller:height:
         *
         * Height of the info roller
	 * Read Only property
 	 *
         */
        pspec = g_param_spec_float ("height",
                        "Height",
                        "Height of the info roller",
                        0.0, G_MAXFLOAT,
                        0.0,
                        G_PARAM_READABLE);
        g_object_class_install_property ( pObjectClass, PROP_HEIGHT, pspec);

	 /**
         * InfoRoller:fix-roller:
         *
         * Type of the roller to be created:
         *              TRUE = LIGHTWOOD_FIXED_ROLLER
         *              FALSE = MILDENHALL_VARIABLE_ROLLER
         * Default: TRUE
         */
        pspec = g_param_spec_boolean ("fix-roller",
                        "Fix-Roller",
                        "Roller Type to create the roller",
			TRUE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_FIXED_ROLLER, pspec);

	 /**
         * InfoRoller: roller-type:
         *
         * Type of the roller to be created:
         *              LIGHTWOOD_FIXED_ROLLER
         *              MILDENHALL_VARIABLE_ROLLER
         * Default: LIGHTWOOD_FIXED_ROLLER
         */
        pspec = g_param_spec_gtype ("item-type",
                        "ItemType",
                        "Item Type for the roller item factory",
                        CLUTTER_TYPE_ACTOR,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_ITEM_TYPE, pspec);

	 /**
         * InfoRoller:model:
         *
         * model for the info roller item
         *
         * Default: NULL
         */
	pspec = g_param_spec_object ("model",
                        "Model",
                        "Model to use to construct the items",
                        G_TYPE_OBJECT,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_MODEL, pspec);

	/**
         * InfoRoller:header-model:
         *
         * model for the info roller header.
         * 1. left-icon
         * 2. left-text
         * 3. right-icon-text
         * 4. middle-text
         *
         * Default: NULL
         */
        pspec = g_param_spec_object ("header-model",
                        "Header-Model",
                        "header model having info roller header data",
                        G_TYPE_OBJECT,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_HEADER_MODEL, pspec);

	 /**
         * InfoRoller:header-left-icon:
         *
         * Set this property to FALSE to set text at header left position
         *
         * Default: TRUE
         */
        pspec = g_param_spec_boolean ("header-left-icon",
                        "Header-Left-Icon",
                        "Whether the icon or text on left side of header",
                        TRUE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_HEADER_ICON_LEFT, pspec);

         /**
         * InfoRoller:header-right-icon:
         *
         * Set this property to FALSE to set text at header right position
         *
         * Default: TRUE
         */
        pspec = g_param_spec_boolean ("header-right-icon",
                        "Header-Right-Icon",
                        "Whether the icon or text on right side of header",
                        TRUE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_HEADER_ICON_RIGHT, pspec);

	/**
         * pInfoRoller:show:
         *
         * Set this property to TRUE to show info roller
         *
         * Default: FALSE
         */
        pspec = g_param_spec_boolean ("show",
                        "Show-Info-Roller",
                        "To show the info roller",
                        TRUE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_SHOW, pspec);

	/**
         * pInfoRoller:list-roller:
         *
         * This property is used to set info roller as a list roller/roll over.
         *
         * Default: TRUE
         */
	pspec = g_param_spec_boolean ("list-roller",
                                "List roller or Roll over",
                                "List roller roll over disabled",
                                TRUE,
                                G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_LIST_ROLLER, pspec);


	 /**
         * InfoRoller::info-roller-animated
  	 * @mildenhallinfoRoller: The object which received the signal
  	 *
	 * Signal emitted when the info roller
         * is animated (is either pulled down or expanded upwards)
         *
         */
        info_roller_signals[SIG_INFO_ROLLER_ANIMATED] =
                g_signal_new ("info-roller-animated",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE |
                        G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (MildenhallInfoRollerClass,
                                	info_roller_animated),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0);
	 /**
         * InfoRoller::info-roller-down-animation-started
  	 * @mildenhallinfoRoller: The object which received the signal
         *
	 * Signal emitted when the info roller
         * down animation starts
         *
         */
        info_roller_signals[SIG_INFO_ROLLER_DOWN_ANIMATION_STARTED] =
                g_signal_new ("info-roller-down-animation-started",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE |
                        G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (MildenhallInfoRollerClass,
        	                        info_roller_down_animation_started),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0);
	 /**
         * InfoRoller::info-roller-up-animation-started
  	 * @mildenhallinfoRoller: The object which received the signal
         *
	 * Signal emitted when the info roller
         * up animation starts
         *
         */
        info_roller_signals[SIG_INFO_ROLLER_UP_ANIMATION_STARTED] =
                g_signal_new ("info-roller-up-animation-started",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE |
                        G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (MildenhallInfoRollerClass,
        	                        info_roller_up_animation_started),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0);

	 /**
         * InfoRoller::info-roller-item-selected
  	 * @mildenhallinfoRoller: The object which received the signal
         *
	 * Signal emitted when an item gets activated in info roller
         *
         */
        info_roller_signals[SIG_INFO_ROLLER_ITEM_SELECTED] =
                g_signal_new ("info-roller-item-selected",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE |
                        G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (MildenhallInfoRollerClass,
	                                info_roller_item_selected),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__UINT,
                        G_TYPE_NONE, 1,
                        G_TYPE_UINT, NULL);

}

/********************************************************
 * Function : mildenhall_info_roller_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void mildenhall_info_roller_init (MildenhallInfoRoller *pSelf)
{
        /* get the hash table for style properties */
        GHashTable *styleHash = thornbury_style_set (PKGDATADIR"/mildenhall_info_roller_style.json");
        ClutterSettings *settings = clutter_settings_get_default ();

	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);
	pSelf->priv = INFO_ROLLER_GET_PRIVATE (pSelf);

	pSelf->priv->pRollerGrp = NULL;
	pSelf->priv->pRoller = NULL;
	pSelf->priv->pArrow = NULL;
	pSelf->priv->pBottomBar = NULL;
	pSelf->priv->pSeamlessBottomFilePath = NULL;
	pSelf->priv->pShadowBottomFilePath = NULL;
	pSelf->priv->pArrowUpFilePath = NULL;
	pSelf->priv->pArrowDownFilePath = NULL;
	pSelf->priv->pBottombarAnimation =  NULL;
	//pSelf->priv->pRollerNormalFilePath = NULL;
	//pSelf->priv->pRollerActiveFilePath = NULL;

	pSelf->priv->bFixedRoller = TRUE;
	pSelf->priv->bInfoRollerAnimate = TRUE;
	pSelf->priv->bInfoRollerDown = FALSE;
	pSelf->priv->bBottomBarPressed = FALSE;
	pSelf->priv->bRollerShown = FALSE;
	pSelf->priv->bAnimationRunning = FALSE;
	pSelf->priv->bListRoller = TRUE;

	pSelf->priv->flRollerImageHeight = 0.0;
	pSelf->priv->flSeamlessBottomHeight = 0.0;
	pSelf->priv->flLineWidth = 0.0;
	pSelf->priv->flHeaderOffset = 0.0;
	pSelf->priv->flRollerOffset = 0.0;

	pSelf->priv->uinAnimationDuration = 0;

	/* create the roller group */
	pSelf->priv->pRollerGrp = clutter_actor_new();
	clutter_actor_add_child(CLUTTER_ACTOR(pSelf), pSelf->priv->pRollerGrp);

        /* pares the hash for styles */
        if(NULL != styleHash)
        {
                GHashTableIter iter;
                gpointer key, value;

                g_hash_table_iter_init(&iter, styleHash);
                /* iter per layer */
                while (g_hash_table_iter_next (&iter, &key, &value))
                {
                        GHashTable *pHash = value;
                        if(NULL != pHash)
                        {
                                g_hash_table_foreach(pHash, mildenhall_info_roller_parse_style, pSelf);
                        }
                }
                /* free the style hash */
                thornbury_style_free(styleHash);
        }

	/* add meta info header */
	pSelf->priv->pHeader =  CLUTTER_ACTOR(mildenhall_meta_info_header_new());
	if(pSelf->priv->pHeader)
	{
                ClutterAction *pGestureAction = clutter_gesture_action_new ();
		clutter_actor_add_child(CLUTTER_ACTOR(pSelf), pSelf->priv->pHeader);
		clutter_actor_set_reactive(pSelf->priv->pHeader, TRUE);
		/* hide the bottom bar from the header */
		//g_object_set(pSelf->priv->pHeader, "show-bottom", FALSE, NULL);

		g_object_get(pSelf->priv->pHeader, "width", &pSelf->priv->flWidth, NULL);
		clutter_actor_set_clip(pSelf->priv->pHeader, 0, 0, pSelf->priv->flWidth, pSelf->priv->flHeaderOffset);

		/* add swipe action to header and connect signals signals */
		/*ClutterAction *pSwipeAction = clutter_swipe_action_new();
		clutter_actor_add_action(pSelf->priv->pHeader, pSwipeAction);
		g_signal_connect(pSwipeAction, "swept", G_CALLBACK(swipe_cb), pSelf);*/

		 /* add gesture action to get swipe event on the lightwood button*/
		/* Inhancement: May be we can implementt a generic swipe gesture */
		clutter_actor_add_action (pSelf->priv->pHeader, pGestureAction);
		//g_signal_connect (pSelf->priv->pGestureAction, "gesture-begin", G_CALLBACK (button_gesture_begin), pSelf);
		//g_signal_connect (pSelf->priv->pGestureAction, "gesture-progress", G_CALLBACK (button_gesture_progress), pSelf);
		g_signal_connect (pGestureAction, "gesture-end", G_CALLBACK (gesture_end), pSelf);


		/* add signals */
		g_signal_connect(pSelf->priv->pHeader, "button-press-event", G_CALLBACK(header_event_cb), pSelf);//G_CALLBACK(bottom_bar_pullupdown_start), pSelf);
		g_signal_connect(pSelf->priv->pHeader, "button-release-event", G_CALLBACK(header_event_cb), pSelf);//G_CALLBACK(bottom_bar_pullupdown), pSelf);
		g_signal_connect(pSelf->priv->pHeader, "touch-event", G_CALLBACK(header_event_cb), pSelf);//G_CALLBACK(bottom_bar_pullupdown), pSelf);
	}

	//clutter_actor_set_position(pSelf, 281.0, 50.0);
	INFO_ROLLER_DEBUG(" info roller width = header width = %f\n", pSelf->priv->flWidth);


	g_object_get (settings, "dnd-drag-threshold", &pSelf->priv->inThreshold, NULL);

        INFO_ROLLER_DEBUG("INFO_ROLLER_DEBUG: threshold = %d\n", pSelf->priv->inThreshold);

}


/*******************************************************************************************************
 *
 * Exposed APIs
 *
 *******************************************************************************************************/
/**
 * mildenhall_info_roller_set_fix_roller:
 * @pInfoRoller: Object reference
 * @bFixedRoller: TRUE for Fixed Roller/ FALSE for Variable roller
 *
 * Function to set the type of roller.
 *
 */
void mildenhall_info_roller_set_fix_roller(MildenhallInfoRoller *pInfoRoller, gboolean bFixedRoller)
{

	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);
        g_return_if_fail ( MILDENHALL_IS_INFO_ROLLER (pInfoRoller ) );

	if( NULL != priv->pRoller )
        {
                clutter_actor_unparent( priv->pRoller );
                g_object_unref (priv->pRoller);
                priv->pRoller = NULL;
        }

	if(NULL == priv->pRollerGrp)
	{
		priv->pRollerGrp = clutter_actor_new();
		clutter_actor_add_child(CLUTTER_ACTOR(pInfoRoller), priv->pRollerGrp);
	}
	/* update the fix/variable roller flag */
	priv->bFixedRoller = bFixedRoller;
	/* create the roller */
	create_roller(pInfoRoller, bFixedRoller);

	/* add info roller bottom bar */
	if(NULL == priv->pBottomBar)
	{
                gfloat flY = clutter_actor_get_y (priv->pRoller) + priv->flRollerImageHeight ;
                ClutterAction *pGestureAction = clutter_gesture_action_new ();
		priv->pBottomBar = create_bottom_bar(pInfoRoller);
		g_object_set(priv->pBottomBar , "x", 0.0, "y", flY, "reactive", TRUE, NULL);
		g_signal_connect(priv->pBottomBar, "button-press-event", G_CALLBACK(header_event_cb), pInfoRoller);
		g_signal_connect(priv->pBottomBar, "button-release-event", G_CALLBACK(header_event_cb), pInfoRoller);
		g_signal_connect(priv->pBottomBar, "touch-event", G_CALLBACK(header_event_cb), pInfoRoller);

		 /* add gesture action to get swipe event on the lightwood button*/
                /* Inhancement: May be we can implementt a generic swipe gesture */
                clutter_actor_add_action (priv->pBottomBar, pGestureAction);
                //g_signal_connect (pButton->priv->pGestureAction, "gesture-begin", G_CALLBACK (button_gesture_begin), NULL);
                //g_signal_connect (pButton->priv->pGestureAction, "gesture-progress", G_CALLBACK (button_gesture_progress), NULL);
                g_signal_connect (pGestureAction, "gesture-end", G_CALLBACK (gesture_end), pInfoRoller);

		 /* add swipe action to header and connect signals signals */
                /*ClutterAction *pSwipeAction = clutter_swipe_action_new();
                clutter_actor_add_action(priv->pBottomBar, pSwipeAction);
                g_signal_connect(pSwipeAction, "swept", G_CALLBACK(swipe_cb), pInfoRoller);*/

		clutter_actor_add_child(priv->pRollerGrp, priv->pBottomBar);
	}
	g_object_notify (G_OBJECT(pInfoRoller), "fix-roller");
}

/**
 * mildenhall_info_roller_set_item_type:
 * @pInfoRoller: info roller object reference
 * @gType: GType for item type
 *
 * Function which the sets the item type
 */
void mildenhall_info_roller_set_item_type(MildenhallInfoRoller *pInfoRoller, GType gType)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);
        g_return_if_fail ( MILDENHALL_IS_INFO_ROLLER (pInfoRoller ) );

        if( NULL != priv->pRoller )
        {

		//g_object_set(priv->pRoller, "item-type", TYPE_SAMPLE_VARIABLE_ITEM, /*gType,*/ NULL);
		g_object_set(priv->pRoller, "item-type", gType, NULL);
	}
	g_object_notify (G_OBJECT(pInfoRoller), "item-type");
}

/**
 * mildenhall_info_roller_set_model:
 * @pInfoRoller: Object reference
 * @pModel: Value which need to be set for roller model
 *
 * Set the model used by the roller
 */
void mildenhall_info_roller_set_model(MildenhallInfoRoller *pInfoRoller, ThornburyModel *pModel)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);
        g_return_if_fail ( MILDENHALL_IS_INFO_ROLLER (pInfoRoller ) );

        if( NULL != priv->pRoller )
        {
                g_object_set(priv->pRoller, "model", pModel, NULL);
		g_signal_connect (pModel,
                                        "row-changed",
                                        G_CALLBACK (row_changed_cb),
                                        pInfoRoller);

        }
	g_object_notify (G_OBJECT(pInfoRoller), "model");
}

/**
 * mildenhall_info_roller_add_attribute
 * @pInfoRoller: object reference
 * @property: Name of the attribute
 * @column: Column number
 *
 * Maps a property of the item actors to a column of the current #ThornburyModel.
 *
 */
void mildenhall_info_roller_add_attribute(MildenhallInfoRoller *pInfoRoller, const gchar *pProperty, gint inColumn)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);
        g_return_if_fail ( MILDENHALL_IS_INFO_ROLLER (pInfoRoller ) );

        if(NULL != priv->pRoller)
		mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (priv->pRoller), pProperty, inColumn);

}

/**
 * mildenhall_info_roller_set_header_icon_left:
 * @pInfoRoller: info roller object reference
 * @bIconLeft: TRUE if icon else FALSE(in case of text)
 *
 * whether header left side has an icon or a text. MetaInfoHeader can
 * have more than one icon on left side which will toggle the icons
 * on click of the left icon and signal will get emitted.
 */
void mildenhall_info_roller_set_header_icon_left(MildenhallInfoRoller *pInfoRoller, gboolean bIconLeft)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);
        g_return_if_fail ( MILDENHALL_IS_INFO_ROLLER (pInfoRoller ) );

	if(NULL != priv->pHeader)
	{
		g_object_set(priv->pHeader, "left-icon", bIconLeft, NULL);
	}
        g_object_notify (G_OBJECT(pInfoRoller), "header-left-icon");

}

/**
 * mildenhall_info_roller_set_header_icon_right:
 * @pInfoRoller: info roller object reference
 * @bIconRight: TRUE if icon else FALSE(in case of text)
 *
 * whether header right end has an icon or a text.
 */
void mildenhall_info_roller_set_header_icon_right(MildenhallInfoRoller *pInfoRoller, gboolean bIconRight)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);
        g_return_if_fail ( MILDENHALL_IS_INFO_ROLLER (pInfoRoller ) );

	if(NULL != priv->pHeader)
	{
		g_object_set(priv->pHeader, "right-icon", bIconRight, NULL);
	}
        g_object_notify (G_OBJECT(pInfoRoller), "header-right-icon");
}

/**
 * mildenhall_info_roller_set_header_model:
 * @pInfoRoller: Object reference
 * @pModel: Value which need to be set for header model
 *
 * Set the model used by the meta info header
 */
void mildenhall_info_roller_set_header_model(MildenhallInfoRoller *pInfoRoller, ThornburyModel *pModel)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);

        g_return_if_fail ( MILDENHALL_IS_INFO_ROLLER (pInfoRoller ) );

        if(NULL != priv->pHeader)
		g_object_set(priv->pHeader, "model", pModel, NULL);

	g_object_notify (G_OBJECT(pInfoRoller), "header-model");
}

/**
 * mildenhall_info_roller_set_list_roller:
 * @pInfoRoller: info roller object reference
 * @bListRoller: TRUE if roller needs to be a list
 *
 *  info roller roll-over feature will be disabled if list roller
 *  is set as true
 */
void mildenhall_info_roller_set_list_roller(MildenhallInfoRoller *pInfoRoller, gboolean bListRoller)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);

        g_return_if_fail ( MILDENHALL_IS_INFO_ROLLER (pInfoRoller ) );

	priv->bListRoller = bListRoller;

	if(priv->pRoller)
	{
		if(priv->bListRoller)
			g_object_set(priv->pRoller, "roll-over", FALSE, NULL);
		else
			g_object_set(priv->pRoller, "roll-over", TRUE, NULL);
	}
}

/**
 * mildenhall_info_roller_set_show:
 * @pInfoRoller: info roller object reference
 * @bShow: TRUE if roller needs to be shown
 *
 *  info roller is shown with animation if set as TRUE.
 */
void mildenhall_info_roller_set_show(MildenhallInfoRoller *pInfoRoller, gboolean bShow)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);

        g_return_if_fail ( MILDENHALL_IS_INFO_ROLLER (pInfoRoller ) );

	if(bShow && priv->bAnimationRunning)
        {
                return;
        }
	/* if roller has to be hidden during show animation */
	if(!bShow && priv->bAnimationRunning)
	{
		clutter_actor_detach_animation(priv->pRollerGrp);
		priv->bAnimationRunning = FALSE;
		priv->bInfoRollerDown = TRUE;
	        /* as button press cb */
        	bottom_bar_pullupdown_start(NULL, NULL, pInfoRoller);
        	/* as button release cb */
        	bottom_bar_pullupdown(NULL, NULL, pInfoRoller);

	}
	/* if roller has to be hidden if already in show state */
	else if(!bShow && ! priv->bAnimationRunning && priv->bRollerShown)
	{
		priv->bInfoRollerDown = TRUE;
		/* as button press cb */
		bottom_bar_pullupdown_start(NULL, NULL, pInfoRoller);
		/* as button release cb */
		bottom_bar_pullupdown(NULL, NULL, pInfoRoller);
	}
	else if(bShow && ! priv->bAnimationRunning)
	{
		priv->bInfoRollerDown = FALSE;
		 /* as button press cb */
                bottom_bar_pullupdown_start(NULL, NULL, pInfoRoller);
                /* as button release cb */
                bottom_bar_pullupdown(NULL, NULL, pInfoRoller);
	}
}

/**
 * mildenhall_info_roller_get_list_roller:
 * @pInfoRoller: Object reference
 *
 * TRUE for  list roller, FLASE if roller has  roll-over
 *
 * Returns: TRUE/FALSE whether roller is of list/roll-over
 */

gboolean mildenhall_info_roller_get_list_roller(MildenhallInfoRoller *pInfoRoller)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);
         if(!MILDENHALL_IS_INFO_ROLLER (pInfoRoller) )
        {
                g_warning("invalid info roller object\n");
                return FALSE;
        }

        return priv->bListRoller;

}

/**
 * mildenhall_info_roller_get_fix_roller:
 * @pInfoRoller: Object reference
 *
 * TRUE for  FIXED_ROLLER, FLASE if roller is a variable roller
 *
 * Returns: TRUE/FALSE whether roller is of fix/variable type
 */
gboolean mildenhall_info_roller_get_fix_roller(MildenhallInfoRoller *pInfoRoller)
{

	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);
	 if(!MILDENHALL_IS_INFO_ROLLER (pInfoRoller) )
        {
                g_warning("invalid info roller object\n");
                return FALSE;
        }

	return priv->bFixedRoller;
}
/**
 * mildenhall_info_roller_get_header_icon_left:
 * @pInfoRoller: info roller object reference
 *
 * whether header left side has an icon or a text.
 *
 * Returns: TRUE if header left side has an icon, FALSE if text
 */
gboolean mildenhall_info_roller_get_header_icon_left(MildenhallInfoRoller *pInfoRoller)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
	gboolean bIconLeft = FALSE;

	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);

	if(!MILDENHALL_IS_INFO_ROLLER (pInfoRoller) )
	{
		g_warning("invalid info roller object\n");
		return FALSE;
	}

	/* get the header  "left-icon" property */
	if(NULL != priv->pHeader)
		g_object_set(priv->pHeader, "left-icon", &bIconLeft, NULL);

        return bIconLeft;
}

/**
 * mildenhall_info_roller_get_header_icon_right:
 * @pInfoRoller: info roller object reference
 *
 * whether header right side has an icon or a text.
 *
 * Returns: TRUE if header right side has an icon, FALSE if text
 */
gboolean mildenhall_info_roller_get_header_icon_right(MildenhallInfoRoller *pInfoRoller)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
	gboolean bIconRight = FALSE;

	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if (!  MILDENHALL_IS_INFO_ROLLER (pInfoRoller ) )
	{
		g_warning("invalid info roller object\n");
		return FALSE;
	}

        /* get the header "right-icon" property */
        if(NULL != priv->pHeader)
                g_object_set(priv->pHeader, "right-icon", &bIconRight, NULL);

        return bIconRight;
}

/**
 * mildenhall_info_roller_get_model:
 * @pInfoRoller: Object reference
 *
 * gets the roller model containing info roller item data
 *
 * Returns: model pointer
 */
ThornburyModel *mildenhall_info_roller_get_model(MildenhallInfoRoller *pInfoRoller)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
	ThornburyModel *pModel = NULL;

	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);

	if(! MILDENHALL_IS_INFO_ROLLER (pInfoRoller ) )
	{
		g_warning("invalid info roller object\n");
		return NULL;
	}

	if(NULL != priv->pRoller)
		g_object_get(priv->pRoller, "model", &pModel, NULL);

	return pModel;
}

/**
 * mildenhall_info_roller_get_header_model:
 * @pInfoRoller: Object reference
 *
 * gets the header model containing info roller header data
 *
 * Returns: header model pointer
 */
ThornburyModel *mildenhall_info_roller_get_header_model(MildenhallInfoRoller *pInfoRoller)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
	ThornburyModel *pModel = NULL;

	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);

	if(! MILDENHALL_IS_INFO_ROLLER (pInfoRoller ) )
	{
		g_warning("invalid info roller object\n");
		return NULL;
	}

	if(NULL != priv->pHeader)
		g_object_get(priv->pHeader, "model", &pModel, NULL);

	return pModel;
}

/**
 * mildenhall_info_roller_get_item_type:
 * @pInfoRoller: Object reference
 *
 * gets the GType of roller item.
 *
 * Returns: item type
 */
GType mildenhall_info_roller_get_item_type(MildenhallInfoRoller *pInfoRoller)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
	GType type = G_TYPE_NONE;

	INFO_ROLLER_DEBUG ("%s\n", __FUNCTION__);

        if(! MILDENHALL_IS_INFO_ROLLER (pInfoRoller ) )
        {
                g_warning("invalid info roller object\n");
                return type;
        }

	if(NULL != priv->pRoller)
		g_object_get(priv->pRoller, "item-type", &type, NULL);

	return type;
}

/**
 * mildenhall_info_roller_get_width:
 * @pInfoRoller: Object reference
 *
 * gets the width of info roller.
 *
 * Returns: width of the info roller
 */
gfloat mildenhall_info_roller_get_width(MildenhallInfoRoller *pInfoRoller)
{
        MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
        INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(! MILDENHALL_IS_INFO_ROLLER (pInfoRoller ) )
        {
                g_warning("invalid info roller object\n");
                return 0.0;
        }

	return priv->flWidth;
}

/**
 * mildenhall_info_roller_get_height:
 * @pInfoRoller: Object reference
 *
 * gets the height of info roller.
 *
 * Returns: height of the info roller
 */
gfloat mildenhall_info_roller_get_height(MildenhallInfoRoller *pInfoRoller)
{
        INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(! MILDENHALL_IS_INFO_ROLLER (pInfoRoller ) )
        {
                g_warning("invalid info roller object\n");
                return 0.0;
        }

	return clutter_actor_get_height(CLUTTER_ACTOR(pInfoRoller));
}

/**
 * mildenhall_info_roller_get_show:
 * @pInfoRoller: info roller object reference
 *
 * whether info roller is shown.
 *
 * Returns: TRUE if info roller is shown
 */
gboolean mildenhall_info_roller_get_show(MildenhallInfoRoller *pInfoRoller)
{
	MildenhallInfoRollerPrivate *priv = INFO_ROLLER_GET_PRIVATE (pInfoRoller);
	INFO_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if (!  MILDENHALL_IS_INFO_ROLLER (pInfoRoller ) )
        {
                g_warning("invalid info roller object\n");
                return FALSE;
        }

        return priv->bRollerShown;
}

/**
 * mildenhall_info_roller_new:
 *
 * Creates a info roller object
 *
 * Returns: (transfer full): info roller object
 */
MildenhallInfoRoller *mildenhall_info_roller_new (void)
{
	return g_object_new (MILDENHALL_TYPE_INFO_ROLLER,  NULL);
}
