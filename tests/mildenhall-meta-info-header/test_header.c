/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* test_header.c */

#include "mildenhall-internal.h"
#include "mildenhall_meta_info_header.h"

#include <thornbury/thornbury.h>

/* header model columns */
enum
{
        COLUMN_LEFT_ICON_TEXT,
        COLUMN_MID_TEXT,
        COLUMN_RIGHT_ICON_TEXT,
        COLUMN_NONE
};

/* creation of model */
ThornburyModel *create_header_model()
{
        g_autofree gchar *music_icon_file = NULL;
        ThornburyModel *model = NULL;
        model = (ThornburyModel*)thornbury_list_model_new (COLUMN_NONE,
                                        G_TYPE_POINTER, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        -1);
        music_icon_file = g_build_filename (_mildenhall_get_theme_path (),
            "test-drawer-base",
            "icon_music_AC.png",
            NULL);

        thornbury_model_append (model,
                                        COLUMN_LEFT_ICON_TEXT, music_icon_file,
                                        COLUMN_MID_TEXT, "MUSIC DERFASD AWra wer Araerase",
                                        COLUMN_RIGHT_ICON_TEXT,NULL,
                                        -1);

        return model;

}

gboolean model_change(gpointer userData)
{
        g_autofree gchar *internet_icon_file = NULL;
        g_autofree gchar *music_icon_file = NULL;

        static guint new_item_count = 0;
        ThornburyModel *model = (ThornburyModel *)userData;

        GValue value1 = {0};
        GValue value2 = {0};
        if(new_item_count == 0)
        {
                internet_icon_file = g_build_filename (_mildenhall_get_theme_path (),
                    "test-drawer-base",
                    "icon_internet_AC.png",
                    NULL);
                g_value_init(&value1, G_TYPE_POINTER);
                g_value_set_pointer (&value1, internet_icon_file);
                thornbury_model_insert_value(model, 0, COLUMN_LEFT_ICON_TEXT, &value1);

                g_value_init(&value2, G_TYPE_STRING);
                g_value_set_static_string (&value2, NULL);//"INTERNET");
                thornbury_model_insert_value(model, 0, COLUMN_MID_TEXT, &value2);
                new_item_count++;
        }
        else
        {
                music_icon_file = g_build_filename (_mildenhall_get_theme_path (),
                    "test-drawer-base",
                    "icon_music_AC.png",
                    NULL);
                g_value_init(&value1, G_TYPE_POINTER);
                g_value_set_pointer (&value1, music_icon_file);
                thornbury_model_insert_value(model, 0, COLUMN_LEFT_ICON_TEXT, &value1);

                g_value_init(&value2, G_TYPE_STRING);
                g_value_set_static_string (&value2, "MUSIC DERFASD AWra wer Araerase");
                thornbury_model_insert_value(model, 0, COLUMN_MID_TEXT, &value2);

                new_item_count--;
        }
        g_value_unset (&value1);
        g_value_unset (&value2);

        return TRUE;

}

int main (int argc, char **argv)
{
        g_autofree gchar *pImage = NULL;
        g_autofree gchar *overlay_img_file = NULL;
        int clInErr = clutter_init(&argc, &argv);
        if (clInErr != CLUTTER_INIT_SUCCESS)
                return -1;

        ClutterActor *stage;

        stage = clutter_stage_new ();
        pImage = g_build_filename (_mildenhall_get_theme_path (),
            "test-drawer-base",
            "background.png",
            NULL);
        ClutterActor *actor = thornbury_ui_texture_create_new(pImage, 0, 0, FALSE, FALSE);
        clutter_actor_add_child(stage, actor);
        clutter_actor_set_size (stage, 728, 480);
        g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);

	ThornburyItemFactory *itemFactory =
		thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_META_INFO_HEADER,
			 PKGDATADIR "/mildenhall_meta_info_header_prop.json");
        GObject *pObject = NULL;
		g_object_get(itemFactory, "object", &pObject, NULL);
        ClutterActor *pHeader = CLUTTER_ACTOR(pObject);
        clutter_actor_add_child(stage, pHeader);


        ThornburyModel *pHeaderModel = create_header_model();
		g_object_set(pHeader,  "model", pHeaderModel, NULL);

		g_timeout_add_seconds(2, model_change, pHeaderModel);

        overlay_img_file = g_build_filename (_mildenhall_get_theme_path (),
            "content_overlay.png",
            NULL);
        actor = thornbury_ui_texture_create_new (overlay_img_file, 0, 0, FALSE, TRUE);
        clutter_actor_add_child(stage, actor);


	clutter_actor_show (stage);
	clutter_main();
        return 0;
}
