/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/***********************************************************************************
 *       @Filename       :       test_mildenhall_selection_popup_item.c
 *       @Project        :       --
 *----------------------------------------------------------------------------------
 *       @Date           :       9/1/13
 *----------------------------------------------------------------------------------
 *       @Description    :      Test App for Selection Popup Item
 *
 ************************************************************************************/

#include "mildenhall_roller_container.h"

#include <mx/mx.h>
#include <thornbury/thornbury.h>

#include "liblightwood-roller.h"
#include "liblightwood-fixedroller.h"
#include "sample-item.h"
#include "mildenhall_selection_popup_item.h"


/***********************************************************************************
        @Macro Definitions
 ************************************************************************************/
#define WIDTH                           800
#define HEIGHT                          480
#define NUM_OF_ITEMS                   100
/***********************************************************************************
        @Local type Definitions
 ************************************************************************************/

enum
{
	COLUMN_ICON_LEFT ,
	COLUMN_LABEL,
	COLOUMN_LABEL2,
	COLOUMN_TYPE_ITEM,

	COLUMN_LAST
};


/* none */
/**
 * test_mildenhall_selection_roller_item_create_model_ :
 * @num_of_items              : Total no of datas
 *
 * create_model: creates the model.
 */
ThornburyModel *
test_mildenhall_selection_roller_item_create_model (guint num_of_items, enMildenhallSelectionPopupItemType inItemType)
{
	g_autofree gchar *email_icon_file = NULL;
	g_autofree gchar *music_icon_file = NULL;

	CoglHandle icon1, icon2;
	ThornburyModel *model;
	int i;

	email_icon_file = g_build_filename (
	    _mildenhall_get_theme_path (),
	    "mildenhallroller",
	    "icon_email.png",
	    NULL);

	music_icon_file = g_build_filename (
	    _mildenhall_get_theme_path (),
	    "mildenhallroller",
	    "icon_music.png",
	    NULL);

	icon1 = cogl_texture_new_from_file (email_icon_file, COGL_TEXTURE_NONE,
			COGL_PIXEL_FORMAT_ANY, NULL);

	icon2 = cogl_texture_new_from_file (music_icon_file, COGL_TEXTURE_NONE,
			COGL_PIXEL_FORMAT_ANY, NULL);

	model = (ThornburyModel*)thornbury_list_model_new (COLUMN_LAST,
			COGL_TYPE_HANDLE, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_UINT,NULL,
			-1);


	for (i = 0; i < num_of_items; i++)
	{
		thornbury_model_append (model, COLUMN_ICON_LEFT, i % 2 == 0 ? icon1 : icon2,
				COLUMN_LABEL, g_strdup_printf ("SElection  item number %i Exceeds Test", i),
				COLOUMN_LABEL2,g_strdup_printf (" item number Exceeds Test  %i", i),
				COLOUMN_TYPE_ITEM,inItemType,
				-1);
	}

	return model;
}



/**
 * main :
 * @argc              	: Number of commandline argument count
 * @argv         	: array of string.
 *
 * roller_container_get_property gets the property value
 */

int main(int argc, char *argv[])
{
	ClutterInitError err = clutter_init(&argc, &argv);
	(void)err;
	ClutterColor black = {0x00, 0x00, 0x00, 0xff};
	/* creating the stage */
	ClutterActor *stage = clutter_stage_new();
	clutter_actor_set_background_color(stage, &black);
	clutter_actor_set_size (stage,800,600);

	ThornburyModel *pModelLabel = test_mildenhall_selection_roller_item_create_model (NUM_OF_ITEMS , TYPE_ICON_LABEL_LABEL);
	GObject *pObject = NULL;
	pObject = g_object_new(MILDENHALL_TYPE_ROLLER_CONTAINER, "roller-type", MILDENHALL_FIXED_ROLLER_CONTAINER, "width", 588.0, "height", 256.0, "roll-over", FALSE,
	"reactive", TRUE, "show-flow-layout",FALSE,"cylinder-distort",FALSE,"motion-blur",FALSE, NULL);
	ClutterActor *pContLabel = CLUTTER_ACTOR(pObject);
	g_object_set(pObject,  "item-type", MILDENHALL_TYPE_SELECTION_POPUP_ITEM/*TYPE_SAMPLE_ITEM*/, "model", pModelLabel,  NULL);

	mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (pContLabel), "left-icon", COLUMN_ICON_LEFT);
	mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (pContLabel), "main-label", COLUMN_LABEL);
	mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (pContLabel), "right-string", COLOUMN_LABEL2);
	mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (pContLabel), "item-type",COLOUMN_TYPE_ITEM );

	clutter_actor_add_child(stage,pContLabel);

	clutter_actor_show(stage);
	clutter_main();

	return 0;
}


