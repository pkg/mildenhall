/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/***********************************************************************************
*       @Filename       :       roller-container.c
*       @Module         :       RollerContainer
*       @Project        :       --
*----------------------------------------------------------------------------------
*       @Date           :       18/07/2012
*----------------------------------------------------------------------------------
*       @Description    :       The basic idea is to test all the 
*				properties of RollerContainer(fixed roller)
*				and also showcase the usage of lib in application. 
*
************************************************************************************/

#include "mildenhall-internal.h"
#include "mildenhall_roller_container.h"

#include <mx/mx.h>
#include <thornbury/thornbury.h>

#include "liblightwood-roller.h"
#include "liblightwood-fixedroller.h"
#include "sample-item.h"
#include "test-utils.h"


#define AUTOSCROLL_FREQUENCY    1000


# if 0
static ClutterActor *
create_roller (ThornburyModel *model,gfloat width,gfloat height)
{
        GType item_type = MILDENHALL_TYPE_SAMPLE_ITEM;

        ThornburyItemFactory *item_factory = g_object_new (THORNBURY_TYPE_ITEM_FACTORY,
                        "item-type", item_type,
                        "model", model,
                        NULL);

        ClutterActor *roller = g_object_new (MILDENHALL_TYPE_FIXED_ROLLER,
			"height",height,
			"width",width,
                        "roll-over", TRUE,
                        "factory", item_factory,
                        "reactive", TRUE,
                        "clamp-duration", 1250,
                        "clamp-mode", get_elastic_mode (),
                        NULL);
        g_object_set(roller,"model", model,NULL);

        mildenhall_roller_add_attribute (MILDENHALL_ROLLER (roller), "name", COLUMN_NAME);
        mildenhall_roller_add_attribute (MILDENHALL_ROLLER (roller), "label", COLUMN_LABEL);

        return roller;
}
# endif
/* on timeout ,scrolls one item up/down */
static gboolean
scroll_timeout_cb (MxAdjustment *vadjustment)
{
  static gboolean direction_down = TRUE;
  gfloat position;
 	
  /* Get item height  */
  position = mx_adjustment_get_step_increment(vadjustment) ;
  /* get the current position */
  gdouble start = mx_adjustment_get_value(vadjustment);
  if (!direction_down)
    position = start - position;
  else
    position = start + position;

  mx_adjustment_interpolate (vadjustment, position, AUTOSCROLL_FREQUENCY,
      CLUTTER_EASE_IN_OUT_QUAD);

  if (direction_down)
    direction_down = FALSE;
  else
    direction_down = TRUE;

  return TRUE;
}

static void
roller_item_activated_cb (LightwoodRoller *roller, guint row, gpointer data)
{
	g_print("%s:row = %d\n",__FILE__,row);
        static guint new_item_count = 0;
        ThornburyModel *model = (ThornburyModel *)data;
#if 0
        CoglHandle icon = cogl_texture_new_from_file (ICON1_FILE_NAME,
                        COGL_TEXTURE_NONE,
                        COGL_PIXEL_FORMAT_ANY,
                        NULL);
#endif

        g_print("%s:new_item_count = %d\n",__FILE__,new_item_count);
        thornbury_model_remove (model, row);
        g_print("item removed\n");
#if 0
#if 0
        thornbury_model_insert (model, 1, 0, g_strdup_printf ("new item number %i",
                                new_item_count),
                        1, icon,
                        2, g_strdup_printf ("new item number %i",
                                new_item_count),
                        3, FALSE,
                        4, VIDEO1_FILE_NAME,
                        -1);
#endif
	 thornbury_model_append (model, 0, g_strdup_printf ("new item number %i",
                                new_item_count),
                        1, icon,
                        2, g_strdup_printf ("new item number %i",
                                new_item_count),
                        3, FALSE,
                        4, VIDEO1_FILE_NAME,
                        -1);
                new_item_count++;
#endif
}

gboolean change_bg_clb(gpointer userData)
{
	GObject *pObject1 = G_OBJECT(userData);
	gboolean bEnableBg = FALSE;
  g_object_get (pObject1, "display-background", &bEnableBg, NULL);
  g_object_set (pObject1, "display-background", !bEnableBg, NULL);
  if (bEnableBg)
    g_print ("background disabled\n");
  else
    g_print ("background enabled\n");
	return TRUE;

}
gboolean set_focus(gpointer userData)
{
	g_object_set(G_OBJECT(userData),"focused-row", 0,NULL);
	return FALSE;
}
gboolean append_clb(gpointer userData)
{
	g_autofree gchar *email_icon_file = NULL;
	g_autofree gchar *launcher_video_file = NULL;

	 static guint new_item_count = 0;
        ThornburyModel *model = (ThornburyModel *)userData;
        g_print("%s:new_item_count = %d\n",__FILE__,new_item_count);
	
	email_icon_file = g_build_filename (
	    _mildenhall_get_theme_path (),
	    "mildenhallroller",
	    "icon_email.png",
	    NULL);

	launcher_video_file = g_build_filename (
	    _mildenhall_get_theme_path (),
	    "mildenhallroller",
	    "01_appl_launcher_2009.03.09.avi",
	    NULL);

         thornbury_model_append (model, 0, g_strdup_printf ("new item number %i",
                                new_item_count),
                        1, email_icon_file,
                        2, g_strdup_printf ("new item number %i",
                                new_item_count),
                        3, FALSE,
                        4, launcher_video_file,
                        -1);
                new_item_count++;
        g_print("item appended\n");
	return TRUE;

}

int main(int argc,char ** argv)
{
  g_autofree gchar *roller_container_prop_file = NULL;
  ThornburyItemFactory *itemFactory;
  ThornburyItemFactory *itemFactory1;
  ClutterInitError err = clutter_init(&argc, &argv);
  (void)err;
  ClutterColor black = {0x00, 0x00, 0x00, 0xff};
  /* creating the stage */
  ClutterActor *stage = clutter_stage_new();
  clutter_actor_set_background_color(stage, &black);
  clutter_actor_set_size (stage,800,480);
  /* if auto_scroll is required, set the falg to true */
  gboolean auto_scroll = FALSE;

# if 0
  /* container */
  ThornburyModel *model = lightwood_create_model (2);
  ClutterActor *roller = create_roller (model,728,480);//300,300);
  ClutterActor *cont = roller_container_new (roller,728,480);//300,300);//,570);
  v_ROLLERCONT_set_arrow(MILDENHALL_ROLLER_CONTAINER(cont),FALSE);
  //v_ROLLERCONT_set_arrow_position(MILDENHALL_ROLLER_CONTAINER(cont),200.0);
  clutter_actor_add_child(stage,cont);

  /* roller */
  ThornburyModel *model1 = lightwood_create_model (10);
  ClutterActor *roller1 = create_roller(model1,300,300);
  ClutterActor *cont1 = roller_container_new (roller1, 300,300);//,470);
  v_ROLLERCONT_set_arrow(MILDENHALL_ROLLER_CONTAINER(cont1),TRUE);
  clutter_actor_set_position(cont1,310.0,0.0);
  clutter_actor_add_child(stage,cont1);

  g_signal_connect (G_OBJECT (roller), "item-activated",
      G_CALLBACK (roller_item_activated_cb), model);
  g_timeout_add_seconds(15,append_clb,model);
#endif
  /* create model */
  ThornburyModel *model = lightwood_create_model (3);

  /* create itemFactory instance */
  roller_container_prop_file = _mildenhall_get_resource_path ("mh_roller_container_prop.json");
  itemFactory = thornbury_item_factory_generate_widget_with_props (
      MILDENHALL_TYPE_ROLLER_CONTAINER, roller_container_prop_file);
  GObject *pObject = NULL;
  //pObject = g_object_new(MILDENHALL_TYPE_ROLLER_CONTAINER, "roller-type", MILDENHALL_FIXED_ROLLER_CONTAINER, "width", 300.0, "height", 380.0, "roll-over", TRUE, "reactive", TRUE, "arrow", TRUE, "arrowPosition", 190.0, NULL);

  /* Get roller instance from item factory */
  g_object_get(itemFactory, "object", &pObject, NULL);
  ClutterActor *cont = CLUTTER_ACTOR(pObject);
  /* Set relevent roller Property according to the requirement*/
  g_object_set (pObject,
                "display-background", FALSE,
                "item-type", MILDENHALL_TYPE_SAMPLE_ITEM,
                "model", model,
                NULL);
  /* Set the attributes on the roller to be shown */
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (cont), "name", COLUMN_NAME);
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (cont), "label", COLUMN_LABEL);
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (cont), "icon", COLUMN_ICON);
  //v_ROLLERCONT_set_arrow(MILDENHALL_ROLLER_CONTAINER(cont),TRUE);
  /* Add roller to stage */
  clutter_actor_add_child(stage,cont);
  /* connect signal on item activation (double click)*/
  g_signal_connect (G_OBJECT (cont), "roller-item-activated", G_CALLBACK (roller_item_activated_cb), model);

  g_timeout_add_seconds(15,append_clb,model);
  /* set roller to focus for given row */
  g_timeout_add_seconds(1,set_focus,cont);




  /* To test the adjustment prop */
  MxAdjustment *vadjust;
  /* Get Adjustment value of roller */
  g_object_get (pObject, "vadjust", &vadjust, NULL);
  if (auto_scroll)
  {
    g_print("auto scroll is set \n");
    g_timeout_add_full (G_PRIORITY_LOW, AUTOSCROLL_FREQUENCY,
	(GSourceFunc) scroll_timeout_cb, vadjust, NULL);
  }

  g_timeout_add_seconds (5, change_bg_clb, pObject);
  clutter_actor_show(stage);
  /* Run Main Loop */
  clutter_main();

  /* Returns success status on exiting main thread */
  return EXIT_SUCCESS;

}
