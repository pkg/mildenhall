/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-internal.h"
#include "mildenhall_roller_container.h"

#include <mx/mx.h>
#include <thornbury/thornbury.h>

#include "liblightwood-roller.h"
#include "liblightwood-fixedroller.h"
#include "sample-item.h"
#include "sample-thumbnail-item.h"
#include "test-utils.h"

#define with_cogl 1

#if 0
static ClutterActor *
create_roller (ThornburyModel *model,gfloat width,gfloat height)
{
	//GType item_type = TYPE_SAMPLE_ITEM;

	ThornburyItemFactory *item_factory ;

	item_factory = g_object_new (THORNBURY_TYPE_ITEM_FACTORY,
			"item-type", TYPE_SAMPLE_THUMBNAIL_ITEM,
			NULL);
	ClutterActor *roller = g_object_new (MILDENHALL_TYPE_FIXED_ROLLER,
			"roll-over", TRUE,
			"factory", item_factory,
			"flow-layout", TRUE,
			"cylinder-distort", TRUE,
			"motion-blur", FALSE,
			"reactive", TRUE,
			//"clamp-duration", 1250,
			//"clamp-mode", get_elastic_mode (),
			NULL);
	g_object_set(roller,"model", model,NULL);

	mildenhall_roller_add_attribute (MILDENHALL_ROLLER (roller), "name", 0);
	mildenhall_roller_add_attribute (MILDENHALL_ROLLER (roller), "cogl-texture", 6);
	return roller;
}
#endif

static void
roller_item_activated_cb (LightwoodRoller *roller, guint row, gpointer data)
{
	g_print("%s row = %d \n",__FUNCTION__,row);
}
gboolean set_focus(gpointer userData)
{
        g_object_set(G_OBJECT(userData),"focused-row", 0,NULL);
        return FALSE;
}

int main(int argc,char ** argv)
{
	g_autofree gchar *prop_file = NULL;
	ThornburyItemFactory *itemFactory;
        ClutterInitError err = clutter_init(&argc, &argv);
	(void)err;
        ClutterColor black = {0x00, 0x00, 0x00, 0xff};
        /* creating the stage */
        ClutterActor *stage = clutter_stage_new();
        clutter_actor_set_background_color(stage, &black);
        clutter_actor_set_size (stage,800,480);
# if 0
        /* container */
        ThornburyModel *model = lightwood_create_model (30);
        ClutterActor *roller = create_roller(model,300,300);
        ClutterActor *cont = roller_container_new (roller, 300,480);//,770);
	clutter_actor_set_position(cont,100,100);
        clutter_actor_add_child(stage,cont);
# endif

        ThornburyModel *model = lightwood_create_model (16);

	prop_file = _mildenhall_get_resource_path ("cover_detail_trans_prop.json");
	itemFactory = thornbury_item_factory_generate_widget_with_props (
	    MILDENHALL_TYPE_ROLLER_CONTAINER, prop_file);
        GObject *pObject = NULL;

        g_object_get(itemFactory, "object", &pObject, NULL);
        ClutterActor *cont = CLUTTER_ACTOR(pObject);
        g_object_set(pObject,  "item-type", TYPE_SAMPLE_THUMBNAIL_ITEM, "model", model,  NULL);
        mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (cont), "name", 0);
        mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (cont), "cogl-texture", 6);
        mildenhall_roller_container_set_show_arrow (MILDENHALL_ROLLER_CONTAINER (cont), TRUE);
	mildenhall_roller_container_set_min_visible_actors (MILDENHALL_ROLLER_CONTAINER (cont), 30);

	clutter_actor_add_child(stage,cont);
#if 0
        /* roller */
        ThornburyModel *model1 = lightwood_create_model (10);
        ClutterActor *roller1 = create_roller(model1,300,300);
        ClutterActor *cont1 = roller_container_new (roller1);
        clutter_actor_set_position(cont1,310.0,0.0);
        clutter_actor_add_child(stage,cont1);
#endif

	g_timeout_add(700,set_focus,cont);
	/* connect signal on item activation (double click)*/
  g_signal_connect (G_OBJECT (cont), "roller-item-activated", G_CALLBACK (roller_item_activated_cb), model);
        clutter_actor_show(stage);
        clutter_main();
	return 0;
}
