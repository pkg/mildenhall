/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-internal.h"
#include "map_widget.h"

#include <thornbury/thornbury.h>

MapWidget *map=NULL;
ClutterActor *pStage=NULL;
enum _MapModel
{
   COLUMN_VIEW_POINT_TYPE,
   COLUMN_LATITUDE,
   COLUMN_LONGITUDE,
   COLUMN_MARKER_NAME,
   COLUMN_NORMAL_IMAGE,
   COLUMN_SELECTED_IMAGE,
   COLUMN_HIGHLIGHT_MARKER,
    COLUMN_LAST

};

gboolean move_from_map(gpointer data)
{

    ThornburyModel *model=THORNBURY_MODEL(data);

    thornbury_model_append (model, COLUMN_VIEW_POINT_TYPE,3,
                            COLUMN_LATITUDE, 52.522070,
                            COLUMN_LONGITUDE,13.397999,
                            -1);




return FALSE;
}


gboolean remove_from_map(gpointer data)
{

    ThornburyModel *model=THORNBURY_MODEL(data);

     thornbury_model_remove(model,4);

         g_timeout_add_seconds(4,move_from_map,model);



return FALSE;
}



static gboolean selected_clb(MapWidget *map,gchar *name,gpointer data)
{
g_print("\n  selected_clb     %s\n",name);
return FALSE;
}




int main (int argc, char **argv)
{
    g_autofree gchar *current_img_file = NULL;
    g_autofree gchar *current_active_img_file = NULL;
    gint zoom = 15;
    int clInErr = clutter_init(&argc, &argv);
    if (clInErr != CLUTTER_INIT_SUCCESS)
        return -1;

    clutter_threads_init ();



    ClutterColor black = { 0x00, 0x00, 0x00, 0x00 };

    pStage = clutter_stage_get_default ();
    clutter_stage_set_color (CLUTTER_STAGE (pStage), &black);
    clutter_actor_set_size (pStage, 728, 480);
    ThornburyModel *model=NULL;
    map=p_map_widget_new();
    g_signal_connect(map,"marker-selected",(GCallback)selected_clb,NULL);
    g_object_set (map, "zoom-level", zoom, NULL);
   g_print("\n zoom %d \n",zoom);

    model = (ThornburyModel*)thornbury_list_model_new (COLUMN_LAST, G_TYPE_INT, NULL,
                                  G_TYPE_FLOAT, NULL,
                                  G_TYPE_FLOAT, NULL,
                                  G_TYPE_STRING, NULL,
                                  G_TYPE_STRING, NULL,
                                  G_TYPE_STRING, NULL,
                                  G_TYPE_BOOLEAN, NULL,
                                  -1);
g_object_set(map,"latitude",52.522710,"longitude",13.409951,NULL);
thornbury_model_append (model, COLUMN_VIEW_POINT_TYPE, 2,
                        COLUMN_LATITUDE, 52.517664,
                        COLUMN_LONGITUDE,13.397060,
                        COLUMN_MARKER_NAME,"one",
                        COLUMN_HIGHLIGHT_MARKER,TRUE,-1);

    current_img_file = g_build_filename (_mildenhall_get_theme_path (),
        "current.png",
        NULL);
    current_active_img_file = g_build_filename (_mildenhall_get_theme_path (),
        "current_ac.png",
        NULL);


     thornbury_model_append (model,
          COLUMN_VIEW_POINT_TYPE, 3,
          COLUMN_LATITUDE, 52.516667,
          COLUMN_LONGITUDE, 13.400000,
          COLUMN_NORMAL_IMAGE, current_img_file,
          COLUMN_SELECTED_IMAGE, current_active_img_file,
          -1);
thornbury_model_append (model, COLUMN_VIEW_POINT_TYPE,3,
                        COLUMN_LATITUDE, 52.522710,
                        COLUMN_LONGITUDE, 13.409951,
                        -1);


thornbury_model_append (model, COLUMN_VIEW_POINT_TYPE,3,
                        COLUMN_LATITUDE, 52.523755,
                        COLUMN_LONGITUDE, 13.402038,
                        -1);


thornbury_model_append (model, COLUMN_VIEW_POINT_TYPE,3,
                        COLUMN_LATITUDE, 52.522070,
                        COLUMN_LONGITUDE,13.397999,
                        -1);


g_object_set(map,"model",model,NULL);
g_object_set(map,"draw-path",TRUE,NULL);
g_timeout_add_seconds(8,remove_from_map,model);

    clutter_actor_add_child(pStage,CLUTTER_ACTOR(map));

   clutter_actor_show(pStage);



    clutter_actor_show (CLUTTER_ACTOR(map));

    clutter_main();

    return 0;

}




